<?php

$systemConfig = array();

$systemConfig['cacheDir'] = '/data/cache';
// TXT
$systemConfig['filesDir'] = '/data/files';
// DB
$systemConfig['db'] = '/data/db';

$systemConfig['publicDir'] = '/public';


return $systemConfig;
