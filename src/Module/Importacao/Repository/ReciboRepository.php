<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Repository;

use Core\DB\Connection;
use Importacao\Entity\Recibo;
use PDO;

/**
 * Repositório para as Emissões
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ReciboRepository
{

    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var EmissaoRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param Connection $conn
     */
    private function __construct(Connection $conn)
    {
        $this->conn = $conn;

        $sql = 'CREATE TABLE IF NOT EXISTS recibo (
                num_recibo_gosoft INTEGER NOT NULL
                                          UNIQUE,
                num_recibo_union  INTEGER NOT NULL
                                          UNIQUE,
                emissao           INTEGER NOT NULL
                                          REFERENCES emissao (emissao),
                data_geracao      DATE,
                codigo_condominio INTEGER,
                codigo_bloco      TEXT,
                codigo_unidade    INTEGER,
                numero_bancario   TEXT,
                qtd_dias_prazo    INTEGER,
                isento_pagamento  INTEGER,
                desconto_total    REAL,
                valor_total       REAL,
                PRIMARY KEY (
                    num_recibo_union
                )
            );';

        $this->conn->getHandler()->exec($sql);
        $this->conn->getHandler()->exec('INSERT INTO sqlite_sequence(name,seq) SELECT "recibo", 0 WHERE NOT EXISTS (SELECT 1 FROM sqlite_sequence WHERE name="recibo")');
        $this->conn->doCommit();
    }

    /**
     * Gerenciador de instância para o repositório
     * @param Connection $conn
     * @return EmissaoRepository
     */
    public static function getInstance(Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }

    public function salvar(Recibo $recibo)
    {
        if (!$recibo->getNum_recibo_gosoft()) {
            $recibo->setNum_recibo_gosoft($this->getNextId());
        }

        $stmt = $this->conn->getHandler()->prepare('INSERT INTO '
                . 'recibo (
                    num_recibo_gosoft,
                    num_recibo_union,
                    emissao,
                    data_geracao,
                    codigo_condominio,
                    codigo_bloco,
                    codigo_unidade,
                    numero_bancario,
                    qtd_dias_prazo,
                    isento_pagamento,
                    desconto_total,
                    valor_total
                ) '
                . 'VALUES '
                . '(
                    :num_recibo_gosoft,
                    :num_recibo_union,
                    :emissao,
                    :data_geracao,
                    :codigo_condominio,
                    :codigo_bloco,
                    :codigo_unidade,
                    :numero_bancario,
                    :qtd_dias_prazo,
                    :isento_pagamento,
                    :desconto_total,
                    :valor_total
                ) ');

        if ($stmt) {
            $stmt->bindValue(':num_recibo_gosoft', $recibo->getNum_recibo_gosoft(), PDO::PARAM_STR);
            $stmt->bindValue(':num_recibo_union', $recibo->getNum_recibo_union(), PDO::PARAM_STR);
            $stmt->bindValue(':emissao', $recibo->getEmissao(), PDO::PARAM_STR);
            $stmt->bindValue(':data_geracao', ($recibo->getData_geracao() instanceof \DateTime ? $recibo->getData_geracao()->format('Y-m-d') : $recibo->getData_geracao()), PDO::PARAM_STR);
            $stmt->bindValue(':codigo_condominio', $recibo->getCodigo_condominio(), PDO::PARAM_STR);
            $stmt->bindValue(':codigo_bloco', $recibo->getCodigo_bloco(), PDO::PARAM_STR);
            $stmt->bindValue(':codigo_unidade', $recibo->getCodigo_unidade(), PDO::PARAM_STR);
            $stmt->bindValue(':numero_bancario', $recibo->getNumero_bancario(), PDO::PARAM_STR);
            $stmt->bindValue(':qtd_dias_prazo', $recibo->getQtd_dias_prazo(), PDO::PARAM_STR);
            $stmt->bindValue(':isento_pagamento', $recibo->getIsento_pagamento(), PDO::PARAM_STR);
            $stmt->bindValue(':desconto_total', $recibo->getDesconto_total(), PDO::PARAM_STR);
            $stmt->bindValue(':valor_total', $recibo->getValor_total(), PDO::PARAM_STR);



            if ($stmt->execute()) {

                $this->conn->doCommit();
                return $recibo;
            } else {
                $errorInfo = $stmt->errorInfo();
                return array('success' => false, 'error' => ($errorInfo[0] == 23000 ? 'Código GoSoft já existe em outro cadastro da associação da emissão' : ''));
            }
        } else {
            var_dump($this->conn->getHandler()->errorInfo());
        }


        return null;
    }
    
    public function listar()
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'recibo ORDER BY codigo_condominio,data_geracao');

        if ($stmt) {
            if ($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, Recibo::class);
            }
        }

        return null;
    }
    
    /**
     * Lista emissões por condomínio
     * @param integer $condominio
     * @return array
     */
    public function listarPorCondominio($condominio)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'recibo WHERE codigo_condominio = :condominio ORDER BY codigo_condominio,data_geracao');

        if ($stmt) {
            $stmt->bindParam(':condominio', $condominio, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, Recibo::class);
            }
        }

        return null;
    }
    
    /**
     * Obtém recibo por código
     * @param string $codigo
     * @return string
     */
    public function obterPorCodUnionWeb($codigo)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'recibo WHERE num_recibo_union = :codigo');

        if ($stmt) {
            $stmt->bindParam(':codigo', $codigo, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                return $stmt->fetchObject(Recibo::class);
            }
        }

        return null;
    }

    private function getNextId()
    {
        $selId = 'SELECT seq FROM sqlite_sequence WHERE name = "recibo"';
        $stmt = $this->conn->getHandler()->prepare($selId);

        if ($stmt->execute()) {
            $row = $stmt->fetch();
            
            $this->conn->getHandler()->exec('UPDATE sqlite_sequence SET seq = seq+1 WHERE name="recibo"');
            $this->conn->doCommit();
            
            return $row['seq'];
        }
    }

}
