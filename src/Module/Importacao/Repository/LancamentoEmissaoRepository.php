<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Repository;

use Associacao\Repository\CondominioRepository;
use Core\DB\Connection;
use Importacao\Entity\LancamentoEmissao;
use PDO;

/**
 * Repositório para lançamentos de emissão
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class LancamentoEmissaoRepository
{
    
    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var CondominioRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param Connection $conn
     */
    private function __construct(Connection $conn)
    {
        $this->conn = $conn;
        
        $sqlCreate = 'CREATE TABLE IF NOT EXISTS "emissao_lancamento" (
                        "emissao"	TEXT NOT NULL,
                        "lancamento"	INTEGER NOT NULL,
                        "conta"	INTEGER NOT NULL,
                        "historico"	TEXT(28),
                        PRIMARY KEY("lancamento","emissao")
                );';
        
        $this->conn->getHandler()->exec($sqlCreate);
        $sqlCreateUnq = 'CREATE UNIQUE INDEX IF NOT EXISTS "uq_lancamento_emissao" ON "emissao_lancamento" (
                        "emissao",
                        "lancamento"
                );';
        
        $this->conn->getHandler()->exec($sqlCreateUnq);
        $this->conn->doCommit();
    }

    /**
     * Gerenciador de instância para o repositório
     * @param Connection $conn
     * @return CondominioRepository
     */
    public static function getInstance(Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }
    
    /**
     * Salva os lançamentos das emissões
     * @param LancamentoEmissao $lancamento
     * @return LancamentoEmissao
     */
    public function salvar(LancamentoEmissao $lancamento)
    {
        $stmt = $this->conn->getHandler()->prepare('INSERT INTO '
                . 'emissao_lancamento (
                    emissao,
                    lancamento,
                    conta,
                    historico
                ) '
                . 'VALUES '
                . '(
                    :emissao,
                    :lancamento,
                    :conta,
                    :historico
                ) ');
       
        if ($stmt) {
            $stmt->bindValue(':emissao', $lancamento->getEmissao()->getCodigo(), PDO::PARAM_STR);
            $stmt->bindValue(':lancamento', $lancamento->getLancamento(), PDO::PARAM_STR);
            $stmt->bindValue(':conta', $lancamento->getConta(), PDO::PARAM_STR);
            $stmt->bindValue(':historico', $lancamento->getHistorico(), PDO::PARAM_STR);
            


            if ($stmt->execute()) {
                $this->conn->doCommit();
                return $lancamento;
            } else {
                $errorInfo = $stmt->errorInfo();
                var_dump($errorInfo);
                var_dump($lancamento);
                return array('success' => false, 'error' => ($errorInfo[0] == 23000 ? 'Código GoSoft já existe em outro cadastro da emissão' : ''));
            }
        } else {
            var_dump($this->conn->getHandler()->errorInfo());
        }


        return null;
    }
    
    /**
     * Lista lançamentos por emissão
     * @param \Importacao\Entity\Emissao $emissao
     * @return array
     */
    public function listarPorEmissao($emissao)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'emissao_lancamento WHERE emissao = :emissao');
       
        if ($stmt) {
            $stmt->bindValue(':emissao', (is_object($emissao) ? $emissao->getCodigo() : $emissao), PDO::PARAM_INT);
            
            if($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, LancamentoEmissao::class);
            }
        }
        
        return null;
    }
    
    /**
     * Lista lançamentos por emissão (Codigo UnionData
     * @param \Importacao\Entity\Emissao $emissao
     * @return array
     */
    public function listarPorEmissaoUnionData($emissao)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'emissao_lancamento '
                . 'INNER JOIN emissao ON emissao.codigo = emissao_lancamento.emissao '
                . 'WHERE emissao.emissao = :emissao');
       
        if ($stmt) {
            $stmt->bindValue(':emissao', (is_object($emissao) ? $emissao->getCodigo() : $emissao), PDO::PARAM_INT);
            
            if($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, LancamentoEmissao::class);
            }
        }
        
        return null;
    }
    
    /**
     * Lista todas as emissões
     * @return array
     */
    public function listar()
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT EL.* FROM '
                . 'emissao_lancamento EL '
                . 'INNER JOIN '
                . ' emissao E ON E.codigo = EL.emissao '
                . ' INNER JOIN '
                . ' condominio C on C.codUnionWeb = E.condominio');
       
        if ($stmt) {
            if($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, LancamentoEmissao::class);
            } else {
                var_dump($stmt->errorInfo());
            }
        }
        
        return null;
    }
    
    
    public function obterLancPorEmissaoConta($condominio, $emissao, $conta, $desc)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'emissao_lancamento '
                . 'INNER JOIN emissao ON emissao.codigo = emissao_lancamento.emissao '
                . 'WHERE emissao.emissao = :emissao AND emissao_lancamento.conta = :conta AND emissao.condominio = :condominio AND historico = :historico');
       
        if ($stmt) {
            $stmt->bindValue(':condominio', $condominio, PDO::PARAM_INT);
            $stmt->bindValue(':emissao', (is_object($emissao) ? $emissao->getCodigo() : $emissao), PDO::PARAM_INT);
            $stmt->bindValue(':conta', $conta, PDO::PARAM_INT);
            $stmt->bindValue(':historico', $desc, PDO::PARAM_STR);
            
            if($stmt->execute()) {
                return $stmt->fetchColumn(1);
            } 
        } 
        
        return null;
    }
}
