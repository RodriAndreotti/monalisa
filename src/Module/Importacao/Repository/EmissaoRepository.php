<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Repository;

use Core\DB\Connection;
use Importacao\Entity\Emissao;
use PDO;

/**
 * Repositório para as Emissões
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class EmissaoRepository
{

    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var EmissaoRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param Connection $conn
     */
    private function __construct(Connection $conn)
    {
        $this->conn = $conn;

        $sqlCreateUnq = 'CREATE UNIQUE INDEX IF NOT EXISTS uq_emissao ON emissao (
                condominio,
                emissao
            );
            ';
        $this->conn->getHandler()->exec($sqlCreateUnq);
        $this->conn->doCommit();
    }

    /**
     * Gerenciador de instância para o repositório
     * @param Connection $conn
     * @return EmissaoRepository
     */
    public static function getInstance(Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }

    public function salvar(Emissao $emissao)
    {
        $stmt = $this->conn->getHandler()->prepare('INSERT INTO '
                . 'emissao (
                    condominio,
                    emissao,
                    vencimento
                    
                ) '
                . 'VALUES '
                . '(
                    :condominio,
                    :emissao,
                    :vencimento
                ) ');

        if ($stmt) {
            $stmt->bindValue(':condominio', $emissao->getCondominio(), PDO::PARAM_STR);
            $stmt->bindValue(':emissao', $emissao->getEmissao(), PDO::PARAM_STR);
            if ($emissao->getVencimento() instanceof \DateTime) {
                $stmt->bindValue(':vencimento', $emissao->getVencimento()->format('Y-m-d'), PDO::PARAM_STR);
            } else {
                var_dump($emissao);
                exit;
            }



            if ($stmt->execute()) {
                $emissao->setCodigo($this->conn->getHandler()->lastInsertId());

                if(count($emissao->getLancamentos()) > 0) {
                    $stmtLanc = $this->conn->getHandler()->prepare('INSERT INTO '
                            . 'emissao_lancamento (
                                    emissao,
                                    lancamento,
                                    conta,
                                    historico
                                ) '
                            . 'VALUES '
                            . '(
                                    :emissao,
                                    :lancamento,
                                    :conta,
                                    :historico
                                ) ');
                    if ($stmtLanc) {
                        foreach ($emissao->getLancamentos() as $lancamento) {

                            $stmtLanc->bindValue(':emissao', $lancamento->getEmissao()->getCodigo(), PDO::PARAM_STR);
                            $stmtLanc->bindValue(':lancamento', $lancamento->getLancamento(), PDO::PARAM_STR);
                            $stmtLanc->bindValue(':conta', $lancamento->getConta(), PDO::PARAM_STR);
                            $stmtLanc->bindValue(':historico', $lancamento->getHistorico(), PDO::PARAM_STR);



                            if (!$stmtLanc->execute()) {
                                $this->conn->doRollback();
                                return false;
                            }
                        }
                    } else {
                        var_dump($stmtLanc->errorInfo());
                        exit;
                        $this->conn->doRollback();
                        return false;
                    }
                }

                $this->conn->doCommit();

                return $emissao;
            } else {
                $errorInfo = $stmt->errorInfo();
                var_dump($emissao->getCondominio());
                var_dump($emissao->getEmissao());
                var_dump($errorInfo);
                return array('success' => false, 'error' => ($errorInfo[0] == 23000 ? 'Código GoSoft já existe em outro cadastro da emissão' : ''));
            }
        } else {
            var_dump($this->conn->getHandler()->errorInfo());
        }


        return null;
    }

    /**
     * Lista todas as emissões
     * @return array
     */
    public function listar()
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'emissao ORDER BY condominio,vencimento');

        if ($stmt) {
            if ($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, Emissao::class);
            }
        }

        return null;
    }

    /**
     * Lista emissões por condomínio
     * @param integer $condominio
     * @return array
     */
    public function listarPorCondominio($condominio)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'emissao WHERE condominio = :condominio ORDER BY condominio,vencimento');

        if ($stmt) {
            $stmt->bindParam(':condominio', $condominio, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, Emissao::class);
            }
        }

        return null;
    }

}
