<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Repository;

use Associacao\Repository\CondominioRepository;
use Core\DB\Connection;
use Importacao\Entity\LancamentoEmissao;
use PDO;

/**
 * Repositório para lançamentos de emissão
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class LancamentoReciboRepository
{

    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var CondominioRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param Connection $conn
     */
    private function __construct(Connection $conn)
    {
        $this->conn = $conn;

        $sqlCreate = 'CREATE TABLE recibo_lancamento (
                recibo         INTEGER NOT NULL,
                emissao        INTEGER NOT NULL,
                lancamento     INTEGER NOT NULL,
                comp_historico TEXT,
                valor          DECIMAL,
                UNIQUE (
                    recibo,
                    emissao,
                    lancamento
                ),
                PRIMARY KEY (
                    recibo,
                    emissao,
                    lancamento
                )
            );';

        $this->conn->getHandler()->exec($sqlCreate);
        $this->conn->doCommit();
    }

    /**
     * Gerenciador de instância para o repositório
     * @param Connection $conn
     * @return LancamentoReciboRepository
     */
    public static function getInstance(Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }

    /**
     * Salva os lançamentos das emissões
     * @param \Importacao\Entity\LancamentoRecibo $lancamento
     * @return \Importacao\Entity\LancamentoRecibo
     */
    public function salvar(\Importacao\Entity\LancamentoRecibo $lancamento)
    {
//        if(!$this->existe($lancamento)) {
            $stmt = $this->conn->getHandler()->prepare('INSERT INTO '
                    . 'recibo_lancamento (
                        recibo,
                        emissao,
                        lancamento,
                        comp_historico,
                        valor
                    ) '
                    . 'VALUES '
                    . '(
                        :recibo,
                        :emissao,
                        :lancamento,
                        :comp_historico,
                        :valor
                    ) ');

            if ($stmt) {
                $stmt->bindValue(':recibo', (is_object($lancamento->getRecibo()) ? $lancamento->getRecibo()->getNum_recibo_union() : $lancamento->getRecibo()), PDO::PARAM_STR);
                $stmt->bindValue(':emissao', $lancamento->getEmissao(), PDO::PARAM_STR);
                $stmt->bindValue(':lancamento', $lancamento->getLancamento(), PDO::PARAM_STR);
                $stmt->bindValue(':comp_historico', $lancamento->getCompHistorico(), PDO::PARAM_STR);
                $stmt->bindValue(':valor', $lancamento->getValor(), PDO::PARAM_STR);



                if ($stmt->execute()) {
                    $this->conn->doCommit();
                    return $lancamento;
                } else {
                    $errorInfo = $stmt->errorInfo();
                    if($errorInfo[0] != '23000') {
                        var_dump($errorInfo);
                    }
                    return array('success' => false, 'error' => '');
                }
            } else {
                var_dump($this->conn->getHandler()->errorInfo());
                var_dump($lancamento);
            }


            return null;
       /* }
        return $lancamento;*/
    }

    /**
     * Lista lançamentos por Recibo
     * @param \Importacao\Entity\Recibo $recibo
     * @return array
     */
    public function listarPorRecibo($recibo)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'recibo_lancamento WHERE recibo = :recibo');

        if ($stmt) {
            $stmt->bindValue(':recibo', (is_object($recibo) ? $recibo->getNum_recibo_union() : $recibo), PDO::PARAM_INT);

            if ($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, \Importacao\Entity\LancamentoRecibo::class);
            }
        }

        return null;
    }

    /**
     * Lista todos os lançamentos de recibos
     * @return array
     */
    public function listar()
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT EL.* FROM recibo_lancamento EL 
                INNER JOIN 
                 emissao E ON E.emissao = EL.emissao
                INNER JOIN 
                condominio C on C.codUnionWeb = E.condominio
                WHERE C.codGoSoft');

        if ($stmt) {
            if ($stmt->execute()) {
                return $stmt->fetchAll(PDO::FETCH_CLASS, \Importacao\Entity\LancamentoRecibo::class);
            }
        }

        return null;
    }

    public function existe($lancamento)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM '
                . 'recibo_lancamento WHERE recibo = :recibo AND emissao=:emissao AND lancamento=:lancamento');

        if ($stmt) {
            $stmt->bindValue(':recibo', (is_object($lancamento->getRecibo()) ? $lancamento->getRecibo()->getNum_recibo_union() : $lancamento->getRecibo()), PDO::PARAM_INT);
            $stmt->bindValue(':emissao', (is_object($lancamento->getEmissao()) ? $lancamento->getEmissao()->getEmissao() : $lancamento->getEmissao()), PDO::PARAM_INT);
            $stmt->bindValue(':lancamento', $lancamento->getLancamento(), PDO::PARAM_INT);

            if ($stmt->execute()) {
                return $stmt->fetchObject(\Importacao\Entity\LancamentoRecibo::class);
            }
        }

        return false;
    }

}
