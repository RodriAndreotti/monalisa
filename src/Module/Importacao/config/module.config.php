<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return array(
    'routes'    =>  array(
        'importacao' =>  array(
            'route'     =>  '/importacao[/:action][/:id]',
            'controller'    => \Importacao\Controller\ImportacaoController::class,
            'view-dir'      =>  __DIR__  . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view',
            'padroes'  =>  array(
                'action'    =>  '[a-z][a-zA-Z0-9]+',
                'id'        =>  '[0-9]+'
            )
        ),
        'view' =>  array(
            'route'     =>  '/view[/:action][/:id]',
            'controller'    => \Importacao\Controller\ViewController::class,
            'view-dir'      =>  __DIR__  . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view',
            'padroes'  =>  array(
                'action'    =>  '[a-z][a-zA-Z0-9]+',
                'id'        =>  '[0-9]+'
            )
        )
    ),
    'services' => array(
        \Importacao\Repository\EmissaoRepository::class    => Core\Factory\Repository\DefaultRepositoryFactory::class,
        \Importacao\Repository\ReciboRepository::class  => \Core\Factory\Repository\DefaultRepositoryFactory::class,
        \Importacao\Repository\LancamentoReciboRepository::class  => Core\Factory\Repository\DefaultRepositoryFactory::class,
        \Importacao\Repository\LancamentoEmissaoRepository::class  => Core\Factory\Repository\DefaultRepositoryFactory::class
    )
);