<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$recibos = $this->get('recibos');
$condominios = $this->get('condominios');
$selected = $this->get('selected');
?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3 class="box-title">Recibos</h3>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <?= \Core\Util\Message::getMessages(); ?>
                <form name="frmFiltroCondominio" method="post" id="frmFiltro">
                    <div class="form-group">
                        <label for="cbCondominio">Filtrar por condomínio: </label>
                        <select name="condominio" id="cbCondominio" class="form-control form-control-line">
                            <option value="">Todos</option>
                            <?php foreach ($condominios as $condominio): ?>
                                <option 
                                    value="<?= $condominio->getCodUnionWeb(); ?>"
                                    <?= ($condominio->getCodUnionWeb() == $selected ? ' selected="selected"' : ''); ?>>
                                        <?= $condominio->getNome(); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </form>
                <table class="table table-responsive table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Condomínio</th>
                            <th>Bloco</th>
                            <th>Unidade</th>
                            <th>Emissão (UnionWeb)</th>
                            <th>Código</th>
                            <th>Recibo (UnionWeb)</th>
                            <th>Data de geração</th>
                            <th class="bg-info">Valor do Recibo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($recibos as $recibo): ?>
                            <tr>
                                <td><?= $recibo->getCodigo_condominio() ? $recibo->getCodigo_condominio()->getNome() : 'Condomínio não identificado'; ?></td>
                                <td><?= $recibo->getCodigo_bloco(); ?></td>
                                <td><?= $recibo->getCodigo_unidade(); ?></td>
                                <td class="bg-success"><?= $recibo->getEmissao(); ?></td>
                                <td><?= $recibo->getNum_recibo_gosoft(); ?></td>
                                <td><?= $recibo->getNum_recibo_union(); ?></td>
                                <td><?= ($recibo->getData_geracao() instanceof \DateTime ? $recibo->getData_geracao()->format('d/m/Y') : ''); ?></td>
                                <td class="bg-info"><?= $recibo->getValor_total(); ?></td>
                            </tr>
                            <tr>
                                <th class="text-right bg-olive">Lançamentos:</th>
                                <td colspan="7" class=" bg-olive">
                                    <table class="table table-responsive table-striped table-hover text-black">
                                        <thead>
                                            <tr>
                                                <th style="width: 13%">Lançamento</th>
                                                <th>Emissão</th>
                                                <th style="width: 16%">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($recibo->getLancamentos()):
                                                foreach ($recibo->getLancamentos() as $lancamento):
                                                    ?>
                                                    <tr>
                                                        <td><?= $lancamento->getLancamento(); ?></td>
                                                        <td class="<?=$lancamento->getEmissao() == $recibo->getEmissao() ? 'bg-success' : 'bg-warning';?>"><?= $lancamento->getEmissao(); ?></td>
                                                        <td class="bg-danger bg-"><?= $lancamento->getValor(); ?></td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
ob_start();
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#cbCondominio').select2();
    });
    $('#cbCondominio').on('keyup change', function () {
        $('#frmFiltro').submit();
    });
</script>
<?php
$script = ob_get_clean();
$this
        ->addInlineScript($script, true, 9999);
?>