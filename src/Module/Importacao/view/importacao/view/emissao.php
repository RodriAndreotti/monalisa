<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$emissoes = $this->get('emissoes');
$condominios = $this->get('condominios');
$selected = $this->get('selected');
?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3 class="box-title">Emissões</h3>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <?= \Core\Util\Message::getMessages(); ?>
                <form name="frmFiltroCondominio" method="post" id="frmFiltro">
                    <div class="form-group">
                        <label for="cbCondominio">Filtrar por condomínio: </label>
                        <select name="condominio" id="cbCondominio" class="form-control form-control-line">
                            <option value="">Todos</option>
                            <?php foreach ($condominios as $condominio): ?>
                                <option 
                                    value="<?= $condominio->getCodUnionWeb(); ?>"
                                    <?= ($condominio->getCodUnionWeb() == $selected ? ' selected="selected"' : ''); ?>>
                                        <?= $condominio->getNome(); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </form>
                <table class="table table-responsive table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Condomínio</th>
                            <th>Código</th>
                            <th>Emissão (UnionWeb)</th>
                            <th>Vencimento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($emissoes as $emissao): ?>
                            <tr>
                                <td><?= $emissao->getCondominio() ? $emissao->getCondominio()->getNome() : 'Condomínio nao identificado'; ?></td>
                                <td><?= $emissao->getCodigo(); ?></td>
                                <td><?= $emissao->getEmissao(); ?></td>
                                <td><?= $emissao->getVencimento()->format('d/m/Y'); ?></td>
                            </tr>
                            <tr>
                                <th class="text-right">Lançamentos:</th>
                                <td colspan="3">
                                    <table class="table table-responsive table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Lançamento</th>
                                                <th>Cód. Conta</th>
                                                <th>Histórico</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($emissao->getLancamentos() as $lancamento): ?>
                                                <tr>
                                                    <td><?= $lancamento->getLancamento(); ?></td>
                                                    <td><?= $lancamento->getConta(); ?></td>
                                                    <td><?= $lancamento->getHistorico(); ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
ob_start();
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#cbCondominio').select2();
    });
    $('#cbCondominio').on('keyup change', function () {
        $('#frmFiltro').submit();
    });
</script>
<?php
$script = ob_get_clean();
$this
        ->addInlineScript($script, true, 9999);
?>