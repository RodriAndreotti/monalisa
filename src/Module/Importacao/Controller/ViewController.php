<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Controller;

/**
 * Controller para visualizar as importações de emissão e recibo
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ViewController extends \Core\Controller\AbstractController
{
    /**
     * Visualização das emissões
     * @return \Core\Helper\Model\ViewModel
     */
    public function emissao()
    {
        $repository = $this->getServiceManager()->get(\Importacao\Repository\EmissaoRepository::class);
        $lancRepository = $this->getServiceManager()->get(\Importacao\Repository\LancamentoEmissaoRepository::class);
        $condRepository = $this->getServiceManager()->get(\Associacao\Repository\CondominioRepository::class);
        
        
        
        $request = $this->getRequest();
        
        $postData = $request->getPost();
        $selected = $postData ? filter_var($postData['condominio'], FILTER_SANITIZE_STRING) : null;
        
        if($selected) {
            $emissoes = $repository->listarPorCondominio($selected);
        } else {
            $emissoes = $repository->listar();
        }
        
        $condominios = $condRepository->listar();
        
        
        foreach($emissoes as &$recibo) {
            $recibo->setLancamentos($lancRepository->listarPorEmissao($recibo));
            $recibo->setCondominio($condRepository->obterPorCodUnion($recibo->getCondominio()));
            $recibo->setVencimento(\DateTime::createFromFormat('Y-m-d', $recibo->getVencimento()));
        }
        
        return new \Core\Helper\Model\ViewModel(array(
            'emissoes'  =>  $emissoes,
            'condominios'   =>  $condominios,
            'selected'  =>  $selected
        ));
    }
    
    /**
     * Visuaização das Emissões
     * @return \Core\Helper\Model\ViewModel
     */
    public function recibo()
    {
        $repository = $this->getServiceManager()->get(\Importacao\Repository\ReciboRepository::class);
        $lancRepository = $this->getServiceManager()->get(\Importacao\Repository\LancamentoReciboRepository::class);
        $condRepository = $this->getServiceManager()->get(\Associacao\Repository\CondominioRepository::class);
        
        
        
        $request = $this->getRequest();
        
        $postData = $request->getPost();
        $selected = $postData ? filter_var($postData['condominio'], FILTER_SANITIZE_STRING) : null;
        
        if($selected) {
            $recibos = $repository->listarPorCondominio($selected);
        } else {
            $recibos = $repository->listar();
        }
        
        
        $condominios = $condRepository->listar();
        
        
        foreach($recibos as &$recibo) {
            $recibo->setLancamentos($lancRepository->listarPorRecibo($recibo));
            $recibo->setCodigo_condominio($condRepository->obterPorCodUnion($recibo->getCodigo_condominio()));
            $recibo->setData_geracao(\DateTime::createFromFormat('Y-m-d', $recibo->getData_geracao()));
        }
        
        return new \Core\Helper\Model\ViewModel(array(
            'recibos'  =>  $recibos,
            'condominios'   =>  $condominios,
            'selected'  =>  $selected
        ));
    }
}
