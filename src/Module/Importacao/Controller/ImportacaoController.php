<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Controller;

/**
 * Controller que realiza a importação dos Excel
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ImportacaoController extends \Core\Controller\AbstractController
{

    private $xlsArray;

    public function index()
    {
        $post = $this->getRequest()->getPost();

        if (isset($_FILES['arquivo'])) {
            if (is_uploaded_file($_FILES['arquivo']['tmp_name'])) {

                $file = $_FILES['arquivo'];
                $file_mimes = array(
                    'application/octet-stream',
                    'application/vnd.ms-excel',
                    'application/csv',
                    'application/excel',
                    'application/vnd.msexcel',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                if (in_array($file['type'], $file_mimes)) {
                    if (pathinfo($file['name'], PATHINFO_EXTENSION) == 'xls') {
                        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                    } elseif (pathinfo($file['name'], PATHINFO_EXTENSION) == 'xlsx') {
                        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                    }
                    $excelFile = $reader->load($file['tmp_name']);
                    $iterator = $excelFile->getActiveSheet()->getRowIterator();

                    $this->xlsArray = $excelFile->getActiveSheet()->toArray();



                    if (trim($post['tipo']) == \Associacao\Enum\TipoAssociacao::TIPO_CONDOMINIO) {
                        $this->proccessCondominioExcel();
                    } elseif (trim($post['tipo'] == \Associacao\Enum\TipoAssociacao::TIPO_UNIDADE)) {
                        $this->proccessUnidadeExcel();
                    } elseif (trim($post['tipo'] == \Associacao\Enum\TipoAssociacao::TIPO_EMISSAO)) {
                        $this->proccessEmissaoExcel();
                    } elseif (trim($post['tipo'] == \Associacao\Enum\TipoAssociacao::TIPO_RECIBO)) {
                        $this->proccessReciboExcel();
                    } elseif (trim($post['tipo']) == \Associacao\Enum\TipoAssociacao::TIPO_LANCAMENTO_RECIBO) {
                        $this->proccessLancamentoReciboExcel();
                    }
                }
            } else {
                \Core\Util\Message::setMessage('Houve um erro ao enviar o arquivo.', 'danger');
            }
        }

        return new \Core\Helper\Model\ViewModel();
    }

    private function proccessCondominioExcel()
    {

        $cr = $this->getServiceManager()->get(\Associacao\Repository\CondominioRepository::class);

        $coordCond = $this->findCoordinate('Condomínio:');
        $coordMulta = $this->findCoordinate('Multa sobre Atraso (%):');
        $coordJuros = $this->findCoordinate('Percentual de Juros ao Mês:');
        $coordCedente = $this->findCoordinate('Cedente:');
        $coordDiasLimite = $this->findCoordinate('Quantidade de dias para Data Limite:');

        $cond = new \Associacao\Entity\Condominio();
        $cond->setCodUnionWeb($this->xlsArray[$coordCond['row']][$coordCond['col'] + 2]);
        $cond->setNome($this->xlsArray[$coordCond['row']][$coordCond['col'] + 4]);
        $cond->setMulta($this->xlsArray[$coordMulta['row']][$coordMulta['col'] + 2]);
        $cond->setJuros($this->xlsArray[$coordJuros['row']][$coordJuros['col'] + 1]);
        $cedente = ($this->xlsArray[$coordCedente['row']][$coordCedente['col'] + 1] ?: $this->xlsArray[$coordCedente['row']][$coordCedente['col'] + 2]);
        $cond->setCedente($cedente);
        $cond->setDias_limite($this->xlsArray[$coordDiasLimite['row']][$coordDiasLimite['col'] + 1]);


        $result = $cr->salvar($cond);
        if ($result instanceof \Associacao\Entity\Condominio) {
            \Core\Util\Message::setMessage('Importação finalizada com sucesso', 'success');
        } else {
            \Core\Util\Message::setMessage('Houve um erro na importação', 'danger');
        }

        return $this->redirect()->toRoute('associacao', array('action' => 'condominio'));
    }

    public function proccessEmissaoExcel()
    {
        $coordCond = $this->findCoordinate('Condomínio:');
        //$coordTEmitidos = $this->findCoordinate('Total Emitidos:');

        $condNext = 1;
        do {
            $condominio = $this->xlsArray[$coordCond['row']][$coordCond['col'] + $condNext];

            $condNext++;
        } while (!is_numeric($condominio));



        $er = $this->getServiceManager()->get(\Importacao\Repository\EmissaoRepository::class);
        // $ler = $this->getServiceManager()->get(\Importacao\Repository\LancamentoEmissaoRepository::class);


        while ($cols = array_shift($this->xlsArray)) { // Retira a primeira linha do array
            $cols = array_map('trim', $cols);
            if (trim($cols[0]) == 'Emissão') {
                if ($emissao) {
                    $result = $er->salvar($emissao);
                }
                $emissao = new \Importacao\Entity\Emissao();
                $emissao->setCondominio($condominio);
                /* $coordVenc = $this->findCoordinate('Vencimento');
                  $coordHist = $this->findCoordinate('Histórico');
                  $coordConta = $this->findCoordinate('Conta'); */
                $colVenc = array_search('Vencimento', $cols);
                $colHist = array_search('Histórico', $cols);
                $colConta = array_search('Conta', $cols);
                $i = 0;
                while (!is_numeric($this->xlsArray[$i][0])) {
                    $i++;
                }
                $emissao->setEmissao($this->xlsArray[$i][0]); // Como a linha atual foi removida do array, a próxima linha passa a ser a linha 0
                $emissao->setVencimento(\DateTime::createFromFormat('d/m/Y', $this->xlsArray[$i][$colVenc]));


                $numLancamento = 1;
            } elseif ($colConta &&
                    $colHist &&
                    !in_array('Cancelados:', $cols) &&
                    !in_array('Total Emitidos:', $cols) &&
                    $cols[0] != 'Sistemas  Union Data' &&
                    $cols[$colHist] != '') {

                $lancamento = new \Importacao\Entity\LancamentoEmissao();
                $lancamento->setLancamento($numLancamento);
                $conta = is_numeric($cols[$colConta]) ? $cols[$colConta] : (is_numeric($cols[$colConta - 1]) ? $cols[$colConta - 1] : $cols[$colConta - 2]);
                $lancamento->setConta($conta);
                $lancamento->setHistorico($cols[$colHist]);
                $numLancamento++;
                $emissao->addLancamento($lancamento);
            }
        }

        if ($emissao) {
            $result = $er->salvar($emissao);
        }
        return $this->redirect()->toRoute('view', array('action' => 'emissao'));
    }

    private function findCoordinate($value, $startLine = 0, $maxLines = null)
    {
        if ($startLine < count($this->xlsArray)) {
            $nLines = count($this->xlsArray);
            $count = (is_null($maxLines) || $maxLines > $nLines ? $nLines : $startLine + $maxLines);
            for ($row = $startLine; $row < $count; $row++) {
                $cols = $this->xlsArray[$row];

                $col = array_search($value, $cols);

                if ($col !== false) {
                    return array('row' => $row, 'col' => $col);
                }
                /* foreach ($cols as $col => $valor) {
                  if (trim($valor) == $value) {
                  return array('row' => $row, 'col' => $col);
                  }
                  } */
            }
        }


        return null;
    }

    public function proccessUnidadeExcel()
    {
        $coordCond = $this->findCoordinate('Condomínio:');
        $condParts = explode('-', $this->xlsArray[$coordCond['row']][$coordCond['col'] + 1]);
        $condominio = ltrim(trim($condParts[0]), '0');

        $ur = $this->getServiceManager()->get(\Associacao\Repository\UnidadeRepository::class);

        while ($cols = array_shift($this->xlsArray)) {
            $cols = array_map('trim', $cols);
            if ($cols[0] == 'Unid') {
                $unidParts = array_map('trim', explode('/', $cols[1]));
                $bloco = $unidParts[0];
                $unid = $unidParts[1];
                $unidade = new \Associacao\Entity\Unidade();
                $unidade->setCondominio($condominio);
                $unidade->setBloco($bloco);
                $unidade->setUnidade($unid);

                $ur->salvar($unidade);
            }
        }
        return $this->redirect()->toRoute('associacao', array('action' => 'unidade', 'id' => $condominio));
    }

    public function proccessReciboExcel()
    {
        $condominio = '';


        $coordEmissao = $this->findCoordinate('Emissão');
        $coordBloco = $this->findCoordinate('Bl.');
        $coordUnidade = $this->findCoordinate('Unidade');
        $coordRecibo = $this->findCoordinate('Recibo');
        $coordVenc = $this->findCoordinate('Vencto');

        $coordValorTotal = $this->findCoordinate('Valor');

        $reciboRepository = $this->getServiceManager()->get(\Importacao\Repository\ReciboRepository::class);
        $lancEmissaoRepository = $this->getServiceManager()->get(\Importacao\Repository\LancamentoEmissaoRepository::class);
        $lancReciboRepository = $this->getServiceManager()->get(\Importacao\Repository\LancamentoReciboRepository::class);

        $first = true;
        while ($cols = array_shift($this->xlsArray)) {
            if ($first) {
                if (strpos($cols[0], 'Condomínio:') !== false) {
                    $condParts = explode('-', $cols[0]);

                    if (count($condParts) < 2) {
                        $i = 1;
                        do {
                            $condominio = $cols[$i];
                            $i++;
                        } while (!is_numeric($condominio));
                    } else {
                        $condominio = (int) trim(str_replace('Condomínio:', '', $condParts[0]));
                    }

                    $first = false;
                }
            } elseif (strpos($cols[0], 'Condomínio:') === false && $cols[$coordBloco['col']] != 'Bl.' && is_numeric($cols[$coordRecibo['col']])) {

                if ($cols[$coordBloco['col']] != '') {
                    $bloco = $cols[$coordBloco['col']];
                    $unidade = $cols[$coordUnidade['col']];
                }

                $vencimento = \DateTime::createFromFormat('d/m/Y', $cols[$coordVenc['col']]);
                $geracao = $vencimento->sub(new \DateInterval('P15D'));

                $recibo = new \Importacao\Entity\Recibo();
                //$recibo->setData_geracao($data_geracao);
                $emissao = $cols[$coordEmissao['col']];
                $recibo->setEmissao($emissao);
                $recibo->setCodigo_condominio($condominio);
                $recibo->setCodigo_bloco($bloco);
                $recibo->setData_geracao($geracao);
                $recibo->setCodigo_unidade($unidade);
                $recibo->setNum_recibo_union($cols[$coordRecibo['col']]);
                $recibo->setValor_total(str_replace(array(',??', '??'), '', $cols[$coordValorTotal['col'] - 1]));


                //$recibo->setDa($coordDescontoJuros['col']);

                $reciboRepository->salvar($recibo);

                /* $lancsEmissao = $lancEmissaoRepository->listarPorEmissaoUnionData($emissao);


                  if($lancsEmissao) {
                  foreach($lancsEmissao as $lEmissao) {
                  $lancamento = new \Importacao\Entity\LancamentoRecibo();
                  $lancamento->setRecibo($recibo);
                  $lancamento->setEmissao($cols[$coordEmissao['col']]);
                  $lancamento->setLancamento($lEmissao->getLancamento());

                  $lancReciboRepository->salvar($lancamento);

                  }
                  }
                  $lancamento = new \Importacao\Entity\LancamentoRecibo();
                  $lancamento->setRecibo($recibo);
                  $lancamento->setEmissao($cols[$coordEmissao['col']]); */
            }
        }
        return $this->redirect()->toRoute('importacao', array('action' => 'index'));
    }

    public function proccessLancamentoReciboExcel()
    {


        $reciboRep = $this->getServiceManager()->get(\Importacao\Repository\ReciboRepository::class);

        $lancEmissaoRepository = $this->getServiceManager()->get(\Importacao\Repository\LancamentoEmissaoRepository::class);
        $lancReciboRepository = $this->getServiceManager()->get(\Importacao\Repository\LancamentoReciboRepository::class);

        $first = true;
        $x = 0;
        while ($cols = array_shift($this->xlsArray)) {
            // if (strpos($cols[8], 'Quantidade de unidades:') !== false || $x == 0) {
            if (strpos($cols[1], 'Condomínio:') !== false || $x == 0) {
                $data = null;
                $data = $this->getReciboHeader();

                $coordEmissao = array_search('Emissão:', $cols);
                $i = $coordEmissao + 1;

                if ($coordEmissao) {
                    do {
                        $emissao = $cols[$i];
                        $i++;
                    } while (!is_numeric($emissao));
                }


                if ($first) {
                    if (strpos($cols[1], 'Condomínio:') !== false) {
                        $condParts = explode('-', $cols[1]);

                        if (count($condParts) < 2) {
                            $i = 2;
                            do {
                                $condominio = $cols[$i];
                                $i++;
                            } while (!is_numeric($condominio));
                        } else {
                            $condominio = (int) trim(str_replace('Condomínio:', '', $condParts[0]));
                        }



                        $first = false;
                    }
                }
            } else {

                $recibo = $cols[$data['coordRecibo']['col']];

                if (is_numeric($recibo)) {
                    if ($reciboRep->obterPorCodUnionWeb($recibo)) {
                        if ($data['codConta1']) {

                            $valor = trim(trim($cols[$data['coordVerba1']['col']], '?'), ',');

                            

                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta1'], $data['desc1']);
                                
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);



                                $lancReciboRepository->salvar($lancamento);



                                unset($lancamento);
                            }

                            unset($valor);
                        }

                        if ($data['codConta2']) {
                            $valor = trim(trim($cols[$data['coordVerba2']['col']], '?'), ',');
                            if ($valor) {
                               
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta2'], $data['desc2']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);


                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }
                            unset($valor);
                        }
                        if ($data['codConta3']) {
                            $valor = trim(trim($cols[$data['coordVerba3']['col']], '?'), ',');
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta3'], $data['desc3']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }

                            unset($valor);
                        }
                        if ($data['codConta4']) {
                            $valor = trim(trim($cols[$data['coordVerba4']['col']], '?'), ',');
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta4'], $data['desc4']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }

                            unset($valor);
                        }

                        if ($data['codConta5']) {
                            $valor = trim(trim($cols[$data['coordVerba5']['col']], '?'), ',');
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta5'], $data['desc5']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }

                            unset($valor);
                        }
                        if ($data['codConta6']) {

                            $valor = trim(trim($cols[$data['coordVerba6']['col']], '?'), ',');
                            
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta6'], $data['desc6']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);
                                

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }

                            unset($valor);
                        }
                        if ($data['codConta7']) {
                            $valor = trim(trim($cols[$data['coordVerba7']['col']], '?'), ',');
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta7'], $data['desc7']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }

                            unset($valor);
                        }
                        if ($data['codConta8']) {
                            $valor = trim(trim($cols[$data['coordVerba8']['col']], '?'), ',');
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta8'], $data['desc8']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }

                            unset($valor);
                        }
                        if ($data['codConta9']) {
                            $valor = trim(trim($cols[$data['coordVerba9']['col']], '?'), ',');
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta9'], $data['desc9']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }

                            unset($valor);
                        }
                        if ($data['codConta10']) {

                            $valor = trim(trim($cols[$data['coordVerba10']['col']], '?'), ',');
                            if ($valor) {
                                $lancEmissao = $lancEmissaoRepository->obterLancPorEmissaoConta($condominio, $emissao, $data['codConta10'], $data['desc10']);
                                $lancamento = new \Importacao\Entity\LancamentoRecibo();
                                $lancamento->setRecibo($recibo);
                                $lancamento->setLancamento($lancEmissao);
                                $lancamento->setEmissao($emissao);
                                $lancamento->setValor($valor);

                                $lancReciboRepository->salvar($lancamento);

                                unset($lancamento);
                            }
                            unset($valor);
                        }
                    }
                }
            }
            $x++;
        }


        /* $lancamento = new \Importacao\Entity\LancamentoRecibo();
          $lancamento->setRecibo($recibo);
          $lancamento->setEmissao($cols[$coordEmissao['col']]);
         */
        return $this->redirect()->toRoute('importacao', array('action' => 'index'));
    }

    private function getReciboHeader($linha = 0)
    {
        if (count($this->xlsArray) > 1) {


            $coordVerba1 = $this->findCoordinate('verba 1', $linha, 20);
            $coordVerba2 = $this->findCoordinate('verba 2', $linha, 20);
            $coordVerba3 = $this->findCoordinate('verba 3', $linha, 20);
            $coordVerba4 = $this->findCoordinate('verba 4', $linha, 20);
            $coordVerba5 = $this->findCoordinate('verba 5', $linha, 20);
            $coordVerba6 = $this->findCoordinate('verba 6', $linha, 20);
            $coordVerba7 = $this->findCoordinate('verba 7', $linha, 20);
            $coordVerba8 = $this->findCoordinate('verba 8', $linha, 20);
            $coordVerba9 = $this->findCoordinate('verba 9', $linha, 20);
            $coordVerba10 = $this->findCoordinate('verba 10', $linha, 20);

            $coordConta = $this->findCoordinate('Conta', $linha, 20);

            $codConta1 = null;
            if ($coordVerba1) {
                $codConta1 = $this->xlsArray[$coordVerba1['row']][$coordConta['col']];
                $desc1 = $this->xlsArray[$coordVerba1['row']][$coordConta['col']+1];
            }
            $codConta2 = null;
            if ($coordVerba2) {
                $codConta2 = $this->xlsArray[$coordVerba2['row']][$coordConta['col']];
                $desc2 = $this->xlsArray[$coordVerba2['row']][$coordConta['col']+1];
            }
            $codConta3 = null;
            if ($coordVerba3) {
                $codConta3 = $this->xlsArray[$coordVerba3['row']][$coordConta['col']];
                $desc3 = $this->xlsArray[$coordVerba3['row']][$coordConta['col']+1];
            }

            $codConta4 = null;
            if ($coordVerba4) {
                $codConta4 = $this->xlsArray[$coordVerba4['row']][$coordConta['col']];
                $desc4 = $this->xlsArray[$coordVerba4['row']][$coordConta['col']+1];
            }

            $codConta5 = null;
            if ($coordVerba5) {
                $codConta5 = $this->xlsArray[$coordVerba5['row']][$coordConta['col']];
                $desc5 = $this->xlsArray[$coordVerba5['row']][$coordConta['col']+1];
            }

            $codConta6 = null;
            if ($coordVerba6) {
                $codConta6 = $this->xlsArray[$coordVerba6['row']][$coordConta['col']];
                $desc6 = $this->xlsArray[$coordVerba6['row']][$coordConta['col']+1];
            }

            $codConta7 = null;
            if ($coordVerba7) {
                $codConta7 = $this->xlsArray[$coordVerba7['row']][$coordConta['col']];
                $desc7 = $this->xlsArray[$coordVerba7['row']][$coordConta['col']+1];
            }

            $codConta8 = null;
            if ($coordVerba8) {
                $codConta8 = $this->xlsArray[$coordVerba8['row']][$coordConta['col']];
                $desc8 = $this->xlsArray[$coordVerba8['row']][$coordConta['col']+1];
            }

            $codConta9 = null;
            if ($coordVerba9) {
                $codConta9 = $this->xlsArray[$coordVerba9['row']][$coordConta['col']];
                $desc9 = $this->xlsArray[$coordVerba9['row']][$coordConta['col']+1];
            }

            $codConta10 = null;
            if ($coordVerba10) {
                $codConta10 = $this->xlsArray[$coordVerba10['row']][$coordConta['col']];
                $desc10 = $this->xlsArray[$coordVerba10['row']][$coordConta['col']+1];
            }

            $coordRecibo = $this->findCoordinate('Recibo');


            $coordVerba1 = $this->findCoordinate('Verba 1', $coordRecibo['row']);
            $coordVerba2 = $this->findCoordinate('Verba 2', $coordRecibo['row']);
            $coordVerba3 = $this->findCoordinate('Verba 3', $coordRecibo['row']);
            $coordVerba4 = $this->findCoordinate('Verba 4', $coordRecibo['row']);
            $coordVerba5 = $this->findCoordinate('Verba 5', $coordRecibo['row']);
            $coordVerba6 = $this->findCoordinate('Verba 6', $coordRecibo['row']);
            $coordVerba7 = $this->findCoordinate('Verba 7', $coordRecibo['row']);
            $coordVerba8 = $this->findCoordinate('Verba 8', $coordRecibo['row']);
            $coordVerba9 = $this->findCoordinate('Verba 9', $coordRecibo['row']);
            $coordVerba10 = $this->findCoordinate('Verba 10', $coordRecibo['row']);




            return array(
                // Códigos
                'codConta1' => $codConta1,
                'codConta2' => $codConta2,
                'codConta3' => $codConta3,
                'codConta4' => $codConta4,
                'codConta5' => $codConta5,
                'codConta6' => $codConta6,
                'codConta7' => $codConta7,
                'codConta8' => $codConta8,
                'codConta9' => $codConta9,
                'codConta10' => $codConta10,
                // Coords
                'coordVerba1' => $coordVerba1,
                'coordVerba2' => $coordVerba2,
                'coordVerba3' => $coordVerba3,
                'coordVerba4' => $coordVerba4,
                'coordVerba5' => $coordVerba5,
                'coordVerba6' => $coordVerba6,
                'coordVerba7' => $coordVerba7,
                'coordVerba8' => $coordVerba8,
                'coordVerba9' => $coordVerba9,
                'coordVerba10' => $coordVerba10,
                'coordRecibo' => $coordRecibo,
                
                // Desc
                'desc1' => $desc1,
                'desc2' => $desc2,
                'desc3' => $desc3,
                'desc4' => $desc4,
                'desc5' => $desc5,
                'desc6' => $desc6,
                'desc7' => $desc7,
                'desc8' => $desc8,
                'desc9' => $desc9,
                'desc10' => $desc10
            );
        }
    }

}
