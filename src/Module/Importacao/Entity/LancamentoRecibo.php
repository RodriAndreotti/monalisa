<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Entity;

/**
 * Entidade dos lançamentos do Recibo
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class LancamentoRecibo
{

    private $recibo;
    private $emissao;
    private $lancamento;
    private $compHistorico;
    private $sinalIndValor;
    private $valor;
    private $valorOriginal;
    private $valorDesconto;

    public function getRecibo()
    {
        return $this->recibo;
    }

    public function getEmissao()
    {
        return $this->emissao;
    }

    public function getLancamento()
    {
        return $this->lancamento;
    }

    public function getCompHistorico()
    {
        return $this->compHistorico;
    }

    public function getSinalIndValor()
    {
        return $this->sinalIndValor;
    }

    public function getValor()
    {
        return $this->valor;
    }

    public function getValorOriginal()
    {
        return $this->valorOriginal;
    }

    public function getValorDesconto()
    {
        return $this->valorDesconto;
    }

    public function setRecibo($recibo)
    {
        $this->recibo = $recibo;
        return $this;
    }

    public function setEmissao($emissao)
    {
        $this->emissao = $emissao;
        return $this;
    }

    public function setLancamento($lancamento)
    {
        $this->lancamento = $lancamento;
        return $this;
    }

    public function setCompHistorico($compHistorico)
    {
        $this->compHistorico = $compHistorico;
        return $this;
    }

    public function setSinalIndValor($sinalIndValor)
    {
        $this->sinalIndValor = $sinalIndValor;
        return $this;
    }

    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    public function setValorOriginal($valorOriginal)
    {
        $this->valorOriginal = $valorOriginal;
        return $this;
    }

    public function setValorDesconto($valorDesconto)
    {
        $this->valorDesconto = $valorDesconto;
        return $this;
    }

    public function toStringForLine($assocTab)
    {
        $line = '';
        if ($assocTab['emissao'][$this->emissao]) {
            $line .= \Core\Util\Text::mb_str_pad($assocTab['recibo'][($this->recibo instanceof Recibo ? $this->recibo->getNum_recibo_union() : $this->recibo)], 8, '0', STR_PAD_LEFT); // Número do Recibo

            $line .= \Core\Util\Text::mb_str_pad($assocTab['emissao'][$this->emissao], 6, '0', STR_PAD_LEFT); // Emissão

            $line .= \Core\Util\Text::mb_str_pad($this->lancamento, 2, '0', STR_PAD_LEFT); // Lançamento

            $line .= \Core\Util\Text::mb_str_pad(mb_substr($this->historico, 0, 15, 'UTF-8'), 15, ' ', STR_PAD_RIGHT); // Histórico

            $line .= \Core\Util\Text::mb_str_pad(($this->valor < 0 ? 1 : 0), 1, '0', STR_PAD_LEFT); // Sinal indicador de Valor
            // Valor Total
            $valorParts = explode('.', str_replace(',', '', str_replace('-', '', $this->valor)));
            $valorTotal = \Core\Util\Text::mb_str_pad($valorParts[0], 10, '0', STR_PAD_LEFT) . \Core\Util\Text::mb_str_pad($valorParts[1], 2, '0', STR_PAD_RIGHT);
            $line .= $valorTotal;
            // Valor Total
            // Valor Original
            $valorOriginalParts = explode('.', str_replace(',', '', str_replace('-', '', $this->valor)));
            $valorOriginal = \Core\Util\Text::mb_str_pad($valorOriginalParts[0], 12, '0', STR_PAD_LEFT) . \Core\Util\Text::mb_str_pad($valorOriginalParts[1], 2, '0', STR_PAD_RIGHT);
            $line .= $valorOriginal;
            // Valor Original
            // Valor Desconto
            $valorDescontoParts = explode('.', str_replace(',', '', $this->valorDesconto));
            $valorDesconto = \Core\Util\Text::mb_str_pad($valorDescontoParts[0], 10, '0', STR_PAD_LEFT) . \Core\Util\Text::mb_str_pad($valorDescontoParts[1], 2, '0', STR_PAD_RIGHT);
            $line .= $valorDesconto;
            // Valor Desconto
        }

        return $line;
    }

}
