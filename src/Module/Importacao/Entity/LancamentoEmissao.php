<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Entity;

/**
 * Description of LancamentoEmissao
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class LancamentoEmissao
{
    private $emissao;
    private $lancamento;
    private $conta;
    private $historico;
    
    public function getEmissao()
    {
        return $this->emissao;
    }

    public function getLancamento()
    {
        return $this->lancamento;
    }

    public function getConta()
    {
        return $this->conta;
    }

    public function getHistorico()
    {
        return $this->historico;
    }

    public function setEmissao($emissao)
    {
        $this->emissao = $emissao;
        return $this;
    }

    public function setLancamento($lancamento)
    {
        $this->lancamento = $lancamento;
        return $this;
    }

    public function setConta($conta)
    {
        $this->conta = $conta;
        return $this;
    }

    public function setHistorico($historico)
    {
        $this->historico = $historico;
        return $this;
    }
    
    public function toStringForLine($assocTab)
    {
        $line = '';
        
        $line .= \Core\Util\Text::mb_str_pad($this->emissao, 6, '0', STR_PAD_LEFT); // Emissão
        $line .= \Core\Util\Text::mb_str_pad($this->lancamento, 2, '0', STR_PAD_LEFT); // Lançamento
        $line .= \Core\Util\Text::mb_str_pad($assocTab['conta'][$this->conta], 8, '0', STR_PAD_LEFT); // Conta
        $line .= \Core\Util\Text::mb_str_pad('0', 4, '0', STR_PAD_LEFT); // Taxa de administração
        $line .= \Core\Util\Text::mb_str_pad(mb_substr($this->historico, 0, 28, 'UTF-8'), 28, ' ', STR_PAD_RIGHT); // Histórico
        $line .= \Core\Util\Text::mb_str_pad('0', 1, '0', STR_PAD_LEFT); // Tipo de desconto
        $line .= \Core\Util\Text::mb_str_pad('0', 16, '0', STR_PAD_LEFT); // Desconto
        
        
        return $line;
    }
}
