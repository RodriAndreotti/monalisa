<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Entity;

/**
 * Entidade para emissão
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Emissao
{

    private $codigo;
    private $condominio;
    private $emissao;
    private $vencimento;
    private $desconto_ate;
    private $limite_pagamento;
    private $primeira_multa;
    private $limite_pmulta;
    private $segunda_multa;
    private $multa_diaria;
    private $limite_multad;
    private $juros_diario;
    private $dias_prazo;
    private $codigo_conta;
    private $codigo_carteira;
    private $lancamentos;

    public function __construct()
    {
        $this->lancamentos = array();
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    public function getCondominio()
    {
        return $this->condominio;
    }

    public function getEmissao()
    {
        return $this->emissao;
    }

    public function getVencimento()
    {
        return $this->vencimento;
    }

    public function getDesconto_ate()
    {
        return $this->desconto_ate;
    }

    public function getLimite_pagamento()
    {
        return $this->limite_pagamento;
    }

    public function getPrimeira_multa()
    {
        return $this->primeira_multa;
    }

    public function getLimite_pmulta()
    {
        return $this->limite_pmulta;
    }

    public function getSegunda_multa()
    {
        return $this->segunda_multa;
    }

    public function getMulta_diaria()
    {
        return $this->multa_diaria;
    }

    public function getLimite_multad()
    {
        return $this->limite_multad;
    }

    public function getJuros_diario()
    {
        return $this->juros_diario;
    }

    public function getDias_prazo()
    {
        return $this->dias_prazo;
    }

    public function getCodigo_conta()
    {
        return $this->codigo_conta;
    }

    public function getCodigo_carteira()
    {
        return $this->codigo_carteira;
    }

    public function setCondominio($condominio)
    {
        $this->condominio = $condominio;
        return $this;
    }

    public function setEmissao($emissao)
    {
        $this->emissao = $emissao;
        return $this;
    }

    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;
        return $this;
    }

    public function setDesconto_ate($desconto_ate)
    {
        $this->desconto_ate = $desconto_ate;
        return $this;
    }

    public function setLimite_pagamento($limite_pagamento)
    {
        $this->limite_pagamento = $limite_pagamento;
        return $this;
    }

    public function setPrimeira_multa($primeira_multa)
    {
        $this->primeira_multa = $primeira_multa;
        return $this;
    }

    public function setLimite_pmulta($limite_pmulta)
    {
        $this->limite_pmulta = $limite_pmulta;
        return $this;
    }

    public function setSegunda_multa($segunda_multa)
    {
        $this->segunda_multa = $segunda_multa;
        return $this;
    }

    public function setMulta_diaria($multa_diaria)
    {
        $this->multa_diaria = $multa_diaria;
        return $this;
    }

    public function setLimite_multad($limite_multad)
    {
        $this->limite_multad = $limite_multad;
        return $this;
    }

    public function setJuros_diario($juros_diario)
    {
        $this->juros_diario = $juros_diario;
        return $this;
    }

    public function setDias_prazo($dias_prazo)
    {
        $this->dias_prazo = $dias_prazo;
        return $this;
    }

    public function setCodigo_conta($codigo_conta)
    {
        $this->codigo_conta = $codigo_conta;
        return $this;
    }

    public function setCodigo_carteira($codigo_carteira)
    {
        $this->codigo_carteira = $codigo_carteira;
        return $this;
    }

    public function getLancamentos()
    {
        return $this->lancamentos;
    }

    public function setLancamentos($lancamentos)
    {
        $this->lancamentos = $lancamentos;
        return $this;
    }

    public function addLancamento(LancamentoEmissao $lancamento)
    {
        $lancamento->setEmissao($this);
        $this->lancamentos[] = $lancamento;
        return $this;
    }

    public function toStringForLine($assocTab)
    {
        $line = '';

        if ($assocTab['condominio'][$this->condominio]) {
            // Condomínio
            $line .= \Core\Util\Text::mb_str_pad($assocTab['condominio'][$this->condominio]->getCodGoSoft(), 4, '0', STR_PAD_LEFT);

            // Fim do condomínio

            $line .= \Core\Util\Text::mb_str_pad($this->codigo, 6, '0', STR_PAD_LEFT); // Emissão


            if (!$this->vencimento instanceof \DateTime) {
                $this->vencimento = \DateTime::createFromFormat('Y-m-d', $this->vencimento);
                $this->desconto_ate = $this->vencimento;
                $this->limite_pmulta = $this->vencimento;
                $this->limite_ = $this->vencimento;

                $this->limite_pagamento = clone $this->vencimento;
                $this->limite_pagamento->add(new \DateInterval('P' . $assocTab['condominio'][$this->condominio]->getDias_limite() . 'D'));
            }


            $line .= \Core\Util\Text::mb_str_pad($this->vencimento->format('dmY'), 8, '0', STR_PAD_LEFT); // Data de vencimento

            $line .= \Core\Util\Text::mb_str_pad($this->desconto_ate->format('dmY'), 8, '0', STR_PAD_LEFT); // Desconto até

            $line .= \Core\Util\Text::mb_str_pad($this->limite_pagamento->format('dmY'), 8, '0', STR_PAD_LEFT); // Limite de pagamento
            // Primeira Multa
            $multaParts = explode('.', str_replace(',', '', $assocTab['condominio'][$this->condominio]->getMulta()));
            $multa = \Core\Util\Text::mb_str_pad($multaParts[0], 2, '0', STR_PAD_LEFT) . \Core\Util\Text::mb_str_pad($multaParts[1], 2, '0', STR_PAD_RIGHT);

            $line .= \Core\Util\Text::mb_str_pad($multa, 4, '0', STR_PAD_LEFT); // Multa
            $line .= \Core\Util\Text::mb_str_pad($this->limite_pmulta->format('dmY'), 8, '0', STR_PAD_LEFT); // Limite da primeira multa
            // Fim da primeira multa
            // Segunda Multa e multa diária
            $line .= \Core\Util\Text::mb_str_pad('0', 4, '0', STR_PAD_LEFT); // Segunda Multa
            $line .= \Core\Util\Text::mb_str_pad('0', 4, '0', STR_PAD_LEFT); // Multa diária
            $line .= \Core\Util\Text::mb_str_pad(' ', 8, ' ', STR_PAD_LEFT); // Data limite Multa diária
            // Fim da segunda multa e multa diária
            // Juros
            $jurosParts = explode('.', str_replace(',', '', $assocTab['condominio'][$this->condominio]->getJuros()));
            $juros = \Core\Util\Text::mb_str_pad($jurosParts[0], 2, '0', STR_PAD_LEFT) . \Core\Util\Text::mb_str_pad($jurosParts[1], 4, '0', STR_PAD_RIGHT);
            $line .= \Core\Util\Text::mb_str_pad('0', 6, '0', STR_PAD_LEFT); // Juros diário
            // Fim dos juros

            $line .= \Core\Util\Text::mb_str_pad('0', 2, '0', STR_PAD_LEFT); // Dias de prazo para cobrança de multa 


            $line .= \Core\Util\Text::mb_str_pad($assocTab['condominio'][$this->condominio]->getCodigo_conta(), 4, '0', STR_PAD_LEFT); // Código da conta bancária de cobrança

            $line .= \Core\Util\Text::mb_str_pad($assocTab['condominio'][$this->condominio]->getCodigo_carteira(), 8, ' ', STR_PAD_RIGHT); // Código da carteira
        }

        return $line;
    }

}
