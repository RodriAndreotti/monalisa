<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Importacao\Entity;

/**
 * Entidade que representa o recibo
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Recibo
{
    private $num_recibo_gosoft;
    private $num_recibo_union;
    private $emissao;
    private $data_geracao;
    private $codigo_condominio;
    private $codigo_bloco;
    private $codigo_unidade;
    private $numero_bancario = '';
    private $qtd_dias_prazo = 0;
    private $isento_pagamento = 0;
    private $desconto_total;
    private $valor_total;
    
    private $lancamentos;
    
    public function getNum_recibo_union()
    {
        return $this->num_recibo_union;
    }

    public function getEmissao()
    {
        return $this->emissao;
    }

    public function getData_geracao()
    {
        return $this->data_geracao;
    }

    public function getCodigo_condominio()
    {
        return $this->codigo_condominio;
    }

    public function getCodigo_bloco()
    {
        return $this->codigo_bloco;
    }

    public function getCodigo_unidade()
    {
        return $this->codigo_unidade;
    }

    public function getNumero_bancario()
    {
        return $this->numero_bancario;
    }

    public function getQtd_dias_prazo()
    {
        return $this->qtd_dias_prazo;
    }

    public function getIsento_pagamento()
    {
        return $this->isento_pagamento;
    }

    public function getDesconto_total()
    {
        return $this->desconto_total;
    }

    public function getValor_total()
    {
        return $this->valor_total;
    }

    public function setNum_recibo_union($num_recibo)
    {
        $this->num_recibo_union = $num_recibo;
        return $this;
    }

    public function setEmissao($emissao)
    {
        $this->emissao = $emissao;
        return $this;
    }

    public function setData_geracao($data_geracao)
    {
        $this->data_geracao = $data_geracao;
        return $this;
    }

    public function setCodigo_condominio($codigo_condominio)
    {
        $this->codigo_condominio = $codigo_condominio;
        return $this;
    }

    public function setCodigo_bloco($codigo_bloco)
    {
        $this->codigo_bloco = $codigo_bloco;
        return $this;
    }

    public function setCodigo_unidade($codigo_unidade)
    {
        $this->codigo_unidade = $codigo_unidade;
        return $this;
    }

    public function setNumero_bancario($numero_bancario)
    {
        $this->numero_bancario = $numero_bancario;
        return $this;
    }

    public function setQtd_dias_prazo($qtd_dias_prazo)
    {
        $this->qtd_dias_prazo = $qtd_dias_prazo;
        return $this;
    }

    public function setIsento_pagamento($isento_pagamento)
    {
        $this->isento_pagamento = $isento_pagamento;
        return $this;
    }

    public function setDesconto_total($desconto_total)
    {
        $this->desconto_total = $desconto_total;
        return $this;
    }

    public function setValor_total($valor_total)
    {
        $this->valor_total = $valor_total;
        return $this;
    }
    
    public function getLancamentos()
    {
        return $this->lancamentos;
    }

    public function setLancamentos($lancamentos)
    {
        $this->lancamentos = $lancamentos;
        return $this;
    }
    
    public function getNum_recibo_gosoft()
    {
        return $this->num_recibo_gosoft;
    }

    public function setNum_recibo_gosoft($num_recibo_gosoft)
    {
        $this->num_recibo_gosoft = $num_recibo_gosoft;
        return $this;
    }
    
    public function toStringForLine($assocTab)
    {
        $line = '';
        
        if($assocTab['condominio'][$this->codigo_condominio] && $assocTab['emissao'][$this->emissao]) {
            if(!$this->data_geracao instanceof \DateTime) {
                $this->data_geracao = \DateTime::createFromFormat('Y-m-d', $this->data_geracao);
            }

            $line .= \Core\Util\Text::mb_str_pad($this->num_recibo_gosoft, 8, '0', STR_PAD_LEFT); // Número do Recibo

            $line .= \Core\Util\Text::mb_str_pad($assocTab['emissao'][$this->emissao], 6, '0', STR_PAD_LEFT); // Emissão

            $line .= \Core\Util\Text::mb_str_pad($this->data_geracao->format('dmY'), 8, '0', STR_PAD_LEFT); // Data geração

            // Condomínio
        

            $line .= \Core\Util\Text::mb_str_pad($assocTab['condominio'][$this->codigo_condominio]->getCodGoSoft(), 4, '0', STR_PAD_LEFT);
            $line .= \Core\Util\Text::mb_str_pad($this->codigo_bloco, 4, ' ', STR_PAD_RIGHT);
            $line .= \Core\Util\Text::mb_str_pad($this->codigo_unidade, 6, ' ', STR_PAD_RIGHT);
            // Fim do condomínio


            $line .= \Core\Util\Text::mb_str_pad($this->numero_bancario, 25, ' ', STR_PAD_RIGHT); // Número bancário

            $line .= \Core\Util\Text::mb_str_pad($this->qtd_dias_prazo, 2, '0', STR_PAD_LEFT); // Dias de prazo
            $line .= \Core\Util\Text::mb_str_pad($this->isento_pagamento, 1, '0', STR_PAD_LEFT); // Isento de pagamento
            $line .= \Core\Util\Text::mb_str_pad($this->desconto_total, 12, '0', STR_PAD_LEFT); // Desconto Total

            // Valor Total
            $valorParts = explode('.', str_replace(',', '', $this->valor_total));
            $valorTotal = \Core\Util\Text::mb_str_pad($valorParts[0], 10, '0', STR_PAD_LEFT) . \Core\Util\Text::mb_str_pad($valorParts['1'], 2, '0', STR_PAD_RIGHT);
            $line .= $valorTotal; 
            // Valor Total

        }
        return $line;
    }
}
