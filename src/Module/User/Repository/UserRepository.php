<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Repository;

use ArrayObject;
use Core\DB\Connection;
use Core\Util\Message;
use Geocoder\Exception\Exception;
use Grade\Repository\ProgramaRepository;
use PDO;
use User\Model\User;
use User\Service\Password;

/**
 * Description of UserRepository
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c)2016, Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class UserRepository
{

    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var ProgramaRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param Connection $conn
     */
    private function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Gerenciador de instância para o repositório
     * @param Connection $conn
     * @return UserRepository
     */
    public static function getInstance(Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }

    /**
     * Realiza o login
     * @return boolean|\app\model\User
     */
    public function doLogin($user)
    {

        // Atrasa a execução do código em 2 segundos, para dificultar ataques de bruteforce
        sleep(2);

        if ($this->existe($user->getLogin())) {
            $passService = new Password();
            $senha = $user->getSenha();

            $userLogado = $this->obterPorLogin($user->getLogin());

            if ($passService->validatePassword($senha, $this->loadSenha($userLogado))) {
                $this->setDataAcesso($userLogado);
                $userLogado->setIsLogado(true);

                Message::setMessage('Login efetuado com sucesso', 'success');

                return $userLogado;
            } else {
                Message::setMessage('Erro ao efetuar login, usuário ou senha inválidos', 'danger');

                return $user;
            }
        } else {
            Message::setMessage('Erro ao efetuar login, usuário ou senha inválidos', 'danger');

            return $user;
        }
    }

    /**
     * Salva um usuário
     * @param IModel $model
     * @return IModel|boolean
     */
    public function inserir($model)
    {
        $sql = 'INSERT INTO login ('
                . 'login,'
                . 'email,'
                . 'senha,'
                . 'nome'
                . ') '
                . 'VALUES ('
                . ':login,'
                . ':email,'
                . ':senha,'
                . ':nome'
                . ')';

        try {
            $query = $this->conn->getHandler()
                    ->prepare($sql);

            $query->bindValue(':login', $model->getLogin(), PDO::PARAM_STR);
            $query->bindValue(':email', $model->getEmail(), PDO::PARAM_STR);
            $service = new Password();
            
            $query->bindValue(':senha', $model->getSenha(), PDO::PARAM_STR);
            $query->bindValue(':nome', $model->getNome(), PDO::PARAM_STR);




            if ($query->execute()) {

                $model->setId($this->conn->getHandler()->lastInsertId());

                $this->conn->doCommit();

                return $model;
            } else {
                if ($this->conn->getConfig()->getDebug()) {
                    echo $query->errorInfo();
                }
                $this->conn->doRollback();
                return false;
            }
        } catch (Exception $ex) {
            if ($this->conn->getConfig()->getDebug()) {
                echo $ex->getMessage();
            }
            return false;
        }
    }

    /**
     * Atualiza o usuário
     * @param IModel $model
     * @return IModel
     * @throws Exception
     */
    public function atualizar($model)
    {
        $exist = $this->obterPorId($model->getId());

        if ($exist) {
            $sql = 'UPDATE login SET '
                    . 'email = :email,'
                    . 'nome = :nome WHERE id = :id';

            try {
                $query = $this->conn->getHandler()->prepare($sql);
                $query->bindValue(':email', $model->getEmail(), PDO::PARAM_STR);
                $query->bindValue(':nome', $model->getNome(), PDO::PARAM_STR);
                $query->bindValue(':id', $model->getId(), PDO::PARAM_INT);

                if ($query->execute()) {
                    $this->conn->doCommit();
                    return $model;
                } else {
                    if ($this->conn->getConfig()->getDebug()) {
                        echo $this->conn->getHandler()->errorInfo();
                    }
                    $this->conn->doRollback();
                    return false;
                }
            } catch (Exception $ex) {
                if ($this->conn->getConfig()->getDebug()) {
                    echo $ex->getMessage();
                }
                return false;
            }
        } else {
            throw new \Exception('Usuário não encontrado');
        }
    }

    /**
     * Retorna todos os usuários
     * @return ArrayObject
     */
    public function listar()
    {
        $prepare = $this->conn->getHandler()->prepare('SELECT id, nome, login, email, dtUltimoAcesso FROM login');
        $prepare->execute();

        $results = $prepare->fetchAll(PDO::FETCH_CLASS, User::class);
        return new ArrayObject($results);
    }

    /**
     * Carrega usuário por ID
     * @param int $id
     * @return User
     */
    public function obterPorId($id)
    {
        $prepare = $this->conn->getHandler()->prepare('SELECT id, login, nome, email, dtUltimoAcesso FROM login WHERE id=:id');
        $prepare->bindValue(':id', $id, PDO::PARAM_INT);

        $prepare->execute();


        $result = $prepare->fetchObject(User::class);


        return $result;
    }

    /**
     * Carrega usuário por Login
     * @param string $login
     * @return User
     */
    public function obterPorLogin($login)
    {
        $prepare = $this->conn->getHandler()->prepare('SELECT id, login, nome, email, dtUltimoAcesso FROM login WHERE login=:login');
        $prepare->bindValue(':login', $login, PDO::PARAM_STR);

        $prepare->execute();


        $result = $prepare->fetchObject(User::class);


        return $result;
    }


    /**
     * Atualiza a data do último acesso do usuário
     * @param User $user
     */
    private function setDataAcesso($user)
    {
        $sql = 'UPDATE login SET dtUltimoAcesso = date() WHERE id=:id';

        try {
            $query = $this->conn->getHandler()->prepare($sql);
            $query->bindValue(':id', $user->getId(), PDO::PARAM_INT);

            $query->execute();

            $this->conn->doCommit();
        } catch (Exception $ex) {
            if ($this->conn->getConfig()->getDebug()) {
                echo $ex->getMessage();
            }
        }
    }

    /**
     * Altera senha
     * @param User $user
     * @return boolean
     */
    public function mudaSenha($user)
    {
        $sql = 'UPDATE login SET senha = :senha WHERE id=:id';

        try {
            $query = $this->conn->getHandler()->prepare($sql);
            $query->bindValue(':senha', $user->getSenha(), PDO::PARAM_STR);
            $query->bindValue(':id', $user->getId(), PDO::PARAM_INT);

            $query->execute();
            $this->conn->doCommit();
            return true;
        } catch (Exception $ex) {
            if ($this->conn->getConfig()->getDebug()) {
                echo $ex->getMessage();
            }
            return false;
        }
    }

    /**
     * Verifica se usuário existe
     * @return boolean
     */
    public function existe($login)
    {
        $prepare = $this->conn->getHandler()->prepare('SELECT login FROM login WHERE login = :login');

        $prepare->bindValue(':login', $login, \PDO::PARAM_STR);

        $prepare->execute();



        $results = new ArrayObject($prepare->fetchAll(PDO::FETCH_CLASS, User::class));

        return (boolean) $results->count();
    }

    /**
     * Carrega senha
     * @param User $user
     * @return Carrega senha
     */
    public function loadSenha($user)
    {
        $prepare = $this->conn->getHandler()->prepare('SELECT senha FROM login WHERE login = :login');

        $prepare->bindValue(':login', $user->getLogin(), \PDO::PARAM_STR);

        $prepare->execute();



        $result = $prepare->fetchObject(User::class);


        return ($result ? $result->getSenha() : null);
    }

}
