<?php

/*
 *  @link http://eximiaweb.com.br/
 *  @copyright Copyright (c) 2016-2016 ExímiaWeb Informática
 *  @license 
 *  Desenvolvido por: EximiaWeb Informática
 * 
 */

namespace User\Service;


/**
 * Rotina de senhas
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Password
{

    /**
     * Encripta a senha
     * @param string $senha
     * @return string
     */
    public function encrypt($senha)
    {
        $passwd = password_hash($senha, PASSWORD_DEFAULT, array('cost'=>14));

        return $passwd;
    }


    /**
     * Cria nova senha
     * @param int $size
     * @return string
     */
    public static function generate(int $size)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*() /*-+,.;/:?';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $size; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function validatePassword($senha, $original)
    {
        return password_verify($senha, $original);
    }

}
