<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Service\Validator;

/**
 * Validador para o Usuário
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class User extends \Core\Service\Validator\AbstractValidator
{

    /**
     *
     * @var Programa
     */
    private static $instance;

    /**
     * Array com erros encontrados
     * @var array 
     */
    protected $errors = array();
    private $confirmSenha;

    private function __construct()
    {
        
    }

    /**
     * Retorna instância do validador
     * @return Programa
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Verifica se o usuário é válido
     * @param \User\Model\User $user
     * @return boolean
     */
    public function isValid($user)
    {
        $result = $this->validaInteiro($user->getId(), 'dtid');

        $result &= $this->validaVazio($user->getNome(), 'nome') && $this->validaMinMax($user->getNome(), 4, 50, 'nome');

        $result &= $this->validaVazio($user->getEmail(), 'email') && $this->validaMinMax($user->getEmail(), 4, 150, 'email') && $this->validaEmail($user->getEmail(), 'email');

        $result &= $this->validaVazio($user->getLogin(), 'login') && $this->validaMinMax($user->getLogin(), 3, 16, 'login');

        if (!$user->getId() || $user->getSenha()) {
            $result &= $this->validaVazio($user->getSenha(), 'senha') && $this->validaMinMax($user->getSenha(), 3, 32, 'senha');

            $result &= $this->validaIguais($user->getSenha(), $this->confirmSenha, 'senha', 'confirmar-senha');
        }

        return $result;
    }

    public function setConfirmSenha($confirmSenha)
    {
        $this->confirmSenha = $confirmSenha;
    }

    /**
     * Valida endereço de e-mail
     * @param string $valor
     * @param string $nomeCampo
     * @return boolean
     */
    private function validaEmail($valor, $nomeCampo)
    {
        if ($valor) {
            $pattern = '/[a-z][a-z0-9\-_.]*@[a-z0-9\-_]+.[a-z0-9]{2,3}.?[a-z]{0,2}/';
            if (!preg_match($pattern, $valor)) {
                $this->errors[$nomeCampo] = 'O valor parece não ser um e-mail válido';
                return false;
            }
        }

        return true;
    }

    /**
     * Valida campos de confirmação
     * @param \User\Model\User $user
     * @return boolean
     */
    private function validaIguais($valor1, $valor2, $campo1, $campo2)
    {
        if ($valor1 != $valor2) {
            $this->errors[$campo1] = 'O valor de ' . $campo1 . ' não confere com o valor de ' . $campo2;
            $this->errors[$campo2] = 'O valor de ' . $campo2 . ' não confere com o valor de ' . $campo1;
            return false;
        }
        return true;
    }

}
