<?php

/*
 *  @link http://eximiaweb.com.br/
 *  @copyright Copyright (c) 2016-2016 ExímiaWeb Informática
 *  @license 
 *  Desenvolvido por: EximiaWeb Informática
 * 
 */

namespace User\Service;

/**
 * Encripta as informações para realizar o login
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Crypto
{

    private $privateKey;
    private $publicKey;
    private $validade;
    private static $valid = null;

    public function __construct($validade = 60)
    {
        $dn = array(
            'private_key_bits' => 1024,
            'private_key_type' => OPENSSL_KEYTYPE_RSA
        );
        $this->validade = $validade;
        $res = openssl_pkey_new($dn);

        openssl_pkey_export($res, $this->privateKey);

        $k = openssl_pkey_get_details($res);
        $this->publicKey = $k['key'];
        
        
        $this->session_start();
    }

    public function startTransaction()
    {
        $session = new \Core\Helper\SessionHelper();
        $session->setVar('KL-K', base64_encode($this->privateKey));
        $session->setVar('KL-V', $this->validade);
        $session->setVar('KL-T', time());

        return base64_encode($this->publicKey);
    }

    public static function crypt($value, $key)
    {
        openssl_public_encrypt($value, $enncripted, base64_decode($key), OPENSSL_RAW_DATA);
        return $enncripted;
    }

    public static function decrypt($value, $key)
    {
        if (is_null(self::$valid)) {
            throw new \Exception('Criptografia não validada ainda, utilize o método valid antes');
        } elseif (!self::$valid) {
            throw new \Exception('Validade do token expirada, tente novamente');
        } else {
            
            openssl_private_decrypt($value, $decripted, base64_decode($key), OPENSSL_RAW_DATA);
            return $decripted;
        }
    }

    public static function validateSession()
    {
        $session = new \Core\Helper\SessionHelper();

        if ($session->hasSession()) {
            $validade = $session->getVar('KL-V');
            $startTime = \DateTime::createFromFormat('U',$session->getVar('KL-T'));
            $endTime = \DateTime::createFromFormat('U', time());

            $dif = $startTime->diff($endTime, true);
            
            
            $ellapsedTime = $dif->format('%s') + ($dif->format('%i')*60)  + ($dif->format('%h')*3600);

            self::$valid = $ellapsedTime < $validade;

        } else {
            self::$valid = false;
        }

        return self::$valid;
    }

    public function getPublicKey()
    {
        return $this->publicKey;
    }

    public function getValidade()
    {
        return $this->validade;
    }

    private static function session_start()
    {
        switch (session_status()) {
            case PHP_SESSION_DISABLED :
                throw new Exception('Este plugin necessita sessões');
                break;
            case PHP_SESSION_NONE :
                session_start();
                break;
        }
    }

}
