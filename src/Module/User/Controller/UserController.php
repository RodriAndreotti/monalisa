<?php

/*
 *  @link http://eximiaweb.com.br/
 *  @copyright Copyright (c) 2016-2016 ExímiaWeb Informática
 *  @license 
 *  Desenvolvido por: EximiaWeb Informática
 */

namespace User\Controller;

/**
 * Controller para o usuário
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class UserController extends \Core\Controller\AbstractController
{

    public function index()
    {
        $crypt = new \User\Service\Crypto();
        $pk = $crypt->startTransaction();
        $origem = md5(uniqid());

        $ses = new \Core\Helper\SessionHelper();
        $ses->setVar('form-origem', $origem);


        $view = new \Core\Helper\Model\ViewModel();

        $layout = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, __DIR__ . '../../view/template/login.php');
        $view->setLayout($layout);
        $view->addVar('origem', $origem);
        $view->addVar('pk', $pk);
        return $view;
    }

    public function login()
    {
        $login = base64_decode(filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING));
        $senha = base64_decode(filter_input(INPUT_POST, 'passwd', FILTER_SANITIZE_STRING));
        $origem = base64_decode(filter_input(INPUT_POST, 'origem', FILTER_SANITIZE_STRING));

        $session = new \Core\Helper\SessionHelper();

        $sm = $this->getServiceManager();

        if (\User\Service\Crypto::validateSession()) {
            if (\User\Service\Crypto::decrypt($origem, $session->getVar('KL-K')) == $session->getVar('form-origem')) {
                $ur = $sm->get(\User\Repository\UserRepository::class);

                $usuario = new \User\Model\User();
                $usuario->setLogin(\User\Service\Crypto::decrypt($login, $session->getVar('KL-K')))
                        ->setSenha(\User\Service\Crypto::decrypt($senha, $session->getVar('KL-K')));

                $usuario = $ur->doLogin($usuario);
                



                $success = $usuario->isLogado();

                if ($success) {
                    $session->setVar('user', $usuario);
                    $msg = 'Login realizado com sucesso';
                } else {
                    $msg = 'Usuário ou senha inválidos';
                }
            } else {
                $success = false;
                $msg = 'O formulário não se originou do site';
            }
        } else {
            $success = false;
            $msg = 'O tempo limite da requisição expirou';
        }

        $response = array(
            'success' => $success,
            'msg' => $msg
        );

        $response = new \Core\Helper\Model\JsonModel($response);

        return $response;
    }

    public function logoff()
    {
        $session = new \Core\Helper\SessionHelper();
        $user = $session->getObject('user');

        if ($user) {
            unset($_SESSION['user']);
            \Core\Util\Message::setMessage('Logoff realizado com sucesso', 'success');
        }
        return $this->redirect()->toUrl('/');
    }

    public function consultaLogin()
    {
        $ur = new \User\Repository\UserRepository($this->getConfig(), new \User\Model\User);


        $response = array(
            'existe' => $ur->existe(filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING))
        );

        return new \User\Helper\JsonHelper($response);
    }

    public function mudarSenha()
    {
        $vh = new \Core\Helper\Model\ViewModel();


        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $sm = $this->getServiceManager();

            $session = new \Core\Helper\SessionHelper();
            $user = $session->getObject('user');

            $repository = $sm->get(\User\Repository\UserRepository::class);



            $user->setSenha(filter_input(INPUT_POST, 'senhaAtual', FILTER_SANITIZE_STRING));

            $passwordService = new \User\Service\Password();



            if ($passwordService->validatePassword($user->getSenha(), $repository->loadSenha($user))) {
                if (filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING) == filter_input(INPUT_POST, 'confirmar-senha', FILTER_SANITIZE_STRING)) {
                    $user->setSenha($passwordService->encrypt(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING)));
                    if ($repository->mudaSenha($user)) {
                        \Core\Util\Message::setMessage('Senha alterada com sucesso', 'success', 'page');
                    } else {
                        \User\Util\Message::setMessage('Houve um erro ao alterar a senha', 'danger', 'page');
                    }
                } else {
                    \Core\Util\Message::setMessage('A senhas informadas são diferentes', 'danger', 'senha');
                    \Core\Util\Message::setMessage('A senhas informadas são diferentes', 'danger', 'confirmar-senha');
                }
            } else {
                \Core\Util\Message::setMessage('A senha informada não corresponde a senha do usuário', 'danger', 'senhaAtual');
            }
        }


        return $vh;
    }

    public function listar()
    {
        $vh = new \Core\Helper\Model\ViewModel();

        $sm = $this->getServiceManager();

        $ur = $sm->get(\User\Repository\UserRepository::class);

        $colaboradores = $ur->listar();

        $vh->addVar('colaboradores', $colaboradores);

        return $vh;
    }

    public function editar()
    {
        $request = $this->getRequest();
        $sm = $this->getServiceManager();

        $session = new \Core\Helper\SessionHelper();
        $loggedUser = $session->getObject('user');

        $id = $request->getFromRoute('id');



        if ($loggedUser->getLogin() == 'admin' || (isset($id) && $id = $loggedUser->getId())) {

            $vh = new \Core\Helper\Model\ViewModel();






            $colaborador = $sm->get(\User\Repository\UserRepository::class)->obterPorId($id);

            if ($request->getPost()) {
                $post = $request->getPost();
                $colaborador = \User\Model\User::withData((object) $post);
                $validator = \User\Service\Validator\User::getInstance();
                $validator->setConfirmSenha($post['confirmar-senha']);

                $msg = new \Core\Util\Message();

                if ($validator->isValid($colaborador)) {

                    $service = new \User\Service\Password();

                    if ((int) $colaborador->getId() == 0) {
                        $colaborador->setSenha($service->encrypt($colaborador->getSenha()));
                        $colaborador = $sm->get(\User\Repository\UserRepository::class)->inserir($colaborador);
                    } else {
                        $colaborador = $sm->get(\User\Repository\UserRepository::class)->atualizar($colaborador);
                        if ($colaborador->getSenha()) {
                            $colaborador->setSenha($service->encrypt($colaborador->getSenha()));
                            $sm->get(\User\Repository\UserRepository::class)->mudaSenha($colaborador);
                        }
                    }

                    if ($colaborador) {
                        $msg->setMessage('Colaborador salvo com sucesso', 'success');
                    } else {
                        $msg->setMessage('Houve um erro ao salvar o colaborador', 'danger');
                    }
                } else {
                    $vh->addVar('errors', $validator->getErrors());
                }
            }


            $vh->addVar('colaborador', $colaborador);

            return $vh;
        } else {
            throw new \Core\Exception\NotAllowedException('Infelizmente você não pode acessar esta área do sistema.');
        }
    }

    public function dashboard()
    {
        return new \Core\Helper\Model\ViewModel();
    }

}
