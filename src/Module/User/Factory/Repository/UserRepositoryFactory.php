<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Factory\Repository;

/**
 * Factory para o repositório de usuário
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class UserRepositoryFactory implements \Core\Factory\FactoryInterface
{
    public function createService(\Core\Application $app, $requestedClass)
    {
        $userRepository = \User\Repository\UserRepository::getInstance(\Core\DB\Connection::getInstance($app));
        
        return $userRepository;
    }
}
