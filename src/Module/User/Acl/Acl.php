<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Acl;

/**
 * Description of Acl
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c)2016, Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Acl
{

    private $roles;
    private $resources;
    private $rules;
    private $assertion;

    public function __construct()
    {
        $this->roles = array();
        $this->resources = array();
        $this->rules = array();
    }

    public function isAllowed($resource, $role)
    {
        if (key_exists($role, $this->roles) && key_exists($resource, $this->resources)) {
            if (isset($this->rules[$role][$resource])) {
                return $this->rules[$role][$resource];
            }
            else {
                return false;
            }
        }

        throw new \Exception('Resource não encontrado na cadeia de permissões', 500);
    }

    public function addRole(Role $role)
    {
        $this->roles[$role->getName()] = $role;
    }

    public function addResource(Resource $resource)
    {
        $this->resources[$resource->getName()] = $resource;
    }

    public function allow($resource, $role)
    {
        if (key_exists($role, $this->roles) && key_exists($resource, $this->resources)) {
            $this->rules[$role][$resource] = true;
        }
    }

}
