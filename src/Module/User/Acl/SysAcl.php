<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Acl;

/**
 * Description of SysAcl
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c)2016, Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class SysAcl extends Acl
{

    public function __construct()
    {
        parent::__construct();
        $this->addRole(new Role('guest'));
        $this->addRole(new Role('admin'));
        
        $this->addResource(new Resource('home-index'));
        
        $this->addResource(new Resource('erro-index'));
        
        $this->addResource(new Resource('user-index'));
        $this->addResource(new Resource('user-login'));
        $this->addResource(new Resource('user-logoff'));
        $this->addResource(new Resource('user-dashboard'));
        $this->addResource(new Resource('user-listar'));
        $this->addResource(new Resource('user-editar'));
        $this->addResource(new Resource('user-mudarsenha'));
        
        $this->addResource(new Resource('importacao-editar'));
        $this->addResource(new Resource('importacao-index'));
        $this->addResource(new Resource('importacao-export'));
        $this->addResource(new Resource('importacao-view'));
        $this->addResource(new Resource('importacao-excel'));
        $this->addResource(new Resource('importacao-baixar'));
        $this->addResource(new Resource('importacao-reimportar'));
        
        $this->addResource(new Resource('view-emissao'));
        $this->addResource(new Resource('view-recibo'));
        
        
        $this->addResource(new Resource('exportacao-index'));
        
        $this->addResource(new Resource('associacao-condominio'));
        $this->addResource(new Resource('associacao-editcondo'));
        $this->addResource(new Resource('associacao-delcondo'));
        $this->addResource(new Resource('associacao-emissao'));
        $this->addResource(new Resource('associacao-carteira'));
        $this->addResource(new Resource('associacao-unidade'));
        $this->addResource(new Resource('associacao-editunidade'));
        $this->addResource(new Resource('associacao-delunidade'));
        $this->addResource(new Resource('associacao-conta'));
        $this->addResource(new Resource('associacao-editconta'));
        $this->addResource(new Resource('associacao-delconta'));
        
        $this->allow('erro-index', 'guest');
        $this->allow('erro-index', 'admin');
        
        $this->allow('home-index', 'guest');
        
        $this->allow('user-index', 'guest');
        $this->allow('user-login', 'guest');
        $this->allow('user-logoff', 'admin');
        $this->allow('user-dashboard', 'admin');
        $this->allow('user-listar', 'admin');
        $this->allow('user-editar', 'admin');
        $this->allow('user-mudarsenha', 'admin');
        
        
        $this->allow('importacao-editar', 'admin');
        $this->allow('importacao-index', 'admin');
        $this->allow('importacao-export', 'admin');
        $this->allow('importacao-view', 'admin');
        $this->allow('importacao-excel', 'admin');
        $this->allow('importacao-baixar', 'admin');
        $this->allow('importacao-reimportar', 'admin');
        
        $this->allow('view-emissao', 'admin');
        $this->allow('view-recibo', 'admin');
        
        $this->allow('exportacao-index', 'admin');
        
        $this->allow('associacao-condominio', 'admin');
        $this->allow('associacao-editcondo', 'admin');
        $this->allow('associacao-delcondo', 'admin');
        
        $this->allow('associacao-emissao', 'admin');
        $this->allow('associacao-carteira', 'admin');
        
        $this->allow('associacao-conta', 'admin');
        $this->allow('associacao-editconta', 'admin');
        $this->allow('associacao-delconta', 'admin');
        
        $this->allow('associacao-unidade', 'admin');
        $this->allow('associacao-editunidade', 'admin');
        $this->allow('associacao-delunidade', 'admin');
    }


}
