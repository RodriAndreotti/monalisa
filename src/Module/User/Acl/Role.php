<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Acl;

/**
 * Description of Role
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c)2016, Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Role
{
    private $name;
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    public function getName()
    {
        return $this->name;
    }
}
