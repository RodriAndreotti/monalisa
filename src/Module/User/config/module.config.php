<?php

return array(
    'routes'    =>  array(
        'user' => array(
            'route' => '/user[/:action][/:id]',
            'controller' => User\Controller\UserController::class,
            'view-dir' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view',
            'padroes' => array(
                'action' => '[a-z][a-zA-Z0-9]+',
                'id' => '[0-9]+'
            )
        ),
    ),
    'services' => array(
        User\Repository\UserRepository::class => \User\Factory\Repository\UserRepositoryFactory::class
    )
);