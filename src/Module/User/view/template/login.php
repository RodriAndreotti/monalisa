<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <?php
        $this
                ->addStyle($this->basePath('assets/bower_components/bootstrap/dist/css/bootstrap.min.css'))
                ->addStyle($this->basePath('assets/bower_components/font-awesome/css/font-awesome.min.css'))
                ->addStyle($this->basePath('assets/bower_components/Ionicons/css/ionicons.min.css'))
                ->addStyle($this->basePath('assets/css/AdminLTE.min.css'))
                ->addStyle($this->basePath('assets/plugins/iCheck/square/blue.css'))
                ->addStyle($this->basePath('assets/css/custom.css'));
        ?>
        
        <?= $this->getStyles(); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index2.html">Monalisa</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <?php $this->getContent();?>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        <?php
        $this
                ->addInlineScript($this->basePath('assets/bower_components/jquery/dist/jquery.min.js'))
                ->addInlineScript($this->basePath('assets/bower_components/bootstrap/dist/js/bootstrap.min.js'))
                ->addInlineScript($this->basePath('assets/plugins/iCheck/icheck.min.js'));
        ob_start();
        ?>
        <script type="text/javascript">
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
        <?php
        $script = ob_get_clean();
        $this
                ->addInlineScript($script, true);
        ?>
        
        <?= $this->getInlineScripts(); ?>
    </body>
</html>
