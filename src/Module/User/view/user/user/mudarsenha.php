<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$errors = $this->get('errors');
?>
<form name="frmGrade" id="frmGrade" action="<?= $this->url('user', array('action' => 'mudarsenha')); ?>" method="post">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Criar / Editar Colaborador</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= Core\Util\Message::getMessages('page'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-xs-12">
                            <div class="form-group">
                                <label for="senha-atual">Senha Atual</label><br>
                                <input type="password" name="senhaAtual" class="form-control" id="senha-atual" placeholder="Senha Atual">

                                <p class="help-block text-red"><?= Core\Util\Message::getMessages('senhaAtual'); ?></p>

                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="senha">Senha</label>
                                <input type="password" name="senha" class="form-control" id="senha" placeholder="Senha">

                                <p class="help-block text-red"><?= Core\Util\Message::getMessages('senha'); ?></p>

                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="confirmar-senha">Confirmar Senha</label>
                                <input type="password" name="confirmar-senha" class="form-control" id="confirmar-senha" placeholder="Confirmar Senha">

                                <p class="help-block text-red"><?= Core\Util\Message::getMessages('confirmar-senha'); ?></p>

                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" id="btn-salvar" class="btn btn-primary">Salvar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<?php
$this
        ->addInlineScript($this->basePath('assets/js/pages/user.js'), false, 1001);
?>
