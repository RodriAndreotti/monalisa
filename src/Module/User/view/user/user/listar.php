<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$colaboradores = $this->get('colaboradores');

?>

<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-10">
                <h3 class="box-title">Criar / Editar Colaborador</h3>
                    </div>
                    <div class="col-md-2 text-right">
                        <a href="<?=$this->url('user', array('action'=>'editar'));?>" class="btn btn-sm btn-primary">
                            <i class="fa fa-fw fa-plus"></i> Novo
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-condensed table-hover table-striped">
                        <thead>
                            <tr>
                                <th>

                                </th>
                                <th>
                                    Nome
                                </th>
                                <th>
                                    Login
                                </th>
                                <th>
                                    E-mail
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($colaboradores): ?>
                                <?php foreach ($colaboradores as $colaborador): ?>
                                    <tr>
                                        <td>
                                            <a href="<?=$this->url('user', array('action'=>'editar', 'id'=>$colaborador->getId()));?>" class="btn btn-primary">
                                                <i class="fa fa-fw fa-edit"></i> Editar
                                            </a>
                                            <a href="<?=$this->url('user', array('action'=>'apagar', 'id'=>$colaborador->getId()));?>" class="btn btn-danger">
                                                <i class="fa fa-fw fa-trash"></i> Apagar
                                            </a>
                                        </td>
                                        <td>
                                            <?= $colaborador->getNome(); ?>
                                        </td>
                                        <td>
                                            <?= $colaborador->getLogin(); ?>
                                        </td>
                                        <td>
                                            <?= $colaborador->getEmail(); ?>
                                        </td>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="4">
                                        <span class="alert alert-info">Não há colaboradores cadastrados no sistema</span>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>