<?php
$pk = $this->get('pk');
$origem = $this->get('origem');

ob_start();
?>
<script type="text/javascript">
    var pk = '<?= $pk; ?>';
    var origem = '<?= $origem; ?>';
    var path = '<?= $this->url('user', array('action' => 'login')); ?>';

</script>
<?php
$script = ob_get_clean();
$this
        ->addInlineScript($script, true, 999);

$this
        ->addInlineScript($this->basePath('assets/js/lib/base.js'), false, 1002)
        ->addInlineScript($this->basePath('assets/js/lib/jsencrypt.js'), false, 1003)
        ->addInlineScript($this->basePath('assets/js/pages/login.js'), false, 1004)
?>




<p class="login-box-msg">Digite seu login e senha</p>

<form action="" method="post">
    <div class="form-group has-feedback">
        <input type="text" class="form-control" name="login" id="txtLogin" placeholder="Login">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" name="senha" id="txtPasswd" placeholder="Senha">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-md-12" id="message">
            
        </div>
    </div>
    <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
            <button type="button" id="btLogar" class="btn btn-primary btn-block btn-flat">Entrar</button>
        </div>
        <!-- /.col -->
    </div>
</form>



<a href="#">Esqueci minha senha</a><br>

