<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$colaborador = $this->get('colaborador');
$isEdit = $colaborador ? ((boolean) $colaborador->getId() ? true : false) : false;
$errors = $this->get('errors');
?>
<form name="frmGrade" id="frmGrade" action="<?= $this->url('user', array('action' => 'editar', 'id' =>  $colaborador ? $colaborador->getId() : null)); ?>" method="post">
    <input type="hidden" name="id" value="<?= $colaborador ? $colaborador->getId() : ''; ?>">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Criar / Editar Colaborador</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome" value="<?= $colaborador ? $colaborador->getNome() : ''; ?>">
                                <?php if (isset($errors['nome'])): ?>
                                    <p class="help-block text-red"><?= $errors['nome']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="text" name="email" class="form-control" id="email" placeholder="E-mail" value="<?= $colaborador ? $colaborador->getEmail() : ''; ?>">
                                <?php if (isset($errors['email'])): ?>
                                    <p class="help-block text-red"><?= $errors['email']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="login">Login</label>
                                <input type="text" name="login" class="form-control" id="login" placeholder="Login" value="<?= $colaborador ? $colaborador->getLogin() : ''; ?>" <?= $isEdit ? 'readonly' : ''; ?>>
                                <?php if (isset($errors['login'])): ?>
                                    <p class="help-block text-red"><?= $errors['login']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="senha">Senha</label>
                                <input type="password" name="senha" class="form-control" id="senha" placeholder="Senha" <?= $isEdit ? 'readonly' : ''; ?>>
                                <?php if (isset($errors['senha'])): ?>
                                    <p class="help-block text-red"><?= $errors['senha']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="confirmar-senha">Confirmar Senha</label>
                                <input type="password" name="confirmar-senha" class="form-control" id="confirmar-senha" placeholder="Confirmar Senha" <?= $isEdit ? 'readonly' : ''; ?>>
                                <?php if (isset($errors['confirmar-senha'])): ?>
                                    <p class="help-block text-red"><?= $errors['confirmar-senha']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <div class="form-group">
                                <?php if ($isEdit): ?>
                                    <label for="btn-nova-senha">&nbsp;</label><br>
                                    <button type="button" id="btn-nova-senha" class="btn btn-info">Nova senha</button>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" id="btn-salvar" class="btn btn-primary">Salvar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<?php
$this
        ->addInlineScript($this->basePath('assets/js/pages/user.js'), false, 1001);
?>
