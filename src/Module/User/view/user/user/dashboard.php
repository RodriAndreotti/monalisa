<?php $session = new Core\Helper\SessionHelper(); ?>
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-7 connectedSortable">


        <!-- TO DO List -->
        <div class="box box-primary">
            <div class="box-header">
                <i class="ion ion-clipboard"></i>

                <h3 class="box-title">To Do List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                <ul class="todo-list">
                    <?php if ($session->getVar('qtdProgramas') > 0) : ?>
                        <li>
                            <!-- drag handle -->
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <!-- todo text -->
                            <span class="text">Programas sem Revisão</span>
                            <!-- Emphasis label -->
                            <small class="label label-danger"><i class="fa fa-clock-o"></i> <?= $session->getVar('qtdProgramas'); ?> programas</small>
                            <!-- General tools such as edit or delete-->
                            <div class="tools">
                                <a href="<?= $this->url('grade'); ?>"><i class="fa fa-edit"></i></a>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if ($session->getVar('qtdCanais') > 0) : ?>
                        <li>
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                            <span class="text">Canais sem Mapeamento</span>
                            <small class="label label-warning"><i class="fa fa-asterisk "></i> <?= $session->getVar('qtdCanais'); ?> canais</small>
                            <div class="tools">
                                <a href="<?= $this->url('canal'); ?>"><i class="fa fa-edit"></i></a>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if ($session->getVar('qtdGeneros') > 0) : ?>
                        <li>
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>

                            <span class="text">Gêneros sem Mapeamento</span>
                            <small class="label label-warning"><i class="fa fa-universal-access"></i> <?= $session->getVar('qtdGeneros'); ?> Gêneros</small>
                            <div class="tools">
                                <a href="<?= $this->url('categoria'); ?>"><i class="fa fa-edit"></i></a>
                            </div>
                        </li>
                    <?php endif; ?>

                </ul>
            </div>
        </div>
        <!-- /.box -->



    </section>
    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">



    </section>
    <!-- right col -->
</div>
<!-- /.row (main row) -->