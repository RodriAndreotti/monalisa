<?php

namespace User\Model;

use VipMar\Model\Interfaces\IUser;
use VipMar\Repository\UserRepository;
use VipMar\Util\Message;
use stdClass;

/**
 * Model para o usuário
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * 
 */
class User
{

    private $id;
    private $login;
    private $email;
    private $senha;
    private $nome;
    private $isLogado = false;

    const ATIVO = 1, INATIVO = 0;
    const ADMIN = 1;

    /**
     * Valida as informações vindas do formulário
     * @return boolean
     */
    public function isValid()
    {
        return true;
    }

    
    /**
     * Cria um objeto com dados
     * @param stdClass $data
     * @return User
     */
    public static function withData(stdClass $data)
    {
        $user = new User();
        $user->setId(property_exists($data, 'id') ? $data->id : null)
                ->setLogin(property_exists($data, 'login') ? $data->login : null)
                ->setEmail(property_exists($data, 'email') ? $data->email : null)
                ->setSenha(property_exists($data, 'senha') ? $data->senha : null)
                ->setNome(property_exists($data, 'nome') ?  $data->nome : null);

        return $user;
    }

    public function isLogado()
    {
        return $this->isLogado;
    }

    public function isAtivo()
    {
        return $this->ativo;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getSenha()
    {
        return $this->senha;
    }


    public function getIsLogado()
    {
        return $this->isLogado;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }


    public function setIsLogado($isLogado)
    {
        $this->isLogado = $isLogado;
        return $this;
    }
    
    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }
}