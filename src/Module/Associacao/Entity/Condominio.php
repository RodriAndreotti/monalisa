<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Entity;

/**
 * Modelo que descreve o condomínio para associação
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Condominio
{

    private $id;
    private $nome;
    private $codUnionWeb;
    private $codGoSoft;
    private $multa;
    private $juros;
    private $dias_limite;
    private $cedente;
    private $codigo_conta;
    private $codigo_carteira;

    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getCodUnionWeb()
    {
        return $this->codUnionWeb;
    }

    public function getCodGoSoft()
    {
        return $this->codGoSoft;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setCodUnionWeb($codUnionWeb)
    {
        $this->codUnionWeb = $codUnionWeb;
        return $this;
    }

    public function setCodGoSoft($codGoSoft)
    {
        $this->codGoSoft = $codGoSoft;
        return $this;
    }

    public function getMulta()
    {
        return $this->multa;
    }

    public function getJuros()
    {
        return $this->juros;
    }

    public function getDias_limite()
    {
        return $this->dias_limite;
    }

    public function getCedente()
    {
        return $this->cedente;
    }

    public function setMulta($multa)
    {
        $this->multa = $multa;
    }

    public function setJuros($juros)
    {
        $this->juros = $juros;
    }

    public function setDias_limite($dias_limite)
    {
        $this->dias_limite = $dias_limite;
    }

    public function setCedente($cedente)
    {
        $this->cedente = $cedente;
    }

    public function getCodigo_conta()
    {
        return $this->codigo_conta;
    }

    public function getCodigo_carteira()
    {
        return $this->codigo_carteira;
    }

    public function setCodigo_conta($codigo_conta)
    {
        $this->codigo_conta = $codigo_conta;
        return $this;
    }

    public function setCodigo_carteira($codigo_carteira)
    {
        $this->codigo_carteira = $codigo_carteira;
        return $this;
    }

}
