<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Entity;

/**
 * Entidade de conta
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Conta
{
    private $codUnionWeb;
    private $codGoSoft;
    private $descricao;
    
    public function getCodUnionWeb()
    {
        return $this->codUnionWeb;
    }

    public function getCodGoSoft()
    {
        return $this->codGoSoft;
    }

    public function setCodUnionWeb($codUnionWeb)
    {
        $this->codUnionWeb = $codUnionWeb;
        return $this;
    }

    public function setCodGoSoft($codGoSoft)
    {
        $this->codGoSoft = $codGoSoft;
        return $this;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }
}
