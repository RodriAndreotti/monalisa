<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Entity;

/**
 * Entidade de associação para unidades
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Unidade
{
    private $rowid;
    
    private $condominio;
    private $bloco;
    private $unidade;

    private $codBlocoGoSoft;    
    private $codUnidadeGoSoft;
    
    public function getCondominio()
    {
        return $this->condominio;
    }

    public function getBloco()
    {
        return $this->bloco;
    }

    public function getUnidade()
    {
        return $this->unidade;
    }

    public function getCodBlocoGoSoft()
    {
        return $this->codBlocoGoSoft;
    }

    public function getCodUnidadeGoSoft()
    {
        return $this->codUnidadeGoSoft;
    }

    public function setCondominio($condominio)
    {
        $this->condominio = $condominio;
        return $this;
    }

    public function setBloco($bloco)
    {
        $this->bloco = $bloco;
        return $this;
    }

    public function setUnidade($unidade)
    {
        $this->unidade = $unidade;
        return $this;
    }

    public function setCodBlocoGoSoft($codBlocoGoSoft)
    {
        $this->codBlocoGoSoft = $codBlocoGoSoft;
        return $this;
    }

    public function setCodUnidadeGoSoft($codUnidadeGoSoft)
    {
        $this->codUnidadeGoSoft = $codUnidadeGoSoft;
        return $this;
    }
    
    public function getId()
    {
        return $this->rowid;
    }

    public function setId($id)
    {
        $this->rowid = $id;
        return $this;
    }
}
