<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$unidade = $this->get('unidade');
?>
<form name="frm-unidade" method="post" action="<?= $this->url('associacao', array('action' => 'editunidade', 'id'   =>  $unidade->getId())); ?>">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="box-title">Unidade <?= ($unidade ? ' - ' . (!$unidade->getCondominio() instanceof Associacao\Entity\Condominio ?: $unidade->getCondominio()->getNome()) . ' - B.' . $unidade->getBloco() . ' - ' . $unidade->getUnidade() : ''); ?></h3>
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">
                                <i class="fa fa-fw fa-save"></i> Salvar
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <?= \Core\Util\Message::getMessages(); ?>
                    <div class="form-group text-center">
                        <input type="hidden" name="id" value="<?= $unidade ? $unidade->getId() : '';?>">
                        <?php if ($unidade): ?>
                            <?= $unidade->getCondominio()->getNome(); ?>
                        <input type="hidden" name="condominio" value="<?=$unidade->getCondominio()->getCodUnionWeb(); ?>">
                        <?php else: ?>
                            <label for="cbCondominio">Condomínio: </label>
                            <select name="condominio" id="cbCondominio" class="form-control form-control-line">
                                <option value="">Todos</option>
                                <?php foreach ($condominios as $condominio): ?>
                                    <option 
                                        value="<?= $condominio->getCodUnionWeb(); ?>">
                                            <?= $condominio->getNome(); ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                    </div>
                    <h4 class="text-center">Códigos</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="txtBloco">Bloco</label>
                                
                                <input type="text" 
                                       name="bloco" 
                                       id="txtBloco" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'bloco') ?
                                               ($unidade ? $unidade->getBloco() : '') :
                                               filter_input(INPUT_POST, 'bloco', FILTER_SANITIZE_STRING);
                                       ?>"<?=$unidade ? ' readonly="readonly"' : '';?>>
                            </div>
                            <div class="form-group">
                                <label for="txtUnidade">Unidade</label>
                                <input type="text" 
                                       name="unidade" 
                                       id="txtUnidade" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'unidade') ?
                                               ($unidade ? $unidade->getUnidade() : '') :
                                               filter_input(INPUT_POST, 'unidade', FILTER_SANITIZE_STRING);
                                       ?>"<?=$unidade ? ' readonly="readonly"' : '';?>>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="txtCodBlocoGoSoft">Código Bloco GoSoft</label>
                                <input type="text" 
                                       name="codBlocoGoSoft" 
                                       id="txtCodBlocoGoSoft" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codBlocoGoSoft') ?
                                               ($unidade ? $unidade->getCodBlocoGoSoft() : '') :
                                               filter_input(INPUT_POST, 'codBlocoGoSoft', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtcodUnidadeGoSoft">Código Unidade GoSoft</label>
                                <input type="text" 
                                       name="codUnidadeGoSoft" 
                                       id="txtcodUnidadeGoSoft" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codUnidadeGoSoft') ?
                                               ($unidade ? $unidade->getCodUnidadeGoSoft() : '') :
                                               filter_input(INPUT_POST, 'codUnidadeGoSoft', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>