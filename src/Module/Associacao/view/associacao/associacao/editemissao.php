<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$conta = $this->get('conta');
?>
<form name="frm-conta" method="post" action="<?= $this->url('associacao', array('action' => 'editconta')); ?>">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="box-title">Condomínios <?= ($conta ? ' - ' . $conta->getCodUnionWeb() : ''); ?></h3>
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">
                                <i class="fa fa-fw fa-save"></i> Salvar
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <?= \Core\Util\Message::getMessages(); ?>
                    <h4 class="text-center">Códigos</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="txtCodUnionWeb">Código UnionWeb</label>
                                <input type="text" 
                                       name="codUnionWeb" 
                                       id="txtCodUnionWeb" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codUnionWeb') ?
                                               ($conta ? $conta->getCodUnionWeb() : '') :
                                               filter_input(INPUT_POST, 'codUnionWeb', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="txtCodGoSoft">Código GoSoft</label>
                                <input type="text" 
                                       name="codGoSoft" 
                                       id="txtCodGoSoft" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codGoSoft') ?
                                               ($conta ? $conta->getCodGoSoft() : '') :
                                               filter_input(INPUT_POST, 'codGoSoft', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>