<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$contas = $this->get('contas');
?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3 class="box-title">Condomínios</h3>
                    </div>
                    <div class="col-md-2 text-right">
                        <a href="<?= $this->url('associacao', array('action' => 'editconta')); ?>" class="btn btn-sm btn-primary">
                            <i class="fa fa-fw fa-plus"></i> Novo
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <?= \Core\Util\Message::getMessages(); ?>
                <table class="table table-hover table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>
                                UnionWeb
                            </th>
                            <th>
                                GoSoft
                            </th>
                            <th>

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($contas): ?>
                            <?php foreach ($contas as $conta): ?>
                                <tr>
                                    <td>
                                        <?= $conta->getCodUnionWeb(); ?>
                                    </td>
                                    <td>
                                        <?= $conta->getCodGoSoft(); ?>
                                    </td>
                                    <td class="text-right">
                                        <a href="<?= $this->url('associacao', array('action' => 'editconta', 'id' => $conta->getCodUnionWeb())); ?>" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Editar associação</a>
                                        <a href="<?= $this->url('associacao', array('action' => 'delconta', 'id' => $conta->getCodUnionWeb())); ?>" class="btn btn-sm btn-danger confirm"><i class="fa fa-trash"></i> Apagar associação</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?> 
                        <?php endif; ?>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>