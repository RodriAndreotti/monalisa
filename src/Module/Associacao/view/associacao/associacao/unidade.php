<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$unidades = $this->get('unidades');
?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-10">
                        <h3 class="box-title">Unidades</h3>
                    </div>
                    <div class="col-md-2 text-right">
                        <a href="<?= $this->url('associacao', array('action' => 'editunidade')); ?>" class="btn btn-sm btn-primary">
                            <i class="fa fa-fw fa-plus"></i> Novo
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <?= \Core\Util\Message::getMessages(); ?>
                <table class="table table-hover table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>
                                Condomínio
                            </th>
                            <th>
                                Bloco
                            </th>
                            <th>
                                Unidade
                            </th>
                            <th>
                                Bloco GoSoft
                            </th>
                            <th>
                                Unidade GoSoft
                            </th>
                            <th>

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($unidades): ?>
                            <?php foreach ($unidades as $unidade): ?>
                                <tr>
                                    <td>
                                        <?= $unidade->getCondominio() instanceof Associacao\Entity\Condominio ? : $unidade->getCondominio() ; ?>
                                    </td>
                                    <td>
                                        <?= $unidade->getBloco(); ?>
                                    </td>
                                    <td>
                                        <?= $unidade->getUnidade(); ?>
                                    </td>
                                    <td>
                                        <?= $unidade->getCodBlocoGoSoft(); ?>
                                    </td>
                                    <td>
                                        <?= $unidade->getCodUnidadeGoSoft(); ?>
                                    </td>
                                    <td class="text-right">
                                        <a href="<?= $this->url('associacao', array('action' => 'editunidade', 'id' => $unidade->getId())); ?>" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Editar associação</a>
                                        <a href="<?= $this->url('associacao', array('action' => 'delunidade', 'id' => $unidade->getId())); ?>" class="btn btn-sm btn-danger confirm"><i class="fa fa-trash"></i> Apagar associação</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?> 
                        <?php endif; ?>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>