<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$condominio = $this->get('condominio');
?>
<form name="frm-condominio" method="post" action="<?= $this->url('associacao', array('action' => 'editcondo')); ?>">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="box-title">Condomínios <?= ($condominio ? ' - ' . $condominio->getNome() : ''); ?></h3>
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="submit" class="btn btn-sm btn-primary">
                                <i class="fa fa-fw fa-save"></i> Salvar
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <?= \Core\Util\Message::getMessages(); ?>
                    <div class="form-group">
                        <label for="txtNome">Nome do condomínio</label>
                        <input type="text" 
                               name="nome" 
                               id="txtNome" 
                               class="form-control" 
                               value="<?=
                               !filter_has_var(INPUT_POST, 'nome') ?
                                       ($condominio ? $condominio->getNome() : '') :
                                       filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
                               ?>">
                    </div>
                    <h4 class="text-center">Códigos</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="txtCodUnionWeb">Código UnionWeb</label>
                                <input type="text" 
                                       name="codUnionWeb" 
                                       id="txtCodUnionWeb" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codUnionWeb') ?
                                               ($condominio ? $condominio->getCodUnionWeb() : '') :
                                               filter_input(INPUT_POST, 'codUnionWeb', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtMulta">Multa</label>
                                <input type="text" 
                                       name="multa" 
                                       id="txtMulta" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'multa') ?
                                               ($condominio ? $condominio->getMulta() : '') :
                                               filter_input(INPUT_POST, 'multa', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtJuros">Juros</label>
                                <input type="text" 
                                       name="juros" 
                                       id="txtJuros" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'juros') ?
                                               ($condominio ? $condominio->getJuros() : '') :
                                               filter_input(INPUT_POST, 'juros', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtDiasLimite">Dias Limite</label>
                                <input type="text" 
                                       name="dias_limite" 
                                       id="txtDiasLimite" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'dias_limite') ?
                                               ($condominio ? $condominio->getDias_limite() : '') :
                                               filter_input(INPUT_POST, 'dias_limite', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtCedente">Cedente</label>
                                <input type="text" 
                                       name="cedente" 
                                       id="txtCedente" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'cedente') ?
                                               ($condominio ? $condominio->getCedente() : '') :
                                               filter_input(INPUT_POST, 'cedente', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="txtCodGoSoft">Código GoSoft</label>
                                <input type="text" 
                                       name="codGoSoft" 
                                       id="txtCodGoSoft" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codGoSoft') ?
                                               ($condominio ? $condominio->getCodGoSoft() : '') :
                                               filter_input(INPUT_POST, 'codGoSoft', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtCodGoSoft">Código da conta no GoSoft</label>
                                <input type="text" 
                                       name="codigo_conta" 
                                       id="txtCodConta" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codigo_conta') ?
                                               ($condominio ? $condominio->getCodigo_conta() : '') :
                                               filter_input(INPUT_POST, 'codigo_conta', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtCodCarteira">Código da carteira no GoSoft</label>
                                <input type="text" 
                                       name="codigo_carteira" 
                                       id="txtCodCarteira" 
                                       class="form-control" 
                                       value="<?=
                                       !filter_has_var(INPUT_POST, 'codigo_carteira') ?
                                               ($condominio ? $condominio->getCodigo_carteira() : '') :
                                               filter_input(INPUT_POST, 'codigo_carteira', FILTER_SANITIZE_STRING);
                                       ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>