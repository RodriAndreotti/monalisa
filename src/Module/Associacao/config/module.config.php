<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'routes'    =>  array(
        'associacao' =>  array(
            'route'     =>  '/associacao[/:action][/:id]',
            'controller'    => \Associacao\Controller\AssociacaoController::class,
            'view-dir'      =>  __DIR__  . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view',
            'padroes'  =>  array(
                'action'    =>  '[a-z][a-zA-Z0-9]+',
                'id'        =>  '[0-9]+'
            )
        )
    ),
    'services' => array(
        \Associacao\Repository\CondominioRepository::class => Associacao\Factory\Repository\CondominioRepositoryFactory::class,
        \Associacao\Repository\UnidadeRepository::class => Core\Factory\Repository\DefaultRepositoryFactory::class,
        \Associacao\Repository\ContaRespository::class => Associacao\Factory\Repository\ContaRepositoryFactory::class,
        \Associacao\Repository\GeneralRepository::class => Associacao\Factory\Repository\GeneralRepositoryFactory::class
    )
);