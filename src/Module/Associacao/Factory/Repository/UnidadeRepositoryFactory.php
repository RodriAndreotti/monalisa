<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Factory\Repository;

/**
 * Factory para o repositório das unidades
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class UnidadeRepositoryFactory implements \Core\Factory\FactoryInterface
{
    public function createService(\Core\Application $app, $requestedClass)
    {
        var_dump('Entrou aqui');
        $unidadeRepository = \Associacao\Repository\UnidadeRepository::getInstance(\Core\DB\Connection::getInstance($app));
        
        return $unidadeRepository;
    }
}
