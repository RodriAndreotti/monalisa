<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Factory\Repository;

/**
 * Factory para o repositório do condomínio
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class EmissaoRepositoryFactory implements \Core\Factory\FactoryInterface
{

    public function createService(\Core\Application $app, $requestedClass)
    {
        $emissaoRepository = \Associacao\Repository\EmissaoRespository::getInstance(\Core\DB\Connection::getInstance($app));

        return $emissaoRepository;
    }

}
