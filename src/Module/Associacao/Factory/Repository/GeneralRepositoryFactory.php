<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Factory\Repository;

/**
 * Factory para o repositório geral de associações
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class GeneralRepositoryFactory implements \Core\Factory\FactoryInterface
{
    
    /**
     * Cria o serviço
     * @param \Core\Application $app
     * @param type $requestedClass
     */
    public function createService(\Core\Application $app, $requestedClass)
    {
        $condominioRepository = \Associacao\Repository\CondominioRepository::getInstance(\Core\DB\Connection::getInstance($app));
        $contaRepository = \Associacao\Repository\ContaRespository::getInstance(\Core\DB\Connection::getInstance($app));
        $emissaoRepository = \Importacao\Repository\EmissaoRepository::getInstance(\Core\DB\Connection::getInstance($app));
        $reciboRepository = \Importacao\Repository\ReciboRepository::getInstance(\Core\DB\Connection::getInstance($app));
        
        return new \Associacao\Repository\GeneralRepository($condominioRepository, $contaRepository, $emissaoRepository, $reciboRepository);
    }

}
