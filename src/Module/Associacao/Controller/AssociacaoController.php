<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Controller;

/**
 * Controller que cuida das associações
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class AssociacaoController extends \Core\Controller\AbstractController
{
    /**
     * Lista os condomínios
     * @return \Core\Helper\Model\ViewModel
     */
    public function condominio()
    {
        $cr = $this->getServiceManager()->get(\Associacao\Repository\CondominioRepository::class);
        $condominios = $cr->listar();
        return new \Core\Helper\Model\ViewModel(array(
            'condominios'   =>  $condominios
        ));
    }
    
    /**
     * Edita um condomínio
     * @return \Core\Helper\Model\ViewModel
     */
    public function editcondo()
    {
        $request = $this->getRequest();
        $postData = $request->getPost();
        
        $view = new \Core\Helper\Model\ViewModel();
        
        $cr = $this->getServiceManager()->get(\Associacao\Repository\CondominioRepository::class);
        
        $cod = filter_var($request->getFromRoute('id'), FILTER_SANITIZE_STRING);
        if($cod) {
            $condominio = $cr->obterPorCodUnion($cod);

            if($condominio) {
                $view->addVar('condominio', $condominio);
            }
        }
        
        if($postData) {
            $condominioAssoc = new \Associacao\Entity\Condominio();
            $condominioAssoc->setNome(filter_var($postData['nome'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setCodUnionWeb(filter_var($postData['codUnionWeb'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setMulta(filter_var($postData['multa'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setJuros(filter_var($postData['juros'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setCedente(filter_var($postData['cedente'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setDias_limite(filter_var($postData['dias_limite'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setCodigo_conta(filter_var($postData['codigo_conta'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setCodigo_carteira(filter_var($postData['codigo_carteira'], FILTER_SANITIZE_STRING));
            $condominioAssoc->setCodGoSoft(filter_var($postData['codGoSoft'], FILTER_SANITIZE_STRING));
            
            $result = $cr->salvar($condominioAssoc);
            if($result instanceof \Associacao\Entity\Condominio) {
                \Core\Util\Message::setMessage("Associação de condomíno salva com sucesso", 'success');
                return $this->redirect()->toRoute('associacao', array('action' => 'condominio'));
            } else {
                $msg = "Erro ao salvar associação do condomínio";
                if(is_array($result)) {
                    $msg .= ' - Erro: ' . $result['error'];
                }
                \Core\Util\Message::setMessage($msg, 'danger');
            }            
        }
        return $view;
    }
    
    /**
     * Exclui um condomínio
     * @return type
     */
    public function delcondo()
    {
        $request = $this->getRequest();
        
        $cr = $this->getServiceManager()->get(\Associacao\Repository\CondominioRepository::class);
        
        $cod = filter_var($request->getFromRoute('id'), FILTER_SANITIZE_STRING);
        
        if($cr->obterPorCodUnion($cod)) {
            
            if($cr->apagar($cod)) {
                \Core\Util\Message::setMessage('Condomínio apagado com sucesso', 'success');
            } else {
                \Core\Util\Message::setMessage('Erro ao apagar condomínio', 'danger');   
            }
        }
        
        return $this->redirect()->toRoute('associacao', array('action' => 'condominio'));
    }
    
    /**
     * Lista as unidades
     * @return \Core\Helper\Model\ViewModel
     */
    public function unidade()
    {
        $condominio = filter_var($this->getRequest()->getFromRoute('id'), FILTER_SANITIZE_NUMBER_INT);
        $ur = $this->getServiceManager()->get(\Associacao\Repository\UnidadeRepository::class);
        $unidades = $ur->listar($condominio);
        return new \Core\Helper\Model\ViewModel(array(
            'unidades'   =>  $unidades
        ));
    }

    
    /**
     * Edição de uma unidade
     * @return \Core\Helper\Model\ViewModel
     */
    public function editunidade()
    {
        $request = $this->getRequest();
        $postData = $request->getPost();
        
        $view = new \Core\Helper\Model\ViewModel();
        
        $ur = $this->getServiceManager()->get(\Associacao\Repository\UnidadeRepository::class);
        $cr = $this->getServiceManager()->get(\Associacao\Repository\CondominioRepository::class);
        
        $condominios = $cr->listar();
        $view->addVar('condominios', $condominios);
        
        $cod = filter_var($request->getFromRoute('id'), FILTER_SANITIZE_STRING);
        if($cod) {
            $unidade = $ur->obterPorId($cod);

            if($unidade) {
                $unidade->setCondominio($cr->obterPorCodUnion($unidade->getCondominio()));
                $view->addVar('unidade', $unidade);
            }
        }
        
        if($postData) {
            $unidade = new \Associacao\Entity\Unidade();
            $unidade->setId(filter_var($postData['id'], FILTER_SANITIZE_NUMBER_INT));
            $unidade->setCondominio(filter_var($postData['condominio'], FILTER_SANITIZE_STRING));
            $unidade->setBloco(filter_var($postData['bloco'], FILTER_SANITIZE_STRING));
            $unidade->setUnidade(filter_var($postData['unidade'], FILTER_SANITIZE_STRING));
            $unidade->setCodBlocoGoSoft(filter_var($postData['codBlocoGoSoft'], FILTER_SANITIZE_STRING));
            $unidade->setCodUnidadeGoSoft(filter_var($postData['codUnidadeGoSoft'], FILTER_SANITIZE_STRING));
            
            $result = $ur->salvar($unidade);
            if($result instanceof \Associacao\Entity\Unidade) {
                \Core\Util\Message::setMessage("Associação de unidade salva com sucesso", 'success');
                return $this->redirect()->toRoute('associacao', array('action' => 'unidade', 'id'   =>  $unidade->getCondominio()));
            } else {
                $msg = "Erro ao salvar associação de unidade";
                if(is_array($result)) {
                    $msg .= ' - Erro: ' . $result['error'];
                }
                \Core\Util\Message::setMessage($msg, 'danger');
            }            
        }
        return $view;
    }
    
        
    /**
     * Lista as contas
     * @return \Core\Helper\Model\ViewModel
     */
    public function conta()
    {
        $cr = $this->getServiceManager()->get(\Associacao\Repository\ContaRespository::class);
        $contas = $cr->listar();
        return new \Core\Helper\Model\ViewModel(array(
            'contas'   =>  $contas
        ));
    }
    
    /**
     * Edição de uma conta
     * @return \Core\Helper\Model\ViewModel
     */
    public function editconta()
    {
        $request = $this->getRequest();
        $postData = $request->getPost();
        
        $view = new \Core\Helper\Model\ViewModel();
        
        $cr = $this->getServiceManager()->get(\Associacao\Repository\ContaRespository::class);
        
        $cod = filter_var($request->getFromRoute('id'), FILTER_SANITIZE_STRING);
        if($cod) {
            $conta = $cr->obterPorCodUnion($cod);

            if($conta) {
                $view->addVar('conta', $conta);
            }
        }
        
        if($postData) {
            $contaAssoc = new \Associacao\Entity\Conta();
            $contaAssoc->setDescricao(filter_var($postData['descricao'], FILTER_SANITIZE_STRING));
            $contaAssoc->setCodUnionWeb(filter_var($postData['codUnionWeb'], FILTER_SANITIZE_STRING));
            $contaAssoc->setCodGoSoft(filter_var($postData['codGoSoft'], FILTER_SANITIZE_STRING));
            
            $result = $cr->salvar($contaAssoc);
            if($result instanceof \Associacao\Entity\Conta) {
                \Core\Util\Message::setMessage("Associação de conta salva com sucesso", 'success');
                return $this->redirect()->toRoute('associacao', array('action' => 'conta'));
            } else {
                $msg = "Erro ao salvar associação do conta";
                if(is_array($result)) {
                    $msg .= ' - Erro: ' . $result['error'];
                }
                \Core\Util\Message::setMessage($msg, 'danger');
            }            
        }
        return $view;
    }
    
    /**
     * Exclui uma conta
     * @return type
     */
    public function delconta()
    {
        $request = $this->getRequest();
        
        $cr = $this->getServiceManager()->get(\Associacao\Repository\ContaRespository::class);
        
        $cod = filter_var($request->getFromRoute('id'), FILTER_SANITIZE_STRING);
        
        if($cr->obterPorCodUnion($cod)) {
            
            if($cr->apagar($cod)) {
                \Core\Util\Message::setMessage('Conta apagada com sucesso', 'success');
            } else {
                \Core\Util\Message::setMessage('Erro ao apagar conta', 'danger');   
            }
        }
        
        return $this->redirect()->toRoute('associacao', array('action' => 'conta'));
    }
    
    
    public function emissao()
    {
        $cr = $this->getServiceManager()->get(\Associacao\Repository\ContaRespository::class);
        $emissoes = $cr->listar();
        return new \Core\Helper\Model\ViewModel(array(
            'emissoes'   =>  $emissoes
        ));
    }
    
    public function editemissao()
    {
        $request = $this->getRequest();
        $postData = $request->getPost();
        
        $view = new \Core\Helper\Model\ViewModel();
        
        $cr = $this->getServiceManager()->get(\Associacao\Repository\ContaRespository::class);
        
        $cod = filter_var($request->getFromRoute('id'), FILTER_SANITIZE_STRING);
        if($cod) {
            $emissao = $cr->obterPorCodUnion($cod);

            if($emissao) {
                $view->addVar('emissao', $emissao);
            }
        }
        
        if($postData) {
            $emissaoAssoc = new \Associacao\Entity\Conta();
            $emissaoAssoc->setCodUnionWeb(filter_var($postData['codUnionWeb'], FILTER_SANITIZE_STRING));
            $emissaoAssoc->setCodGoSoft(filter_var($postData['codGoSoft'], FILTER_SANITIZE_STRING));
            
            $result = $cr->salvar($emissaoAssoc);
            if($result instanceof \Associacao\Entity\Conta) {
                \Core\Util\Message::setMessage("Associação de emissao salva com sucesso", 'success');
                return $this->redirect()->toRoute('associacao', array('action' => 'emissao'));
            } else {
                $msg = "Erro ao salvar associação do emissao";
                if(is_array($result)) {
                    $msg .= ' - Erro: ' . $result['error'];
                }
                \Core\Util\Message::setMessage($msg, 'danger');
            }            
        }
        return $view;
    }
    
    public function delemissao()
    {
        $request = $this->getRequest();
        
        $cr = $this->getServiceManager()->get(\Associacao\Repository\ContaRespository::class);
        
        $cod = filter_var($request->getFromRoute('id'), FILTER_SANITIZE_STRING);
        
        if($cr->obterPorCodUnion($cod)) {
            
            if($cr->apagar($cod)) {
                \Core\Util\Message::setMessage('Conta apagada com sucesso', 'success');
            } else {
                \Core\Util\Message::setMessage('Erro ao apagar emissao', 'danger');   
            }
        }
        
        return $this->redirect()->toRoute('associacao', array('action' => 'emissao'));
    }
}
