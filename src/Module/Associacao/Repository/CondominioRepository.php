<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Repository;

use Associacao\Entity\Condominio;
use Core\DB\Connection;

/**
 * Repositório responsável por salvar os dados de associação dos condomínios
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class CondominioRepository
{

    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var CondominioRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param Connection $conn
     */
    private function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Gerenciador de instância para o repositório
     * @param Connection $conn
     * @return CondominioRepository
     */
    public static function getInstance(Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }

    public function salvar(Condominio $condominio)
    {
        $id = (int) $condominio->getId();

        $stmt = $this->conn->getHandler()->prepare('INSERT INTO '
                . 'condominio (nome, codUnionWeb, codGoSoft, multa, juros, dias_limite, cedente, codigo_conta, codigo_carteira) '
                . 'VALUES '
                . '(:nome, :codUnionWeb, :codGoSoft, :multa, :juros, :dias_limite, :cedente, :codigo_conta, :codigo_carteira) '
                . 'ON CONFLICT(codUnionWeb) DO UPDATE SET nome=:nome, codGoSoft=:codGoSoft, '
                . 'multa=:multa, juros=:juros, dias_limite=:dias_limite, cedente=:cedente, '
                . 'codigo_conta= :codigo_conta, codigo_carteira=:codigo_carteira');
        if ($stmt) {
            $stmt->bindValue(':nome', $condominio->getNome(), \PDO::PARAM_STR);
            $stmt->bindValue(':codUnionWeb', $condominio->getCodUnionWeb(), \PDO::PARAM_STR);
            $stmt->bindValue(':codGoSoft', $condominio->getCodGoSoft(), \PDO::PARAM_STR);
            $stmt->bindValue(':multa', $condominio->getMulta(), \PDO::PARAM_STR);
            $stmt->bindValue(':juros', $condominio->getJuros(), \PDO::PARAM_STR);
            $stmt->bindValue(':dias_limite', $condominio->getDias_limite(), \PDO::PARAM_INT);
            $stmt->bindValue(':cedente', $condominio->getCedente(), \PDO::PARAM_STR);
            $stmt->bindValue(':codigo_conta', $condominio->getCodigo_conta(), \PDO::PARAM_STR);
            $stmt->bindValue(':codigo_carteira', $condominio->getCodigo_carteira(), \PDO::PARAM_STR);

            
            if ($stmt->execute()) {
                $this->conn->doCommit();

                return $condominio;
            } else {
                $errorInfo = $stmt->errorInfo();
                return array('success'  =>  false, 'error'  => ($errorInfo[0] == 23000 ? 'Código GoSoft já existe em outro cadastro' : ''));
            }
        } else {
            var_dump($this->conn->getHandler()->errorInfo());
        }


        return null;
    }
    
    public function listar()
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM condominio');
        
        if($stmt->execute()) {
            $condominios = $stmt->fetchAll(\PDO::FETCH_CLASS, Condominio::class);
            
            return new \ArrayObject($condominios);
        }
        
        return null;
    }
    
    public function obterPorCodUnion($cod)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM condominio WHERE codUnionWeb = :codUnionWeb');
        $stmt->bindValue(':codUnionWeb', $cod, \PDO::PARAM_STR);
        
        if($stmt->execute()) {
            $condominio = $stmt->fetchObject(Condominio::class);
            
            return $condominio;
        }
        
        return null;
    }
    
    public function apagar($cod) 
    {
        $stmt = $this->conn->getHandler()->prepare('DELETE FROM condominio WHERE codUnionWeb = :codUnionWeb');
        $stmt->bindValue(':codUnionWeb', $cod, \PDO::PARAM_STR);
        
        
        if($stmt->execute()) {
            $this->conn->doCommit();
            return true;
        }
        
        return false;
    }

}
