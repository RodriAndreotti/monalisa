<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Repository;

use ArrayObject;
use Associacao\Entity\Conta;
use Core\DB\Connection;
use PDO;

/**
 * Repositório responsável por salvar os dados de associação das contas de consumo
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ContaRespository
{

    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var CondominioRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param Connection $conn
     */
    private function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Gerenciador de instância para o repositório
     * @param Connection $conn
     * @return CondominioRepository
     */
    public static function getInstance(Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }

    public function salvar(Conta $conta)
    {
        $exist = $this->obterPorCodUnion($conta->getCodUnionWeb());
        if ($exist) {
            $sql = 'UPDATE plano_conta SET codGoSoft = :codGoSoft WHERE codUnionWeb = :codUnionWeb';
        } else {
            $sql = 'INSERT INTO '
                    . 'plano_conta (codUnionWeb, codGoSoft, descricao) '
                    . 'VALUES '
                    . '(:codUnionWeb, :codGoSoft, :descricao)';
        }



        $stmt = $this->conn->getHandler()->prepare($sql);
        if ($stmt) {
            if (!$exist) {
                $stmt->bindValue(':descricao', $conta->getDescricao(), PDO::PARAM_STR);
            }
            $stmt->bindValue(':codUnionWeb', $conta->getCodUnionWeb(), PDO::PARAM_STR);
            $stmt->bindValue(':codGoSoft', $conta->getCodGoSoft(), PDO::PARAM_STR);



            if ($stmt->execute()) {
                $this->conn->doCommit();

                return $conta;
            } else {
                $errorInfo = $stmt->errorInfo();
                return array('success' => false, 'error' => ($errorInfo[0] == 23000 ? 'Código GoSoft já existe em outro cadastro' : ''));
            }
        } else {
            var_dump($this->conn->getHandler()->errorInfo());
        }


        return null;
    }

    public function listar()
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM plano_conta');

        if ($stmt->execute()) {
            $contas = $stmt->fetchAll(PDO::FETCH_CLASS, Conta::class);

            return new ArrayObject($contas);
        }

        return null;
    }

    public function listarParaExportacao()
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM plano_conta WHERE codGoSoft IS NOT NULL');

        if ($stmt->execute()) {
            $contas = $stmt->fetchAll(PDO::FETCH_CLASS, Conta::class);

            return new ArrayObject($contas);
        }

        return null;
    }

    public function obterPorCodUnion($cod)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT * FROM plano_conta WHERE codUnionWeb = :codUnionWeb');
        $stmt->bindValue(':codUnionWeb', $cod, PDO::PARAM_STR);

        if ($stmt->execute()) {
            $conta = $stmt->fetchObject(Conta::class);

            return $conta;
        }

        return null;
    }

    public function apagar($cod)
    {
        $stmt = $this->conn->getHandler()->prepare('DELETE FROM plano_conta WHERE codUnionWeb = :codUnionWeb');
        $stmt->bindValue(':codUnionWeb', $cod, PDO::PARAM_STR);


        if ($stmt->execute()) {
            $this->conn->doCommit();
            return true;
        }

        return false;
    }

}
