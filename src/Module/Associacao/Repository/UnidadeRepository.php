<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Repository;

/**
 * Repositório das Unidades
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class UnidadeRepository
{

    /**
     * Conexão com o banco de dados
     * @var Connection 
     */
    private $conn;

    /**
     * Instância da classe
     * @var EmissaoRepository 
     */
    private static $instance;

    /**
     * Inicializa a classe com a conexão
     * @uses \Core\DB\Connection Classe responsável para conexão com o banco de dados
     * @param \Core\DB\Connection $conn
     */
    private function __construct(\Core\DB\Connection $conn)
    {
        $this->conn = $conn;

        $sqlCreate = 'CREATE TABLE IF NOT EXISTS "unidade" (
                "condominio"	INTEGER NOT NULL,
                "bloco"	TEXT NOT NULL,
                "unidade"	INTEGER NOT NULL,
                "codBlocoGoSoft"	TEXT,
                "codUnidadeGoSoft"	INTEGER,
                PRIMARY KEY("condominio","bloco","unidade")
        );';

        $this->conn->getHandler()->exec($sqlCreate);
        $sqlCreateUnq = 'CREATE UNIQUE INDEX IF NOT EXISTS "uq_unidade" ON "unidade" (
                        "condominio",
                        "bloco",
                        "unidade"
                );';

        $this->conn->getHandler()->exec($sqlCreateUnq);
        $sqlCreateUnq2 = 'CREATE INDEX IF NOT EXISTS uq_unidade_gosoft ON unidade (
                    codBlocoGoSoft,
                    codUnidadeGoSoft
                );';

        $this->conn->getHandler()->exec($sqlCreateUnq2);
        $this->conn->doCommit();
    }

    /**
     * Gerenciador de instância para o repositório
     * @param \Core\DB\Connection $conn
     * @return UnidadeRepository
     */
    public static function getInstance(\Core\DB\Connection $conn)
    {
        if (!self::$instance) {
            self::$instance = new self($conn);
        }

        return self::$instance;
    }

    /**
     * Salva uma unidade
     * @param \Associacao\Entity\Unidade $unidade
     * @return \Associacao\Entity\Unidade
     */
    public function salvar(\Associacao\Entity\Unidade $unidade)
    {
        $id = (int) $unidade->getId();
        
        if($id == 0) {
            $sql = 'INSERT INTO '
                . 'unidade (condominio, bloco, unidade, codBlocoGoSoft, codUnidadeGoSoft) '
                . 'VALUES '
                . '(:condominio, :bloco, :unidade, :codBlocoGoSoft, :codUnidadeGoSoft)';
        } else {
            $sql = 'UPDATE unidade SET codBlocoGoSoft=:codBlocoGoSoft, codUnidadeGoSoft=:codUnidadeGoSoft '
                . 'WHERE  '
                . '(condominio = :condominio AND bloco = :bloco AND unidade = :unidade)';
        }

        $stmt = $this->conn->getHandler()->prepare($sql);
        if ($stmt) {
            $stmt->bindValue(':condominio', $unidade->getCondominio(), \PDO::PARAM_INT);
            $stmt->bindValue(':bloco', $unidade->getBloco(), \PDO::PARAM_STR);
            $stmt->bindValue(':unidade', $unidade->getUnidade(), \PDO::PARAM_INT);
            $stmt->bindValue(':codBlocoGoSoft', $unidade->getCodBlocoGoSoft(), \PDO::PARAM_STR);
            $stmt->bindValue(':codUnidadeGoSoft', $unidade->getCodUnidadeGoSoft(), \PDO::PARAM_STR);


            if ($stmt->execute()) {
                $this->conn->doCommit();

                return $unidade;
            } else {
                $errorInfo = $stmt->errorInfo();
                var_dump($errorInfo);
                return array('success' => false, 'error' => ($errorInfo[0] == 23000 ? 'Código GoSoft já existe em outro cadastro' : ''));
            }
        } else {
            var_dump($this->conn->getHandler()->errorInfo());
        }


        return null;
    }
    

    /**
     * Obtém listagem de unidades por condomínio
     * @param integer $condominio
     * @return array
     */
    public function listar($condominio)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT rowid,* FROM '
                . 'unidade WHERE condominio = :condominio');

        if ($stmt) {
            $stmt->bindParam(':condominio', $condominio, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                return $stmt->fetchAll(\PDO::FETCH_CLASS, \Associacao\Entity\Unidade::class);
            }
        }

        return null;
    }

    /**
     * Obtém unidade por ID (RowID do SQLite
     * @param integer $id
     * @return \Associacao\Entity\Unidade
     */
    public function obterPorId($id)
    {
        $stmt = $this->conn->getHandler()->prepare('SELECT rowid,* FROM unidade WHERE rowid = :id');
        $stmt->bindValue(':id', $id, \PDO::PARAM_STR);

        if ($stmt->execute()) {
            $unidade = $stmt->fetchObject(\Associacao\Entity\Unidade::class);

            return $unidade;
        }

        return null;
    }

}
