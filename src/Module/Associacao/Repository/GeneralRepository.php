<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Repository;

use Importacao\Repository\EmissaoRepository;

/**
 * Repositório para gerar array de Associações
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class GeneralRepository
{
    private $condRepository;
    private $contaRepository;
    private $emissaoRepository;
    private $reciboRepository;
    
    public function __construct(CondominioRepository $condRepository, ContaRespository $contaRepository,  EmissaoRepository $emissaoRepository, \Importacao\Repository\ReciboRepository $reciboRepository)
    {
        $this->condRepository = $condRepository;
        $this->contaRepository = $contaRepository;
        $this->emissaoRepository = $emissaoRepository;
        $this->reciboRepository = $reciboRepository;
    }
    
    public function getAssocTable()
    {
        $table = array(
            'condominio'    =>  array(),
            'conta' =>  array(),
            'emissao'   =>  array(),
            'recibo'    =>  array()
        );
        
        $condominios = $this->condRepository->listar();
       
        foreach($condominios as $c) {
            $table['condominio'][$c->getCodUnionWeb()] = $c;
        }
        
        $contas = $this->contaRepository->listarParaExportacao();
        
        foreach($contas as $conta) {
            $table['conta'][$conta->getCodUnionWeb()] = $conta->getCodGoSoft();
        }
        
        $emissoes = $this->emissaoRepository->listar();
        
        foreach($emissoes as $emissao){
            $table['emissao'][$emissao->getEmissao()] = $emissao->getCodigo();
        }
        
        $recibos = $this->reciboRepository->listar();
        
        foreach($recibos as $recibo) {
            $table['recibo'][$recibo->getNum_recibo_union()] = $recibo->getNum_recibo_gosoft();
        }
        
        
        return $table;
    }

}
