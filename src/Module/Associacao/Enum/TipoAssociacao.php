<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Associacao\Enum;

/**
 * Enum para tipos de associação / Importação / Exportação
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class TipoAssociacao
{
    const TIPO_EMISSAO = 'emissao', TIPO_LANCAMENTO_EMISSAO = 'lancemissao', TIPO_RECIBO = 'recibo' , TIPO_LANCAMENTO_RECIBO = 'lancrecibo', TIPO_CONDOMINIO = 'condominio',  TIPO_UNIDADE = 'unidade';
    
    public static function tipoToString($value)
    {
        switch ($value) {
            case self::TIPO_CONDOMINIO: return 'Condomínio';
            case self::TIPO_UNIDADE: return 'Unidade';
            case self::TIPO_EMISSAO: return 'Emissões';
            case self::TIPO_LANCAMENTO_EMISSAO: return 'Lançamentos de Emissões';
            case self::TIPO_RECIBO: return 'Recibos';
            case self::TIPO_LANCAMENTO_RECIBO: return 'Lançamentos de Recibos';
            default: return null;
        }
    }
}
