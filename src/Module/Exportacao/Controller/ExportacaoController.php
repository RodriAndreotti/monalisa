<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Exportacao\Controller;

/**
 * Description of ExportacaoController
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ExportacaoController extends \Core\Controller\AbstractController
{

    public function index()
    {
        $sm = $this->getServiceManager();
        $request = $this->getRequest();
        $post = $request->getPost();

        if ($post) {
            $assocRep = $sm->get(\Associacao\Repository\GeneralRepository::class);

            $tabelaAssoc = $assocRep->getAssocTable();

            switch (trim($post['tipo'])) {
                case \Associacao\Enum\TipoAssociacao::TIPO_EMISSAO: $this->exportEmissao($tabelaAssoc);
                    break;
                case \Associacao\Enum\TipoAssociacao::TIPO_LANCAMENTO_EMISSAO: $this->exportLancamentoEmissao($tabelaAssoc);
                    break;
                case \Associacao\Enum\TipoAssociacao::TIPO_RECIBO: $this->exportRecibo($tabelaAssoc);
                    break;
                case \Associacao\Enum\TipoAssociacao::TIPO_LANCAMENTO_RECIBO: $this->exportLancamentoRecibo($tabelaAssoc);
                    break;
                default:
                    var_dump('Erro');
            }
            exit;
        }

        return new \Core\Helper\Model\ViewModel();
    }

    private function exportEmissao($tabelaAssoc)
    {

        $emissoes = $this->getServiceManager()->get(\Importacao\Repository\EmissaoRepository::class)->listar();

        $txt = '';
        foreach ($emissoes as $emissao) {
            $linha = $emissao->toStringForLine($tabelaAssoc);
            if($linha) {
                $txt .=  $linha . "\n";
            }
        }

        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=emissao.txt");
        echo $txt;
        exit;
    }

    private function exportLancamentoEmissao($tabelaAssoc)
    {
        $lancamentos = $this->getServiceManager()->get(\Importacao\Repository\LancamentoEmissaoRepository::class)->listar();

        $txt = '';
        foreach ($lancamentos as $lancamento) {
            $txt .= $lancamento->toStringForLine($tabelaAssoc) . "\n";
        }

        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=lancamento_emissao.txt");
        echo $txt;
        exit;
    }

    private function exportRecibo($tabelaAssoc)
    {
        $recibos = $this->getServiceManager()->get(\Importacao\Repository\ReciboRepository::class)->listar();

        $txt = '';
        foreach ($recibos as $recibo) {
            $linha = $recibo->toStringForLine($tabelaAssoc);
            if($linha) {
                $txt .=  $linha . "\n";
            }
        }

        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=recibo.txt");
        echo $txt;
        exit;
    }

    private function exportLancamentoRecibo($tabelaAssoc)
    {
        $lancamentos = $this->getServiceManager()->get(\Importacao\Repository\LancamentoReciboRepository::class)->listar();

        $txt = '';
        foreach ($lancamentos as $lancamento) {
            $txt .= $lancamento->toStringForLine($tabelaAssoc) . "\n";
        }

        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=lancamento_recibo.txt");
        echo $txt;
        exit;
    }

}
