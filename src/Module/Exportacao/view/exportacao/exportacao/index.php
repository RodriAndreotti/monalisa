<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="box-title">Exportação de Layout GoSoft</h3>
                        </div>
                        <div class="col-md-2 text-right">
                            <button href="<?= $this->url('importacao', array('action' => 'index')); ?>" class="btn btn-sm btn-primary">
                                <i class="fa fa-fw fa-download"></i> Exportar
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select name="tipo" class="form-control">
                                    <option value="<?= Associacao\Enum\TipoAssociacao::TIPO_EMISSAO; ?> "><?= Associacao\Enum\TipoAssociacao::tipoToString(Associacao\Enum\TipoAssociacao::TIPO_EMISSAO); ?></option>
                                    <option value="<?= Associacao\Enum\TipoAssociacao::TIPO_LANCAMENTO_EMISSAO; ?> "><?= Associacao\Enum\TipoAssociacao::tipoToString(Associacao\Enum\TipoAssociacao::TIPO_LANCAMENTO_EMISSAO); ?></option>
                                    <option value="<?= Associacao\Enum\TipoAssociacao::TIPO_RECIBO; ?> "><?= Associacao\Enum\TipoAssociacao::tipoToString(Associacao\Enum\TipoAssociacao::TIPO_RECIBO); ?></option>
                                    <option value="<?= Associacao\Enum\TipoAssociacao::TIPO_LANCAMENTO_RECIBO; ?> "><?= Associacao\Enum\TipoAssociacao::tipoToString(Associacao\Enum\TipoAssociacao::TIPO_LANCAMENTO_RECIBO); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</form>