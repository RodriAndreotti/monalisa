<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'routes'    =>  array(
        'exportacao' =>  array(
            'route'     =>  '/exportacao[/:action][/:id]',
            'controller'    => Exportacao\Controller\ExportacaoController::class,
            'view-dir'      =>  __DIR__  . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view',
            'padroes'  =>  array(
                'action'    =>  '[a-z][a-zA-Z0-9]+',
                'id'        =>  '[0-9]+'
            )
        )
    ),
    'services' => array(
        // User\Repository\UserRepository::class => \User\Factory\Repository\UserRepositoryFactory::class
    )
);