<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Service\Validator;

/**
 * Validator abstrato, deve ser implementado pelos validadores específicos
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
abstract class AbstractValidator
{
    

    public abstract static function getInstance();

    

    /**
     * Valida se o campo está vazio
     * @param mixed $valor
     * @param string $nomeCampo
     * @return boolean
     */
    protected function validaVazio($valor, $nomeCampo)
    {
        
        if (!$valor || empty($valor) || $valor === 0) {
            $this->errors[$nomeCampo] = 'O campo não pode estar vazio';
            return false;
        }

        return true;
    }

    /**
     * Verifica se o valor é inteiro
     * @param mixed $valor
     * @param string $nomeCampo
     * @return boolean
     */
    protected function validaInteiro($valor, $nomeCampo)
    {
        if ($valor) {
            if ($valor != intval($valor)) {
                $this->errors[$nomeCampo] = 'O campo deve conter um número inteiro';
                return false;
            }
        }

        return true;
    }

    /**
     * Valida se o valor contém quantidade mínima ou máxima de caracteres
     * @param mixed $valor
     * @param int $min
     * @param int $max
     * @param string $nomeCampo
     * @return boolean
     */
    protected function validaMinMax($valor, $min, $max, $nomeCampo)
    {
        if ($valor) {
            if (mb_strlen($valor) > $max) {
                $this->errors[$nomeCampo] = 'O campo deve conter no máximo ' . $max . ' caracteres';
                return false;
            } elseif (mb_strlen($valor) < $min) {
                $this->errors[$nomeCampo] = 'O campo deve conter no mínimo ' . $min . ' caracteres';
                return false;
            }
        }

        return true;
    }

    /**
     * Valida se o campo está recebendo um valor com tamanho específico
     * @param string $valor
     * @param int $tamanho
     * @param string $nomeCampo
     * @return boolean
     */
    protected function validaTamanhoExato($valor, $tamanho, $nomeCampo)
    {
        if ($valor) {
            if (mb_strlen($valor) != $tamanho) {
                $this->errors[$nomeCampo] = 'O campo deve conter ' . $tamanho . ' caracteres';
                return false;
            }
        }

        return true;
    }

    /**
     * Valida se o campo está recebendo Sim ou Não
     * @param string $valor
     * @param string $nomeCampo
     * @return boolean
     */
    protected function validaSimNao($valor, $nomeCampo)
    {
        if ($valor != 'S' && $valor != 'N') {
            $this->errors[$nomeCampo] = 'O campo deve receber os Valores (S)im ou (N)ão';
            return false;
        }

        return true;
    }
    
    /**
     * retorna os errors gerados
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    
    abstract public function isValid($object);
}
