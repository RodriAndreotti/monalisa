<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Service\Writer;

/**
 * Classe responsável por escrita em arquivo
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class FileWriter
{

    private $local;
    private $file;
    private $lines=1;

    public function __construct($local)
    {
        $this->setLocal($local);
    }

    /**
     * Define o local onde o arquivo será salvo
     * @param string $local
     * @return $this
     */
    private function setLocal($local)
    {
        if (file_exists($local) && is_dir($local)) {
            if (is_writable($local)) {
                $this->local = $local;
            } else {
                trigger_error('Diretório de arquivos sem permissão de escrita', E_USER_ERROR);
            }
        } else {
            trigger_error('Diretório de arquivos inacessível', E_USER_ERROR);
        }


        return $this;
    }

    /**
     * Abre um arquivo para escrita
     * @param string $filename
     * @return $this
     */
    public function open($filename)
    {
        $this->file = fopen($this->local . DIRECTORY_SEPARATOR . $filename, 'w+') or die('Erro ao abrir arquivo');
        while (!feof($this->file)) {
            $linha = fgets($this->file);
            $this->lines++;
        }
        
        return $this;
    }

    /**
     * Insere uma linha no arquivo
     * @param string $texto
     * @return $this
     */
    public function addLine($texto)
    {
        if ($this->file) {
            if ($this->lines > 1) {
                fwrite($this->file, $texto. "\r\n");
            } else {
                fwrite($this->file, $texto);
            }
            $this->lines++;
        } else {
            trigger_error('O arquivo não foi aberto!');
        }
        return $this;
    }

    public function close()
    {
        fclose($this->file);
    }

}
