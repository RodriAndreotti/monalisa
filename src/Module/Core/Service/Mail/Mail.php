<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Service\Mail;

/**
 * Description of Mail
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Mail
{

    /**
     * Vari[avel do serviço de envio
     * @var \PHPMailer\PHPMailer\PHPMailer 
     */
    private $service;

    public function __construct($config)
    {
        $this->validateConfig($config);

        $this->service = new \PHPMailer\PHPMailer\PHPMailer();

        $this->service->isSMTP();

        $this->service->Host = $config['host'];
        $this->service->SMTPAuth = true;
        $this->service->Port = $config['port'];
        $this->service->Username = $config['user'];
        $this->service->Password = $config['passwd'];

        $this->service->From = $config['user'];
        $this->service->FromName = 'Não responda';

        $this->service->setLanguage('pt_BR');

        $this->service->CharSet = 'UTF-8';

        //$this->service->SMTPDebug = 4;

        $this->service->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        if ($config['ssl']) {
            $this->service->SMTPSecure = isset($config['ssl']) ? $config['ssl'] : '';
        }
    }

    /**
     * Adiciona um destinatário na mensagem
     * @param string $nomeDestinatario
     * @param string $emailDestinatario
     */
    public function addDestinatario($nomeDestinatario, $emailDestinatario)
    {
        $this->service->addAddress($emailDestinatario, $nomeDestinatario);
    }

    /**
     * Insere o assunto na mensagem
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->service->Subject = $subject;
    }

    /**
     * Insere a mensagem no e-mail
     * @param string $mensagem
     */
    public function setMessage($mensagem)
    {
        $this->service->msgHTML($mensagem);
        $this->service->AltBody = strip_tags($mensagem);
    }

    /**
     * Insere o ReplyTo na mensagem
     * @param string $nome
     * @param string $email
     */
    public function setReplyTo($nome, $email)
    {
        $this->service->addReplyTo($email, $nome);
    }

    /**
     * Envia a mensagem
     * @return boolean
     */
    public function send()
    {
        try {
            return $this->service->send();
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Valida a configuração do serviço
     * @param array $config
     * @throws \Exception
     */
    private function validateConfig($config)
    {
        if (!is_array($config)) {
            throw new \Exception('Parâmetro de configuração não é um array', 500);
        }
        if (!isset($config['host'])) {
            throw new \Exception('Não foi encontrado o host do servidor SMTP', 500);
        }
        if (!isset($config['user'])) {
            throw new \Exception('Não foi encontrado o usuáro do servidor SMTP', 500);
        }
        if (!isset($config['passwd'])) {
            throw new \Exception('Não foi encontrada senha para o servidor SMTP', 500);
        }
        if (!isset($config['port'])) {
            throw new \Exception('Não foi encontrada a porta do servidor SMTP', 500);
        }
    }

}
