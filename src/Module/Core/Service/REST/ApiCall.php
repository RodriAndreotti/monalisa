<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Service\REST;

/**
 * Description of ApiCall
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c)2016, Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ApiCall
{
    
    
    
    /**
     * Processa a requisição usando CUrl
     * @param string $url
     * @return \stdClass
     */
    protected static function doRequest($url)
    {
        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, self::getUserAgent()); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);

            $result = json_decode(curl_exec($ch));

            return $result;
        } catch (Exception $e) {
            var_dump($e->getMessages());
        }
        return null;
    }
    
    private static function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }
}
