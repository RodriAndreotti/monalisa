<?php

namespace Core\Cache;

/**
 * Sistema de cache
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Cache
{

    /**
     * 
     * @var integer Tempo para o cache em minutos
     */
    private $time = 60;

    /**
     *
     * @var string Local onde o cache será salvo
     */
    private $local;

    /**
     * Inicializa a classe e define o local onde o cache será armazenado
     * @uses Cache::setLocal()
     * @param string $local
     */
    public function __construct($local)
    {
        $this->setLocal($local);
    }

    /**
     * Define o local onde o cache será salvo
     * @param string $local
     * @return $this
     */
    private function setLocal($local)
    {
        if (!file_exists($local)){
            trigger_error('Diretório de cache não encontrado', E_USER_ERROR);
        } elseif(!is_dir($local)) {
            trigger_error('Caminho para diretório de cache não aponta para um diretório', E_USER_ERROR);
        } elseif(!is_writable($local)){
            trigger_error('Diretório de cache inacessível', E_USER_ERROR);
        } else {
            $this->local = $local;
        }

        return $this;
    }

    /**
     * Gera o local onde o arquivo será salvo
     * @param string $key
     * @return string
     */
    private function generateFileLocation($key)
    {
        return $this->local . DIRECTORY_SEPARATOR . sha1($key) . '.tmp';
    }

    /**
     * 
     * Cria o arquivo de cache
     * 
     * @uses Cache::generateFileLocation()
     * 
     * @param string $key
     * @param mixed $content
     * 
     * @return boolean
     */
    private function generateCacheFile($key, $content)
    {
        $file = $this->generateFileLocation($key);

        return file_put_contents($file, $content) || trigger_error('Não foi possível criar o arquivo de cache', E_USER_ERROR);
    }

    /**
     * 
     * Salva um valor em cache
     * 
     * @uses Cache::generateCacheFiles
     * 
     * @param string $key
     * @param mixed $content
     * @param integer $time Tempo em minutos
     * 
     * @return boolean
     */
    public function saveCache($key, $content, $time = null)
    {
        $time = strtotime(($time ? $time : $this->time) . ' minutes');

        $content = serialize(array(
            'expira' => $time,
            'content' => $content
        ));

        return $this->generateCacheFile($key, $content);
    }

    /**
     * Recupera um valor salvo no cache
     * 
     * @uses Cache::generateFileLocation()
     * 
     * @param string $key
     * 
     * @return mixed Valor do cache salvo ou null
     */
    public function readCache($key)
    {
        $file = $this->generateFileLocation($key);

        if (is_file($file) && is_readable($file)) {
            $cache = unserialize(file_get_contents($file));

            if ($cache['expira'] > time()) {
                return $cache['content'];
            } else {
                unlink($file);
            }
        }

        return null;
    }

}
