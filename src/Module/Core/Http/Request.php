<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Http;

/**
 * Requisição HTTP
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Request
{

    private $routeParams = array();
    private $postFields = array();
    
    private $url;
    
    public function __construct($url)
    {
        $this->url = rtrim($url, '/');
    }

    public function addRouteParam($param, $value)
    {
        $this->routeParams[$param] = $value;
        return $this;
    }
    
    public function getPost()
    {
        return $this->postFields;
    }

    public function setPostFields($postFields)
    {
        $this->postFields = $postFields;
        return $this;
    }

    /**
     * Retorna parâmetro proveniente da rota
     * @param string $param
     * @return string|null
     */
    public function getFromRoute($param)
    {
        if(isset($this->routeParams[$param])) {
            return $this->routeParams[$param];
        }
        
        return null;
    }
    
    public function getRequestUrl()
    {
        return $this->url;
    }

    /**
     * Redireciona para url requisitada
     * @return $this
     * 
     */
    public function redirect()
    {
        header('Location: ' . $this->getRequestUrl(), true, 302);
        return $this;
    }
}
