<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Manager;

/**
 * Service manager genérico
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Service implements ServiceManagerInterface
{
    private $services;
    private $app;
    
    /**
     * Constrói o service manager com instância da Aplicação
     * @param \Core\Application $app
     */
    public function __construct(\Core\Application $app)
    {
        $this->app = $app;
    }
    
    /**
     * Insere serviço ao manager
     * @param string $service
     * @param \Core\Factory\FactoryInterface $factory
     */
    public function addService($service, $factory)
    {
        $this->services[$service] = $factory;
    }

    /**
     * Obtém um serviço registrado no manager
     * 
     * @param string $service
     * @return object
     * @throws \Exception
     */
    public function get($service)
    {
        if(isset($this->services[$service])){
            return $this->services[$service]->createService($this->app, $service);
        }
        
        throw new \Exception('Factory inválido ou inexistente', 3130) ;
    }

}
