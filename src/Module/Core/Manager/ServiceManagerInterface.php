<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Manager;

/**
 * Description of ServiceInterface
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
interface ServiceManagerInterface
{
    public function addService($service, $factory);
    public function get($service);
}
