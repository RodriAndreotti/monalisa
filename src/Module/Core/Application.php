<?php

namespace Core;

/**
 * Classe de aplicação do sistema, esta classe é responsável pelos métodos 
 * de inicialização do sistema
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Application
{

    /**
     * Array associativo com configurações do sistema
     * @var array 
     */
    private $config;
    
    private $sm;
    
    const URL_SISTEMA = 'localhost';

    public function __construct()
    {
        $this->loadConfig();
    }

    /**
     * Obtém uma configuração pela chave
     * @param string $key
     */
    public function getConfig($key)
    {
        if (is_array($this->config)) {
            if (array_key_exists($key, $this->config)) {
                return $this->config[$key];
            } else {
                throw new \Exception('Configuração não encontrada', 1404);
            }
        } else {
            throw new \Exception('Arquivo de configuração inválido.');
        }
    }

    /**
     * Retorna o caminho absoluto até a raíz do sistema
     * @return string
     */
    public function getAbsolutePath()
    {
        return realpath(__DIR__ . str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, '/../../../'));
    }

    /**
     * Carregas as configurações do sistema
     * @throws \Exception
     */
    private function loadConfig()
    {
        if (!file_exists($this->getAbsolutePath() . '/config/system.config.php')) {
            throw new \Exception('Arquivo de configuração do sistema não encontrado!'
                    . '\n<br>Verifique a existência do arquivo system.config.php no diretório config.');
        }
        $this->config = require_once $this->getAbsolutePath() . '/config/system.config.php';
    }
    
    
    /**
     * 
     * @return Manager\ServiceManagerInterface
     */
    public function getServiceManager()
    {
        return $this->sm;
    }
    
    /**
     * Inicializa sessão
     */
    public function setSession()
    {
        $session = new Helper\SessionHelper();
        if (!$session->hasSession()) {
            $session->start('monalisa_cond');
        }
    }

    public function run()
    {
        
        $this->setSession();
        $routerFactory = new Factory\Route\RouterFactory();
        $router = $routerFactory->createService($this, Route\Router::class);


        $serviceFactory = new Factory\Service\ServiceFactory();
        $sm = $serviceFactory->createService($this, Manager\Service::class);
        
        $this->sm = $sm;
        $this->sm->addService(Route\Router::class, $routerFactory);

        $router->setServiceManager($sm);
        $router->setAcl(new \User\Acl\SysAcl());


        $template = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'view';
        $template .= DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
        $template .= 'template.php';

        $view = new \Core\Helper\View($template, $router, $this->getAbsolutePath());
        $session = new Helper\SessionHelper();
        $session->setVar('publicDir', $this->getAbsolutePath(). $this->getConfig('publicDir'));
        $session->setVar('basePath', $view->basePath(''));
        $session->setVar('absPath', $this->getAbsolutePath());
        

        $view->render();
    }

}
