<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Util;

/**
 * Description of Data
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Data
{

    const PT = 1, ANSI = 2;

    public static function convertToPt(\DateTime $data)
    {
        setlocale(LC_TIME, "pt_BR");
        return strftime('%d de %B, %Y às %H:%M:%S', $data->getTimestamp());
    }

    public static function getFomato($data)
    {
        $formato = self::ANSI;
        if (preg_match("/^\d{1,2}\/\d{1,2}\/\d{4}$/", $data)) {
            $formato = self::PT;
        }
        return $formato;
    }
}