<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Util;

/**
 * Description of Texto
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Text
{

    public static function sanitizeHtml($texto)
    {
        return strip_tags($texto, '<p><a><div><span><b><strong><em><i><u><font>'
                . '<table><thead><tbody><tfoot><tr><th><td><ul><img><ol><li><dl><dt><dd><center><sub>'
                . '<sup><h1><h2><h3><h4><h5><h6><blockquote>');
    }

    public static function getFirstParagraph($texto)
    {
        $texto = substr($texto, strpos($texto, "<p>"), strpos($texto, "</p>") + 4);
        return $texto;
    }

    public static function getBlocoDescado($texto, $termo)
    {
        $inicio = (strpos($texto, $termo) - 100) > 0 ? (strpos($texto, $termo) - 100) : 0;
        $fim = ($inicio > 0) ? strpos($texto, $termo) + 100 : 220;
        $final = substr($texto, $inicio, $fim);
        if (strlen($final) > strlen($texto)) {
            $final = ($inicio > 0 ? '[...]' : '')
                    . $final
                    . ($fim < strlen($texto) ? '[...]' : '');
        }
        $final = str_ireplace($termo, "<span class='highlight'>{$termo}</span>", $final);

        //$final = preg_replace($termo, "<strong><u>$1</u></strong>", $final);
        return $final;
    }

    public static function textToUrl($text, $removeDot = false)
    {
        $text = preg_replace('/\\s/', '-', $text, -1);
        $text = str_replace(array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å'), 'A', $text);
        $text = str_replace(array('à', 'á', 'â', 'ã', 'ä', 'å'), 'a', $text);
        $text = str_replace(array('È', 'É', 'Ê', 'Ë'), 'E', $text);
        $text = str_replace(array('è', 'é', 'ê', 'ë'), 'e', $text);
        $text = str_replace(array('Ì', 'Í', 'Î', 'Ï'), 'I', $text);
        $text = str_replace(array('ì', 'í', 'î', 'ï'), 'i', $text);
        $text = str_replace(array('Ò', 'Ó', 'Ô', 'Õ', 'Ö'), 'O', $text);
        $text = str_replace(array('ò', 'ó', 'ô', 'õ', 'ö'), 'o', $text);
        $text = str_replace(array('Ù', 'Ú', 'Û', 'Ü'), 'U', $text);
        $text = str_replace(array('ù', 'ú', 'û', 'ü'), 'u', $text);
        $text = str_replace('ç', 'c', $text);
        $text = str_replace('Ç', 'C', $text);
        $text = str_replace(array('/', '\\', '+', ',', '@', '!', '?', '#'), '', $text);

        if ($removeDot) {
            $text = str_replace('.', '', $text);
        }

        $text = mb_strtolower($text, 'UTF-8');


        return $text;
    }

    public static function mb_str_pad($input, $pad_length, $pad_string, $pad_style, $encoding = "UTF-8")
    {
        return str_pad($input,
                strlen($input) - mb_strlen($input, $encoding) + $pad_length, $pad_string, $pad_style);
    }
}
