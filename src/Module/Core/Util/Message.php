<?php

namespace Core\Util;

/**
 * Utilitário para tratamento de mensagens entre telas do sistema
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Message
{
     /**
     * Inclui uma mensagem no sistema de mensagens
     * @param string $message
     * @param string $type tipos de alertas disponíveis no bootstrap (success | info | warning | danger)
     * @param string $field Campo do formulário a qual a mensagem pertence
     */
    public static function setMessage($message, $type, $field = null)
    {
        if(!isset($_SESSION['messages'])) {
            $_SESSION['messages'] = array();
        }
        
        if ($field) {
            $_SESSION['messages'][$field] = '<div class="alert alert-' . $type . ' alert-dismissible" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message . '</div>';
        } else {
            $_SESSION['messages']['page'] = '<div class="alert alert-' . $type . ' alert-dismissible" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message . '</div>';
        }
    }

    /**
     * Obtém uma mensagem do sistema de mensagens
     * @param string $field
     * @param boolean $print
     * @return string
     */
    public static function getMessages($field = 'page', $print = false)
    {
        $msg = '';
        if (isset($_SESSION['messages'][$field])) {
            $msg = $_SESSION['messages'][$field];
            unset($_SESSION['messages'][$field]);
            if ($print) {
                echo $msg;
            } else {
                return $msg;
            }
        }
    }
}
