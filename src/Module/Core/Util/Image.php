<?php

namespace Core\Util;


/**
 * Classe de abstração das rotinas de tratamento de imagem pelo PHP
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c) , Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Image {

    const LARGURA = 1, ALTURA = 2;

    private $filename;
    private $image;
    private $width;
    private $height;
    private $extension;

    public function __construct($file) {
        $file = urldecode($file);
        if (file_exists($file)) {
            if (is_file($file)) {
                $this->filename = $file;
                $this->setExtension();
                $this->fixExtension();
                $this->normalizeName();
                $this->loadImageFromFile();
            } else {
                throw new \Exception('O caminho informado não corresponde a um arquivo');
            }
        } else {
            throw new \Exception('O arquivo ' . $file . ' informado não existe.');
        }
    }

    /**
     * Seta a extensão
     */
    private function setExtension() {
        $this->extension = strtolower(pathinfo($this->filename, PATHINFO_EXTENSION));
    }

    /**
     * Corrige a extensão do arquivo, caso esta tenha sido alterada
     */
    private function fixExtension() {
        if (function_exists('exif_imagetype')) {
            switch (@exif_imagetype($this->filename)) {
                case IMAGETYPE_JPEG:
                    $extensions = array('jpg', 'jpeg');
                    break;
                case IMAGETYPE_PNG:
                    $extensions = array('png');
                    break;
                case IMAGETYPE_GIF:
                    $extensions = array('gif');
                    break;
            }

            // Adjust incorrect image file extensions:
            if (!empty($extensions)) {
                $ext = strtolower(pathinfo($this->filename, PATHINFO_EXTENSION));

                if (!in_array($ext, $extensions)) {
                    $ext = $extensions[0];
                    $name = pathinfo($this->filename, PATHINFO_FILENAME) . '.' . $ext;

                    rename($this->filename, pathinfo($this->filename, PATHINFO_DIRNAME) . DIRECTORY_SEPARATOR . $name);
                    $this->extension = $ext;
                }
            }
        }
    }

    /**
     * Normaliza o nome, removendo espaços, caracteres especiais, etc
     * @depends Util
     */
    private function normalizeName() {
        $newName = uniqid(Text::textToUrl(pathinfo($this->filename, PATHINFO_FILENAME)));
        $newLocation = str_replace(pathinfo($this->filename, PATHINFO_FILENAME), $newName, $this->filename);
        rename($this->filename, $newLocation);
        $this->filename = $newLocation;
    }

    /**
     * Carrega uma imagem a partir de um arquivo
     */
    private function loadImageFromFile() {

        if (class_exists('Imagick')) {

            $this->image = new \Imagick($this->filename);

        } else {
            $ext = $this->extension;
            $fType = ($ext == 'jpg' ? 'jpeg' : $ext);

            $funcao = 'imagecreatefrom' . $fType;
            // Seta o resource da imagem
            $this->image = $funcao($this->filename);
        }



        // Seta o tamanho da imagem
        list($largura, $altura) = getimagesize($this->filename);
        
        $this->width = $largura;
        $this->height = $altura;
    }

    /**
     * Cria uma imagem temporária, quando em uso a biblioteca GD
     * @param type $largura
     * @param type $altura
     * @return type
     * @throws \Exception
     */
    private function createTempImage($largura = 100, $altura = 100) {
        if (is_numeric($largura) && is_numeric($altura)) {
            $tmpImage = imagecreatetruecolor($largura, $altura);
            imagealphablending($tmpImage, false); // Herda definições de alpha da imagem original
            imagesavealpha($tmpImage, true);
            return $tmpImage;
        } else {
            throw new \Exception('Forneça somente valores numéricos para largura e altura');
        }
    }

    /**
     * Salva a imagem
     * @param image $image
     */
    private function saveImage($image) {
        if (class_exists('Imagick')) {
            $this->image->writeImage();
            $this->width = $this->image->getImageWidth();
            $this->height = $this->image->getImageHeight();
        } else {
            $ext = $this->extension;
            $fType = ($ext == 'jpg' ? 'jpeg' : $ext);

            $funcao = 'image' . $fType;
            $quality = $fType == 'png' ? 0 : 100;

            if ($fType == 'jpeg' || $fType == 'png') {
                $funcao($image, $this->filename, $quality);
            } else {
                $funcao($image, $this->filename);
            }

            $this->image = $image;
            // Seta o tamanho da imagem
            list($largura, $altura) = getimagesize($this->filename);
            $this->width = $largura;
            $this->height = $altura;
        }
    }

    /**
     * Retorna a largura da imagem
     * @return real
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * Retorna a altura da imagem
     * @return real
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * Redimensiona a imagem
     * @param real $valor
     * @param int $tipo
     */
    public function resize($valor, $tipo = self::LARGURA) {
        if ($tipo == self::LARGURA) {
            $largura = $valor;
            $altura = $largura * $this->getHeight() / $this->getWidth();
        } else {
            $altura = $valor;
            $largura = $altura * $this->getWidth() / $this->getHeight();
        }
        
        if (class_exists('Imagick')) {
            $this->image = $this->image->coalesceImages();
            $this->image->resizeImage($largura, $altura, \Imagick::FILTER_POINT, 0, true);
        } else {
            $imagem = $this->createTempImage($largura, $altura);

            imagecopyresampled($imagem, $this->image, 0, 0, 0, 0, $largura, $altura, $this->getWidth(), $this->getHeight());
        }

        $this->saveImage($imagem);
    }

    /**
     * Corta a imagem
     * @param real $x
     * @param real $y
     * @param real $x2
     * @param real $y2
     */
    public function crop($x, $y, $x2, $y2) {
        $width = ceil($x2 - $x);
        $height = ceil($y2 - $y);

        if (class_exists('Imagick')) {
            $this->image->cropImage($width, $height, $x, $y);
        } else {
            
            $imagem = $this->createTempImage($width, $height);


            imagecopyresampled($imagem, $this->image, 0, 0, $x, $y, $x2, $y2, $x2, $y2);
        }
        $this->saveImage($imagem);
        $this->height = $height;
        $this->width = $width;
    }
/**
 * 
 * @return retorna a imagem
 */
    public function getImage() {
        return $this->image;
    }

    /**
     * Move a imagem para um novo diretório
     * @param String $destino
     * @param String $name
     */
    public function moveToDir($destino, $name = null) {
        if (!file_exists($destino)) {
            mkdir($destino, '0777', true);
        }

        $newname = ($name) ? $name : pathinfo($this->filename, PATHINFO_FILENAME) . '.' . pathinfo($this->filename, PATHINFO_EXTENSION);

        rename($this->filename, $destino . DIRECTORY_SEPARATOR . $newname);

        $this->filename = $destino . DIRECTORY_SEPARATOR . $newname;
    }

    public function getFilename() {
        return pathinfo($this->filename, PATHINFO_FILENAME) . '.' . $this->extension;
    }
    
    
    /**
     * Cria uma instância a partir de um upload
     * @param array $file
     * @param string $tmpDir
     * @return \Util\Image
     */
    public static function createFromUploadedFile($file, $tmpDir){
        $tmpDir = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $tmpDir);
        $name = pathinfo($file['name'], PATHINFO_FILENAME . '.' . PATHINFO_EXTENSION);
        
        if($file){
            move_uploaded_file($file['tmp_name'], $tmpDir.DIRECTORY_SEPARATOR.$name);
        }
        
        $image = new Image($tmpDir.DIRECTORY_SEPARATOR.$name);
        
        return $image;
    }
}
