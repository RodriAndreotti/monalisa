<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Exception;

/**
 * Description of NotAllowedException
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class NotAllowedException extends \Exception
{

    public function __construct(string $message = "")
    {
        parent::__construct($message, 401);
    }

}
