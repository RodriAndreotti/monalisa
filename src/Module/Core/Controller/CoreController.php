<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Controller;

/**
 * Description of CoreController
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class CoreController extends AbstractController
{
    public function index()
    {
        $session = new \Core\Helper\SessionHelper();
        /*if($session->isLogged()) {
            return $this->redirect()->toRoute('dashboard');
        } else {
            return $this->redirect()->toRoute('user');
        }*/
        
        return new \Core\Helper\Model\ViewModel();
    }
}
