<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Controller;

/**
 * Controller abstrato
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
abstract class AbstractController
{
    private $request;
    private $serviceManager;
    
    
    public function __construct(\Core\Http\Request $request)
    {
        $this->request = $request;
    }
    
    public function getRequest(): \Core\Http\Request 
    {
        return $this->request;
    }
    
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }
    
    public function redirect()
    {
        return $this->getServiceManager()->get(\Core\Route\Redirect::class);
    }
}
