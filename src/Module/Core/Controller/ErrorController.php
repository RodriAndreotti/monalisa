<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Controller;

/**
 * Controller para os erros
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ErrorController extends AbstractController
{
    public function index()
    {
        $session = new \Core\Helper\SessionHelper();
        
        
        if($this->getRequest()->getFromRoute('code') == 401 && !$session->hasVar('user')) {
            $request = new \Core\Http\Request('/user');
            $request->redirect();
        }
        
        $vh = new \Core\Helper\Model\ViewModel();

        $vh->addVar('code', $this->getRequest()->getFromRoute('code'));
        $vh->addVar('exception', $session->getVar('exception'));
        
        
        
        return $vh;
    }
}
