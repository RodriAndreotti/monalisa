<?php

/*
 * To change this license header. choose License Headers in Project Properties.
 * To change this template file. choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Form\View;

/**
 * Description of FormView
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c)2016. Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class LteView extends \PFBC\View
{

    protected $class = "";

    public function render()
    {
        $this->_form->appendAttribute("class", $this->class);

        echo '<form' . $this->_form->getAttributes() . '><fieldset>';
        $this->_form->getErrorView()->render();

        $elements = $this->_form->getElements();
        $elementSize = sizeof($elements);
        $elementCount = 0;
        for ($e = 0; $e < $elementSize; ++$e) {
            $element = $elements[$e];
            $element->setAttribute('id', $element->getAttribute('name'));
            if ($element instanceof \PFBC\Element\Hidden || $element instanceof \PFBC\Element\HTML)
                $element->render();
            elseif ($element instanceof \PFBC\Element\Button) {
                if ($e == 0 || !$elements[($e - 1)] instanceof \PFBC\Element\Button)
                    echo '<div class="form-actions">';
                else
                    echo ' ';

                $element->render();

                if (($e + 1) == $elementSize || !$elements[($e + 1)] instanceof \PFBC\Element\Button)
                    echo '</div>';
            }
            else {
                $animateClass = 'form-group';
                if ($element instanceof \PFBC\Element\Checkbox || $element instanceof \Core\Form\Builder\TrueFalse) {
                    $animateClass = 'checkbox';
                    $element->setAttribute('class', 'checkbox');
                }
                elseif($element instanceof \PFBC\Element\CKEditor || 
                        $element instanceof \Core\Form\Builder\FilePreview || 
                        $element instanceof \Core\Form\Builder\JQueryUpload){
                    $animateClass = '';
                }
                
                echo '<div class="form-group ' . $animateClass . '" style="margin-top:40px !important;" '
                . 'id="wrapper_' . $element->getAttribute('name') . '">';
                
                if (!$element instanceof \PFBC\Element\Checkbox && 
                        !$element instanceof \Core\Form\Builder\TrueFalse && 
                        !$element instanceof \PFBC\Element\CKEditor &&  
                        !$element instanceof \Core\Form\Builder\FilePreview && 
                        !$element instanceof \Core\Form\Builder\JQueryUpload) {
                    echo $this->renderLabel($element) . $element->render() . $this->renderDescriptions($element);
                }
                elseif($element instanceof \Core\Form\Builder\FilePreview || 
                        $element instanceof \Core\Form\Builder\JQueryUpload){
                    echo $element->render() . $this->renderDescriptions($element);
                }
                elseif($element instanceof \PFBC\Element\CKEditor){
                    echo $this->renderLabel($element) . $element->render() . $this->renderDescriptions($element);
                }
                else{
                    echo $element->render();
                }
                echo '</div>';
                ++$elementCount;
            }
        }

        echo '</fieldset></form>';
    }

    protected function renderLabel(\PFBC\Element $element)
    {
        $label = $element->getLabel();
        if (!empty($label)) {

            echo '<span class="bar"></span>';

            echo '<label for="' . $element->getAttribute("id") . '">';

            if ($element->isRequired())
                echo '<span class="required">* </span>';
            echo $label . '</label>';
        }
    }

}
