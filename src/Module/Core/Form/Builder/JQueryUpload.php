<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Form\Builder;

/**
 * Description of FilePreview
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class JQueryUpload extends \PFBC\Element
{
    protected $_attributes = array("type" => "file");
    
    public function render()
    {
        $img = parent::getAttribute('value');
        
        $hidden = new \PFBC\Element\Hidden('file' . $this->getAttribute('name'), $img);
        $hidden->render();
        echo '<span class="btn btn-success fileinput-button">';
        echo '<i class="glyphicon glyphicon-plus"></i> ';
        echo '<span>' . $this->getLabel() . '</span>';
        parent::render();
        echo '</span>';
        
        echo ' <span id="nome_' . $this->getAttribute('name') . '">' . $this->getAttribute('value') . '</span>';
    }

}
