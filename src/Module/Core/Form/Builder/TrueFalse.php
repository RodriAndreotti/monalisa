<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Form\Builder;

/**
 * Description of TrueFalse
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * @copyright (c)2016, Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class TrueFalse extends \PFBC\OptionElement
{

    private $checkedValue;
    protected $_attributes = array("type" => "checkbox");

    public function __construct($label, $name, $checkedValue, array $properties = null)
    {
        parent::__construct($label, $name, $properties);
        $this->checkedValue = $checkedValue;


        $this->_attributes["value"] = $checkedValue;
    }

    public function render()
    {

        echo '<label class="checkbox">';

        
        parent::render();
        echo '<span class="outer">';
        echo '<span class="inner"></span>';
        echo '</span>';
        echo $this->getLabel();
        echo '</label>';
    }

}
