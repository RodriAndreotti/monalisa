<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Helper;

/**
 * Abstração da visualização de páginas
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class View
{

    private $router;
    private $title;
    private $styles;
    private $scripts;
    private $inlineScripts;
    private $content;
    private $template = 'tempate/template.php';
    private $vars;
    private $basePath;
    private $model;
    private $absPath;

    public function __construct($template, \Core\Route\Router $router, $absPath)
    {
        $this->router = $router;

        try {
            $this->model = $router->dispatch($this);
        } catch (\Exception $ex) {
            $sessionHelper = new SessionHelper();
            $sessionHelper->setVar('exception', $ex);
            // $sessionHelper->setVar('ex-message', $ex->getMessage());

            $router->redirect('erro', array('code' => $ex->getCode()));
        }

        $this->template = $template;
        $this->absPath = $absPath;

        $this->basePath = $this->getProtocol() . '://';
        $this->basePath .= \Core\Application::URL_SISTEMA . ($_SERVER['SERVER_PORT'] ? ':' . $_SERVER['SERVER_PORT'] : '');
        

        $this->inlineScripts['external'] = array();
        $this->inlineScripts['inline'] = array();
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getStyles()
    {
        $styles = '';

        foreach ($this->styles as $style) {
            $styles .= "<link rel=\"stylesheet\" href=\"{$style}\">\n\t";
        }

        return $styles;
    }

    /**
     * Gera os includes para os scripts de cabeçalho
     * @return string
     */
    public function getScripts()
    {
        $scripts = '';

        foreach ($this->scripts as $script) {
            $scripts .= "<script src=\"{$script}\" type=\"text/javascript\"></script>\n\t";
        }

        return $scripts;
    }

    /**
     * Gera os includes para os scripts inline
     * @return string
     */
    public function getInlineScripts()
    {
        $scripts = '';


        ksort($this->inlineScripts['external']);
        ksort($this->inlineScripts['inline']);


        foreach ($this->inlineScripts['external'] as $script) {
            $scripts .= "<script src=\"{$script}\" type=\"text/javascript\"></script>\n\t";
        }

        $scripts .= '<script type="text/javascript">';
        foreach ($this->inlineScripts['inline'] as $script) {
            $scripts .= preg_replace("/<script\b[^>]*>([\s\S]*?)<\/script>/im", "$1", $script);
        }
        $scripts .= '</script>';

        return $scripts;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function addStyle($styles)
    {
        $this->styles[] = $styles;
        return $this;
    }

    public function addScript($scripts)
    {
        $this->scripts[] = $scripts;
        return $this;
    }

    /**
     * Insere um script inline, pode ser externo ou interno
     * @param string $inlineScripts
     * @param string $inline
     * @param string $priority
     * @return $this
     */
    public function addInlineScript($inlineScripts, $inline = false, $priority = null)
    {
        $priority = ($priority ? $priority : ($inline ? count($this->inlineScripts['inline']) : count($this->inlineScripts['external'])));

        if ($inline) {
            $this->inlineScripts['inline'][$priority] = $inlineScripts;
        } else {
            $this->inlineScripts['external'][$priority] = $inlineScripts;
        }
        return $this;
    }

    public function getContent()
    {
        return require_once $this->content;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function get($key)
    {
        if (isset($this->vars[$key])) {
            return $this->vars[$key];
        }

        return null;
    }

    public function basePath($file)
    {
        return $this->basePath . '/' . $file;
    }

    /**
     * Renderiza o html
     */
    public function render()
    {
        if ($this->model instanceof Model\ViewModel) {
            $this->vars = $this->model->getContent();
            $this->content = $this->router->getRender() . '.php';
            if ($this->model->getLayout()) {
                $this->template = $this->model->getLayout();
            }
            require_once $this->template;
        } elseif ($this->model instanceof Model\JsonModel) {
            header('Content-Type: application/json; charset=utf-8');
            echo $this->model->getContent();
        }
    }

    /**
     * Obtém o protocolo no qual está sendo executado o aplicativo (http ou https)
     * @return string
     */
    private function getProtocol()
    {
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $isSecure = true;
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $isSecure = true;
        }
        return $isSecure ? 'https' : 'http';
    }

    /**
     * Constrói a url para a view
     * @param string $rota
     * @param array $args
     * @return string
     */
    public function url($rota, array $args = null)
    {
        $pattern = $this->router->getRoutePattern($rota);

        $arr = explode('/', trim($pattern, '/'));


        $urlArr = array();

        foreach ($arr as $i => $part) {
            if ($i == 0) {
                if (strpos($part, ':') !== false) {
                    $part = str_replace(':', '', $part);
                    if (isset($args[$part])) {
                        $urlArr[$i] = $args[$part];
                    }
                } else {
                    $urlArr[$i] = $part;
                }
            } else {
                if (strpos($part, ':') !== false) {
                    $part = str_replace(':', '', $part);

                    if (isset($args[$part])) {
                        $urlArr[$i] = $args[$part];
                    }
                } else {
                    $urlArr[$i] = $part;
                }
            }
        }



        $url = implode('/', $urlArr);

        return '/' . $url;
    }

    public function getAbsPath()
    {
        return $this->absPath;
    }

}
