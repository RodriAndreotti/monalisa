<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Helper\Model;

/**
 * Implementação do modelo json
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class JsonModel implements ModelInterface
{
    private $vars;
    
    public function __construct($vars = array())
    {
        $this->vars = $vars;
    }

    public function getContent()
    {
        if(is_array($this->vars)){
            return json_encode($this->vars);
        }
        elseif(is_object($this->vars)){
            return json_encode($this->vars->getArrayCopy());
        }
        elseif(is_string($this->vars)){
            return $this->vars;
        }
    }
    
    public function addVar($var, $value)
    {
        $this->vars[$var] = $value;
        return $this;
    }
}
