<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Helper\Model;

/**
 * Interface dos modelos de renderização
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
interface ModelInterface
{
    public function __construct($vars);
    public function getContent();
    public function addVar($var, $value);
}
