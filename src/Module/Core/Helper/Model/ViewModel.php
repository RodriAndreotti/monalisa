<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Helper\Model;

/**
 * Implementação do modelo para web
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ViewModel implements ModelInterface
{
    private $vars;
    
    private $layout;
    
    public function __construct($vars = array())
    {
        $this->vars = $vars;
    }

    public function getContent()
    {
        return $this->vars;
    }

    public function addVar($var, $value)
    {
        $this->vars[$var] = $value;
        return $this;
    }
    
    public function getLayout() {
        return $this->layout;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }
}
