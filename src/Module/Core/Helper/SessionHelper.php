<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Helper;

/**
 * Helper para acesso as sessões
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class SessionHelper
{

    private $session;

    public function __construct()
    {
        $this->session = &$_SESSION;
    }

    /**
     * Obtém uma variável da sessão
     * @param string $name
     * @return mixed
     */
    public function getVar($name)
    {
        return $this->session[$name];
    }

    /**
     * Obtém um objeto da sessão
     * @param string $name
     * @return object
     */
    public function getObject($name)
    {
        return unserialize(serialize($this->session[$name]));
    }

    /**
     * Insere uma variável na sessão
     * @param string $var
     * @param mixed $value
     */
    public function setVar($var, $value)
    {
        if (is_object($value)) {
            $value = $this->sanitizeObject($value);
        }
        $this->session[$var] = $value;
    }

    /**
     * Sanitiza o objeto para inserir na sessão
     * @param object $objeto
     * @return mixed
     */
    private function sanitizeObject($objeto)
    {
        if (method_exists($objeto, 'setRepository')) {
            $objeto->setRepository(null);
        }

        $api = new \ReflectionClass($objeto);
        foreach ($api->getMethods() as $metodo) {
            $method = $metodo->name;


            if (preg_match('/get/i', $method) && $method != 'getArrayCopy') {
                $val = call_user_func(array($objeto, $method));
                // Verifica se o valor da variável é um objeto
                if (is_object($val)) {

                    $setter = str_replace('get', 'set', $method);

                    call_user_func(
                            array($objeto, $setter), $this->sanitizeObject(
                                    $val
                            )
                    );
                }
            }
        }

        return $objeto;
    }

    public function hasSession()
    {
        return session_status() == PHP_SESSION_ACTIVE;
    }

    public function start($strLocal)
    {        
        $strUuid = $strLocal;
        $strUuid .= (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
        $strUuid .= (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');

        $siteUuid = md5($strUuid);
        if (!isset($_SESSION['origem'])) {
            ini_set("session.gc_maxlifetime", "7200");
            session_cache_expire(120);
            session_name($siteUuid);
            session_start();
            $_SESSION['origem'] = $siteUuid;
        }
    }

    public function hasVar($var)
    {
        return isset($_SESSION[$var]);
    }

    public function isLogged()
    {
        return $this->hasVar('user') && $this->getObject('user');
    }

}
