<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Factory\Route;

/**
 * Factory para o router
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class RouterFactory implements \Core\Factory\FactoryInterface
{

    public function createService(\Core\Application $app, $requestedClass)
    {
        $modulePath = $app->getAbsolutePath() . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Module' . DIRECTORY_SEPARATOR;

        $handle = opendir($modulePath);

        $routes = array();
        
        
        while ($file = readdir($handle)) {
            
            if(is_dir($modulePath . $file) && $file != '.' && $file != '..'){                
                $configFile = $modulePath . $file . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'module.config.php';
                
                if(file_exists($configFile)){
                    $configArray = require $configFile;
                    
                    if(isset($configArray['routes'])){
                        $routes += $configArray['routes'];
                    }
                }
            }
        }
        
        $router =  new \Core\Route\Router();
        
        $i = 0;
        foreach($routes as $routeName=>$route) {
            $route['name'] = $routeName;
            if(isset($route['order'])){
                $router->addRoute($route, $route['order']);
            }
            else {
                $router->addRoute($route, $i++);
            }
        }
        
        $request = new \Core\Http\Request($_SERVER['REQUEST_URI']);
        
        
        $request->setPostFields($_POST);
        $router->setRequest($request);
        
        return $router;
    }

}
