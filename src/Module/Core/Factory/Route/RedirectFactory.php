<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Factory\Route;

/**
 * Cria o serviço de redirect
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class RedirectFactory implements \Core\Factory\FactoryInterface
{
    
    public function createService(\Core\Application $app, $requestedClass)
    {
        return \Core\Route\Redirect::getInstance($app->getServiceManager()->get(\Core\Route\Router::class));
    }

}
