<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Factory\Service;

/**
 * Description of ServiceFactory
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class ServiceFactory implements \Core\Factory\FactoryInterface
{

    public function createService(\Core\Application $app, $requestedClass)
    {
        $modulePath = $app->getAbsolutePath() . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Module' . DIRECTORY_SEPARATOR;

        $handle = opendir($modulePath);

        $services = array();


        while ($file = readdir($handle)) {

            if (is_dir($modulePath . $file) && $file != '.' && $file != '..') {
                $configFile = $modulePath . $file . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'module.config.php';

                if (file_exists($configFile)) {
                    $configArray = require $configFile;
                    
                    if(isset($configArray['services'])){
                        $services += $configArray['services'];
                    }
                }
            }
        }

        $serviceManager = new \Core\Manager\Service($app);
        
        foreach ($services as $service => $fac) {
            $factory = new $fac();
            $serviceManager->addService($service, $factory);
        }

        return $serviceManager;
    }

}
