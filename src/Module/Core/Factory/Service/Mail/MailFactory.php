<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Factory\Service\Mail;

/**
 * Factory para o serviço de e-mail
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class MailFactory implements \Core\Factory\FactoryInterface
{
    
    public function createService(\Core\Application $app, $requestedClass)
    {
        $config = array();
        
        $siteConfig = $app->getServiceManager()->get(\Site\Repository\SiteConfigRepository::class)->carregar();
        
        if($siteConfig){
            $config['host'] = $siteConfig->getServidorSmtp();
            $config['user'] = $siteConfig->getUsuarioSmtp();
            $config['passwd'] = $siteConfig->getSenhaSmtp();
            $config['port'] = $siteConfig->getPortaSmtp();
            $config['ssl'] = $siteConfig->getTipoCriptografiaSmtp();
        }
        
        return new \Core\Service\Mail\Mail($config);
    }

}
