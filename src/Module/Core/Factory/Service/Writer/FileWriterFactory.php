<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 

namespace Core\Factory\Service\Writer;

/**
 * Factory para o writer de arquivos
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class FileWriterFactory implements \Core\Factory\FactoryInterface
{

    public function createService(\Core\Application $app, $requestedClass)
    {
        return new \Core\Service\Writer\FileWriter($app->getAbsolutePath() . DIRECTORY_SEPARATOR . $app->getConfig('filesDir'));
    }
}
