<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Factory\Form;

/**
 * Factory padrão para formulários
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class DefaultFormFactory implements \Core\Factory\FactoryInterface
{
    public function createService(\Core\Application $app, $requestedClass)
    {
        if(class_exists($requestedClass)) 
        {
            return new $requestedClass($_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] ? ':' . $_SERVER['SERVER_PORT'] : '') . '/');
        }
        
        return null;
    }

}
