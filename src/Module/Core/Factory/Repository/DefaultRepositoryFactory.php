<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Factory\Repository;

/**
 * Factory padrão para repositório
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class DefaultRepositoryFactory implements \Core\Factory\FactoryInterface
{
   
    public function createService(\Core\Application $app, $requestedClass) {
        if (class_exists($requestedClass)) {
            return $requestedClass::getInstance(\Core\DB\Connection::getInstance($app));
        }
        return null;
    }

}
