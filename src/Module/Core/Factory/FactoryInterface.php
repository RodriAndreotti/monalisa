<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Factory;

/**
 * Interface para factory
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
interface FactoryInterface
{
    public function createService(\Core\Application $app, $requestedClass);
}
