<?php
$session = new Core\Helper\SessionHelper();
$totalNotificacoes = 0;

/*if ($session->getVar('qtdProgramas') > 0)
    $totalNotificacoes++;

if ($session->getVar('qtdCanais') > 0)
    $totalNotificacoes++;

if ($session->getVar('qtdGeneros') > 0)
    $totalNotificacoes++;*/
?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?= $this->url('user', array('action' => 'dashboard')); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>M</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Monalisa</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Abrir menu</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <?php if ($totalNotificacoes) : ?>
                            <span class="label label-warning"><?= $totalNotificacoes; ?></span>
                        <?php endif; ?>
                    </a>
                    <?php /*if ($totalNotificacoes) : ?>
                        <ul class="dropdown-menu">
                            <li class="header">Você tem <?= $totalNotificacoes; ?> notificações</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <?php if ($session->getVar('qtdProgramas') > 0) : ?>
                                        <li>
                                            <a href="<?= $this->url('grade'); ?>">
                                                <i class="fa fa-th text-aqua"></i> <?= $session->getVar('qtdProgramas'); ?> Programas aguardando revisão
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if ($session->getVar('qtdCanais') > 0) : ?>
                                        <li>
                                            <a href="<?= $this->url('canal'); ?>">
                                                <i class="fa fa-asterisk text-yellow"></i> <?= $session->getVar('qtdCanais'); ?> Canais sem mapeamento
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if ($session->getVar('qtdGeneros') > 0) : ?>
                                        <li>
                                            <a href="<?= $this->url('categoria'); ?>">
                                                <i class="fa fa-universal-access text-red"></i> <?= $session->getVar('qtdGeneros'); ?> Gêneros sem mapeamento
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; */?>
                </li>
                <!-- Tasks: style can be found in dropdown.less -->

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $this->basepath('assets/img/no-pic.png'); ?>" class="user-image" alt="User Image">
                        <?php
                        $user = $session->getObject('user');
                        ?>
                        <span class="hidden-xs"><?= $user ? $user->getNome() : ''; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- Menu Footer-->
                        <li class="user-footer">

                            <div class="pull-right">
                                <a href="<?= $this->url('user', array('action' => 'mudarsenha')); ?>" class="btn btn-info btn-flat">Mudar Senha</a>
                                <a href="<?= $this->url('user', array('action' => 'logoff')); ?>" class="btn btn-default btn-flat">Sair</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>