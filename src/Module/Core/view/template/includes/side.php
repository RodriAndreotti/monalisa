<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->


        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu</li>
            <li>
                <a href="<?= $this->url('user', array('action' => 'dashboard')); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>

            </li>
            <li>
                <a href="<?= $this->url('user', array('action' => 'listar')); ?>">
                    <i class="fa fa-users"></i> <span>Colaboradores</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->url('importacao', array('action' => 'index')); ?>">
                    <i class="fa fa-file-excel-o "></i> <span>Importar</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-refresh"></i> <span>Associações</span> 
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?= $this->url('associacao', array('action' => 'condominio')); ?>"><i class="fa fa-id-card-o"></i> Condomínio</a>
                        </li>
                        <li>
                            <a href="<?= $this->url('associacao', array('action' => 'unidade')); ?>"><i class="fa fa-home"></i> Unidade</a>
                        </li>
                        <?php /*<li><a href="<?= $this->url('associacao', array('action' => 'emissao')); ?>"><i class="fa fa-money"></i> Emissões</a></li>  */?>
                        <li><a href="<?= $this->url('associacao', array('action' => 'conta')); ?>"><i class="fa fa-bank"></i> Planos de conta</a></li>
                        <?php /*<li><a href="<?= $this->url('associacao', array('action' => 'carteira')); ?>"><i class="fa fa-dollar"></i> Carteira</a></li> */?>


                    </ul>
                </a>
            </li>
            <li><hr></li>
            <li>
                <a href="<?= $this->url('view', array('action' => 'emissao')); ?>">
                    <i class="fa fa-eye"></i> <span>Visualizar Emissões importadas</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->url('view', array('action' => 'recibo')); ?>">
                    <i class="fa fa-eye"></i> <span>Visualizar Recibos importadas</span>
                </a>
            </li>
            <li><hr></li>
            <li>
                <a href="<?= $this->url('exportacao', array('action' => 'index')); ?>">
                    <i class="fa fa-download"></i> <span>Exportar</span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>