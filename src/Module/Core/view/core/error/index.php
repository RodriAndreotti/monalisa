<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$exception = $this->get('exception');

if ($this->get('code') == 500) {
    $headLineColor = 'text-red';
    $message = 'Ops! Algo deu errado.';
    $message2 = 'Nós estamos trabalhando para resolver o problema o mais breve o possível.';
    $redirectMsg = 'Volte ao <a href="' . $this->url('user', array('action' => 'dashboard')) . '">dashboard</a> e tente acessar novamente';
} elseif ($this->get('code') == 404) {
    $headLineColor = 'text-yellow';
    $message = 'Ops! Página não encontrada.';
    $message2 = 'A tela que você tentou acessar não existe ou não está disponível.';
    $redirectMsg = 'Volte ao <a href="' . $this->url('user', array('action' => 'dashboard')) . '">dashboard</a> e tente acessar novamente';
} elseif ($this->get('code') == 401) {
    $headLineColor = 'text-yellow';
    $message = 'Ops! Você não tem acesso a esta área.';
    $message2 = 'Você tentou acessar uma página que você não tem permissão para acessar.';
    $redirectMsg = 'Talvez vocé precise realizar <a href="' . $this->url('user') . '">login</a> para acessar esta área';
}
else {
    $headLineColor = 'text-red';
    $message = 'Ops! Algo deu errado.';
    $message2 = 'Erro desconhecido.';
    $redirectMsg = 'Volte ao <a href="' . $this->url('user', array('action' => 'dashboard')) . '">dashboard</a> e tente acessar novamente';
}
?>

<div class="error-page">
    <h2 class="headline <?= $headLineColor; ?>"><?= $this->get('code'); ?></h2>

    <div class="error-content">
        <h3><i class="fa fa-warning <?= $headLineColor; ?>"></i> <?= $message; ?>.</h3>

        <p>
            <?= $message2; ?>
        </p>
        <p>
            <?= $redirectMsg; ?>
        </p>

    </div>
    <?php if (isset($exception)) : ?>
        <h4>Detalhes do erro</h4>
        <pre>
            <?php print_r($exception->getTraceAsString()); ?>
        </pre>
    <?php endif; ?>
</div>