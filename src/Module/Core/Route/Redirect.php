<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Route;

/**
 * Redireciona uma requisição
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Redirect
{

    private $router;
    private static $instance;

    private function __construct(Router $router)
    {
        $this->router = $router;
    }

    public static function getInstance(Router $router)
    {
        if (!self::$instance) {
            self::$instance = new self($router);
        }

        return self::$instance;
    }

    public function toRoute($rota, array $args = null)
    {
        $pattern = $this->router->getRoutePattern($rota);

        $arr = explode('/', trim($pattern, '/'));

        $urlArr = array();

        foreach ($arr as $i => $part) {
            if ($i == 0) {
                if (strpos($part, ':') !== false) {
                    $part = str_replace(':', '', $part);
                    if (isset($args[$part])) {
                        $urlArr[$i] = $args[$part];
                    }
                } else {
                    $urlArr[$i] = $part;
                }
            } else {
                if (strpos($part, ':') !== false) {
                    $part = str_replace(':', '', $part);

                    if (isset($args[$part])) {
                        $urlArr[$i] = $args[$part];
                    }
                } else {
                    $urlArr[$i] = $part;
                }
            }
        }

        $url = implode('/', $urlArr);

        header('Location: /' . $url);
        exit;
    }

    public function toUrl($url)
    {
        header('Location: ' . $url);
        exit;
    }

}
