<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Route;

/**
 * Roteador padrão do sistema
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Router
{

    /**
     * Armazena as rotas
     * @var array 
     */
    private $routes;

    /**
     * Armazena a requisição
     * @var \Core\Http\Request 
     */
    private $request;

    /**
     * Armazena o service manager do sistema
     * @var \Core\Manager\ServiceManagerInterface 
     */
    private $serviceManager;

    /**
     * Recebe a ACL do sistema
     * @var \User\Acl\Acl 
     */
    private $acl;
    private $render;

    public function __construct()
    {
        $this->routes = array();
    }

    /**
     * Adiciona uma rota ao router
     * @param array $route
     */
    public function addRoute($route, int $order = null)
    {
        if($order){
            $this->routes[$order] = $route;
        }
        else {
            $this->routes[] = $route;
        }
    }

    /**
     * Seta a requisição
     * @param \Core\Http\Request $request
     * @return $this
     */
    public function setRequest(\Core\Http\Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Seta a ACL para o sistema
     * @param \User\Acl\Acl $acl
     * @return $this
     */
    public function setAcl(\User\Acl\Acl $acl)
    {
        $this->acl = $acl;
        return $this;
    }

    /**
     * Retorna o pattern da rota
     * @param string $route
     * @return string
     */
    public function getRoutePattern($route)
    {
        $routes = $this->routes;
        ksort($routes);

        foreach ($routes as $r) {
            if ($route == $r['name']) {
                $pattern = str_replace(array('[', '\\', ']'), "", $r['route']);
                return $pattern;
            }
        }

        return null;
    }

    /**
     * Dispara a action encontrada na rota
     * @return \Core\Helper\Model\ModelInterface
     */
    public function dispatch()
    {
        $requestUri = $this->request->getRequestUrl();


        $routes = $this->routes;        
        ksort($routes); 
        
        
        
        

        foreach ($routes as $route) {


            $regexFull = $this->buildRegex($route['route'], isset($route['padroes']) ? $route['padroes'] : null);

            $return = $this->match($regexFull, $route);


            if ($return) {
                return $return;
            }
        }

        throw new \Core\Exception\NotFoundException(sprintf('A rota para a url %s informada não foi encontrada', $requestUri), 404);
    }

    /**
     * Faz o match da url informada com as rotas, caso encontre a URL retorna 
     * o controller instanciado, caso contrário lança exceção
     * 
     * @param array $regexFull array com expressões regulares a serem matcheadas
     * @param string $controller Nome do controller a ser instanciado
     * @return \Core\Controller\AbstractController
     * @throws \Exception
     */
    private function match($regexFull, $route)
    {
        $controller = $route['controller'];
        $requestUri = $this->request->getRequestUrl();

        if ($requestUri == '') {
            $requestUri = '/';
        }



        while (count($regexFull) > 0) {
            $pattern = implode('', $regexFull);


            if (preg_match('/' . str_replace('/', '\/', $pattern) . '/', $requestUri)) {

                foreach ($regexFull as $key => $var) {

                    if (is_string($key)) {
                        preg_match('/' . str_replace('/', '\/', $var) . '/', $requestUri, $valor);
                        
                        $this->request->addRouteParam($key, trim($valor[0], '/'));
                    } else {
                        $this->setRequestVars($requestUri, $route);
                        $requestUri = preg_replace('/' . str_replace('/', '\/', $var) . '/', '', $requestUri);
                        
                    }
                }

                
                
                $ctrl = new $controller($this->request);
                $ctrl->setServiceManager($this->serviceManager);

                $content = str_replace(array('controller', '\\\\'), array('', DIRECTORY_SEPARATOR), strtolower(get_class($ctrl)));


                if ($this->request->getFromRoute('action')) {
                    $this->autorize($route['name'] . '-' . $this->request->getFromRoute('action'));
                    $action = $this->request->getFromRoute('action');
                    $this->render = $route['view-dir'] . DIRECTORY_SEPARATOR . $content . DIRECTORY_SEPARATOR . $action;
                    return $ctrl->$action();
                } else {
                    $this->autorize($route['name'] . '-index');
                    $this->render = $route['view-dir'] . DIRECTORY_SEPARATOR . $content . DIRECTORY_SEPARATOR . 'index';
                    return $ctrl->index();
                }
            }
            array_pop($regexFull);
        }
    }

    /**
     * Constrói os patterns para o match de url
     * 
     * @param string $route
     * @param array $padroes
     * @return string Pattern regex construída com suas devidas chaves apontando para os parâmetros
     */
    private function buildRegex($route, $padroes)
    {
        $match = '/\[[\/:]*[a-zA-Z0-9]*\]/';

        /** @var $optionals array */
        preg_match_all($match, $route, $optionals);

        $requiredRoute = explode('/', preg_replace("/[[\/:]*[a-zA-Z0-9]*\]/", "", $route));



        $regexObrigArr = array();
        foreach ($requiredRoute as $part) {

            if ($part != '') {
                $cleanPart = str_replace(array('[', ']', ':'), '', $part);

                if (preg_match('/:[a-zA-Z0-9]+/', $part)) {

                    $regexObrigArr[$cleanPart] = '/(' . (isset($padroes[$cleanPart]) ? $padroes[$cleanPart] : $cleanPart) . ')';
                } else {
                    $regexObrigArr[$cleanPart] = '/' . $cleanPart;
                }
            }
        }
        
        $regexFull = array();
        $regexFull[] = implode('', $regexObrigArr);


        foreach ($optionals[0] as $i => $part) {
            if ($part != '') {
                $cleanPart = str_replace(array('[', ']'), '', $part);
                $fullCleanPart = str_replace(array('[', ']', ':', '/'), '', $part);

                if (preg_match('/:[a-zA-Z0-9]+/', $part)) {
                    $pattern = '(' . (
                            isset($padroes[$fullCleanPart]) ?
                            preg_replace('/:[a-zA-Z0-9]+/', $padroes[$fullCleanPart], $cleanPart) :
                            $cleanPart) . ')';
                    $regexFull[$fullCleanPart] = $pattern;
                } else {
                    $pattern = $cleanPart;
                    $regexFull[$cleanPart] = $pattern;
                }
            }
        }
        

        return $regexFull;
    }

    /**
     * Seta o gerenciador de serviços
     * 
     * @param \Core\Manager\ServiceManagerInterface $serviceManager
     * @return $this
     */
    public function setServiceManager(\Core\Manager\ServiceManagerInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    /**
     * Retorna o render referente a action
     * @return string
     */
    public function getRender()
    {
        return $this->render;
    }

    /**
     * Verifica se a url pode ser acessada
     * @param string $requestUri
     * @return string
     */
    private function autorize($requestUri)
    {
        if ($this->acl) {
            $resource = str_replace('/', '-', trim($requestUri, '/'));
            $session = new \Core\Helper\SessionHelper();
            $role = $session->isLogged() ? 'admin' : 'guest';


            if (!$this->acl->isAllowed($resource, $role)) {
                throw new \Core\Exception\NotAllowedException('Você não tem permissão para acessar esta tela');
            }
        }
        return $requestUri;
    }

    /**
     * Constrói a url para redirecionamento
     * @param string $rota
     * @param array $args
     * @return string
     */
    public function redirect($rota, array $args)
    {
        $pattern = $this->getRoutePattern($rota);

        $arr = explode('/', trim($pattern, '/'));

        $urlArr = array();

        foreach ($arr as $i => $part) {
            if ($i == 0) {
                $urlArr[$i] = $part;
            } else {
                if(strpos($part, ':') !== false){
                    $part = str_replace(':', '', $part);
                    
                    if (isset($args[$part])) {
                        $urlArr[$i] = $args[$part];
                    }
                }
                else {
                    $urlArr[$i] = $part;
                }
            }
        }

        $url = implode('/', $urlArr);

        $request = new \Core\Http\Request('/' . $url);

        $request->redirect();
    }

    /**
     * Adiciona os parametros no request para urls obrigatórias
     * @param string $matchedUrl
     * @param array $route
     */
    private function setRequestVars($matchedUrl, $route)
    {
        
        $requiredRoute = explode('/', preg_replace("/[[\/:]*[a-zA-Z0-9]*\]/", "", $route['route']));

        foreach ($requiredRoute as $part) {

            if ($part != '') {
                $cleanPart = str_replace(array('[', ']', ':'), '', $part);

                if (preg_match('/:[a-zA-Z0-9]+/', $part)) {
                    
                    preg_match('/' . str_replace('/', '\/', $route['padroes'][$cleanPart]) . '/', $matchedUrl, $valor);
                    
                    $this->request->addRouteParam($cleanPart, $valor[0]);
                } 
            }
        }
    }

}
