<?php
/**
 * Arquivo de configuração do módulo Core
 */
return array(
    'routes'    =>  array(
        'erro' =>  array(
            'route'     =>  '/erro/:code',
            'padroes' => array(
                'code' => '[0-9]+'
            ),
            'controller'    =>  Core\Controller\ErrorController::class,
            'view-dir'      =>  __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view'
        )
    ),
    'services' => array(
        Core\Route\Redirect::class => Core\Factory\Route\RedirectFactory::class,
        Core\Service\Writer\FileWriter::class => Core\Factory\Service\Writer\FileWriterFactory::class,
        \Core\Service\Mail\Mail::class => \Core\Factory\Service\Mail\MailFactory::class
    )
);