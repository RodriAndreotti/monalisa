<?php

namespace RestClient\Service;

/**
 * Classe de teste para o cliente REST dos canais
 */
class CanalClientTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var CanalClient
     */
    protected $object;

    /**
     * Inicializa o objeto para realização dos testes
     */
    protected function setUp()
    {
        $app = new \Core\Application();
        $cache = new \Core\Cache\Cache($app->getAbsolutePath() . $app->getConfig('cacheDir'));
        $this->object = new CanalClient($app, $cache);
    }

    /**
     * Testa a obtenção da lista de canais
     * 
     * @covers AbstractClient::*
     * @covers CanalClient::listCanais
     */
    public function testListCanais()
    {

        $canais = $this->object->listCanais();



        $this->assertTrue($canais instanceof \ArrayObject);
        $this->assertTrue($canais[0] instanceof \Grade\Model\Canal);
        $this->assertTrue($canais[count($canais) - 1] instanceof \Grade\Model\Canal);
    }

}
