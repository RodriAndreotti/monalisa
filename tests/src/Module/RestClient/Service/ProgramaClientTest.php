<?php

namespace RestClient\Service;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2017-10-24 at 22:12:15.
 */
class ProgramaClientTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var ProgramaClient
     */
    protected static $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        if (!ProgramaClientTest::$object) {
            $app = new \Core\Application();
            $cache = new \Core\Cache\Cache($app->getAbsolutePath() . $app->getConfig('cacheDir'));
            ProgramaClientTest::$object = new ProgramaClient($app, $cache);
        }
    }

    /*
     * @covers RestClient\Service\ProgramaClient::listByCanal
     * @todo   Implement testListByCanal().
     */

    public function testListByCanal()
    {
        $hoje = new \DateTime();
        $d = date('w');

        $startDate = clone $hoje;

        $endDate = $hoje->add((new \DateInterval('P' . 6 . 'D')));

        $programas =  ProgramaClientTest::$object->listByCanal(1, $startDate, $endDate);

        $this->assertTrue(is_array($programas));
    }

    public function testeListByCanalNotFound()
    {
        $hoje = new \DateTime();
        $d = date('w');

        $startDate = clone $hoje;

        $endDate = $hoje->add((new \DateInterval('P' . 6 . 'D')));

        $programas =  ProgramaClientTest::$object->listByCanal(27, $startDate, $endDate);

        $this->assertEquals(404, $programas);
    }

    public function testListCategoryByCanal()
    {
        $hoje = new \DateTime();
        $d = date('w');

        $startDate = clone $hoje;

        $endDate = $hoje->add((new \DateInterval('P' . 6 . 'D')));

        $categorias =  ProgramaClientTest::$object->listCategoryByCanal(1, $startDate, $endDate);

        $this->assertTrue(is_array($categorias));
        $this->assertTrue(array_key_exists('contentTypes', $categorias));
        $this->assertTrue(array_key_exists('generos', $categorias));
        $this->assertTrue(array_key_exists('subgeneros', $categorias));
        
        var_dump($categorias);
    }

}
