<?php

namespace Core;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2017-10-19 at 18:30:17.
 */
class ApplicationTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var Application
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Application;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Core\Application::getConfig
     * @expectedException     Exception
     * @expectedExceptionCode 1404
     */
    public function testGetConfig()
    {
        $this->assertEquals('localhost', $this->object->getConfig('db_host'));
        $this->assertEquals('587', $this->object->getConfig('smtp_port'));
        $this->object->getConfig('teste');
    }

    /**
     * @covers Core\Application::getAbsolutePath
     */
    public function testGetAbsolutePath()
    {

        $this->assertTrue(stripos($this->object->getAbsolutePath(), 'NetBeansProjects\revistaeletronica')!== false);
    }

}
