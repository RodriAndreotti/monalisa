# README #

## Dependências ##
* PHP 7.3+
* Extensão mb_string do PHP (Extensão nativa do PHP)
* Extensão OpenSSL do PHP (Extensão nativa do PHP)
* Extensão SQlite do PHP (Extensão nativa do PHP)
* Extensão PDO Mysql do PHP (Extensão nativa do PHP)
* PHP Office)
* PHPMailer (Composer)


## Instalação ##
O sistema segue os padrões sugeridos na PSR-4, sendo assim a instalação do mesmo deve ser seguida da utilização do composer para que o autoloader das classes seja gerado pelo mesmo.

Além do autoloader, é necessário utilizar o composer para instalar também algumas das depenências acima mencionadas.

Sendo assim, após clonar o projeto no servidor de destino é necessário acessar o diretório onde o mesmo foi clonado e utilizar um dos seguintes comandos:

`php composer.phar install --no-dev // Caso o composer tenha sido baixado dentro do diretório do sistema somente para instalação do mesmo`

Ou

`composer install --no-dev // Caso o composer esteja instalado globalmente no sistema`

### Configurações Pós Instalação do Sistema ###

* Configurar as credenciais de acesso ao banco de dados e webservice, estas credenciais se encontram no diretório */src/config/system.config.php*
* Certifique-se que o diretório *data* tem permissão de escrita para o usuário responsável pela execução do PHP
* Certifique-se também que os diretórios */data/cache* e */data/files* existem e tem as mesmas permissões do item acima

#### Caso desejado é possível configurar os diretórios de cache e files para outros diretórios, desde que estes estejam dentro da pasta principal do sistema ####

## Instalação Interface Gráfica ##
* Para que a interface web do sistema esteja acessível é necessário que este seja instalado junto com um servidor http (Apache ou Nginx), em um diretório configurado para permitir o acesso.
* O servidor deve permitir o uso do arquivo .htaccess
* O servidor também deve apontar para  o diretório public do sistema.

