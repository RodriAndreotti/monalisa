// Listeners
$('#btnCopiar').on('click', function (e) {
    e.preventDefault();

    let sinopse = $('#sinopseGlobosat').val();

    $('#sinopse').val(sinopse);

});

$('#dtsinol').on('keyup change', function () {
    let el = $('#qtdCaracteres');
    let charsLeft = 1000;
    charsLeft -= parseInt($('#dtsinol').val().length);

    if (charsLeft < 0) {
        $('#dtsinol').val($('#dtsinol').val().substring(0, 1000));
    }

    el.text(charsLeft);

});

$('#dtsinoc').on('keyup change', function () {
    let el = $('#qtdCaracteresSinoc');
    let charsLeft = 219;
    charsLeft -= parseInt($('#dtsinoc').val().length);

    if (charsLeft < 0) {
        $('#dtsinoc').val($('#dtsinoc').val().substring(0, 219));
    }

    el.text(charsLeft)
});

$('#btGerarSinoc').on('click', function () {
    constroiSinopseCurta();
});


$('#dtgen').off('change keyup').on('change keyup', function () {
    consultaGenero($(this));
});

// Funções
function constroiSinopseCurta() {
    // Formato de sinopse curta:
    // ano. genero. DE diretor. COM 2 nomes de elenco. sinopse

    let strSinoc = $('#dtano').val() ? $('#dtano').val() + '. ' : '';
    strSinoc += $('#dtgen').val() ? $('#dtgen').val() + '. ' : '';

    let diretoresFull = $('#dtdiretor').val();
    let elencoFull = $('#dtelenco').val();

    let diretoresArr = diretoresFull.split(',');
    let elencoArr = elencoFull.split(',');

    if (diretoresFull) {
        if (diretoresArr.length > 1) {
            strSinoc += 'De ' + diretoresArr[0].trim() + '. ';
        } else {
            strSinoc += 'De ' + diretoresFull.trim() + '. ';
        }
    }

    if (elencoFull) {
        if (elencoArr.length > 2) {
            strSinoc += 'Com ' + elencoArr[0].trim() + ', ';
            strSinoc += elencoArr[1].trim() + '. ';
        } else {
            strSinoc += 'Com ' + elencoArr.trim() + '. ';
        }
    }

    strSinoc += $('#dsinol').val();

    strSinoc = strSinoc.substr(0, 219);

    $('#dtsinoc').val(strSinoc);
    $('#dtano, #dtgen, #dtdiretor, #dtelenco, #dtsinol');

}

function consultaGenero(el, setValue)
{
    let genero = el.val();
    
    if('undefined' == typeof setValue) {
        setValue = false;
    }
    
    $.ajax({
        url: '/categoria/listarsubajax',
        type: 'post',
        dataType: 'json',
        data: {genero: genero},
        success: function (response) {
            if (response) {
                if (response.subgeneros.length) {
                    $('#dtsubgen').empty();
                    $.each(response.subgeneros, function (i, item) {
                        $('#dtsubgen').append('<option value="' + item.nome + '">' + item.nome + '</option>');
                    });
                    
                    if(setValue) {
                        $('#dtsubgen').val(subGen);
                    }
                }
            }
        }
    });
}


// Ready
$(document).ready(function () {
    /** 
     * Raty - Rating Star
     */
    $.fn.raty.defaults.path = '/assets/img/raty';

    // Default size star 
    $('.star-rating').raty({
        round: {down: .2, full: .6, up: .8},
        half: false,
        space: true,
        scoreName: 'dtstars',
        score: function () {
            //   $('#dtstars').val($(this).attr('data-rating-score'));
            //return $('#dtstars').val($(this).attr('data-rating-score'));
            return $(this).attr('data-rating-score');
        }
    });
    
    consultaGenero($('#dtgen'), true);
});


