$(document).on('keypress', function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        $("#btLogar").click();
    }
});

$('#btLogar').on('click', function (e) {
    var crypt = new JSEncrypt;
    crypt.setPublicKey(Key.decode(pk));


    var dados = {
        login: crypt.encrypt($('#txtLogin').val()),
        passwd: crypt.encrypt($('#txtPasswd').val()),
        origem: crypt.encrypt(origem)
    };

    $.ajax({
        url: path,
        data: dados,
        dataType: 'json',
        type: 'post',
        beforeSend: function (xhr) {
            $('#message').html('<div id="ajaxloader"/> <span>Logando...</span>');
        },
        complete: function (jqXHR, textStatus) {
            var r = jqXHR.responseJSON;
            if (!r.success) {
                $('#message').html('<div class="alert alert-error alert-dismissible">' + r.msg + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            }

        },
        success: function (response) {
            if (response) {
                if (response.success) {
                    // var lParts = document.location.href.split('/');
                    
                    window.location.assign('/user/dashboard');
                    
                }
            }
        }
    });
});

